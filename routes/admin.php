<?php


// Категории сайта
Route::module('siteSections');


// Проекты
Route::group(['prefix' => 'proj'], function(){

    // Проекты
    Route::module('projects');

    // Списки
    Route::module('projectLists');

    // Типы работ
    Route::module('projectCategories');

    // Направления работ
    Route::module('projectDirections');

    // Награды
    Route::module('aboutAwards');

    // Клиенты
    Route::module('clients');

    // Категории клиентов
    Route::module('clientCategories');

});


// Страница услуг
Route::group(['prefix' => 'serv'], function(){

    // Страница услуг
    Route::module('servicesPages');

    // Страница услуг
    Route::module('services');

});


// About
Route::group(['prefix' => 'about'], function(){

    // About
    Route::module('aboutPages');

    // Направления для About
    Route::module('aboutDirections');

    // Награды для About
    Route::module('aboutAwards');

    // Клиенты
    Route::module('clients');

    // Факты
    Route::module('facts');

    // Вакансии
    Route::module('jobs');

});


// Главная страница
Route::group(['prefix' => 'home'], function(){

    // Главная страница
    Route::module('homePages');

    // Клиенты
    Route::module('clients');

    // Проекты
    Route::module('projects');

    // Награды для About
    Route::module('aboutAwards');

});


// Контакты
Route::module('contactPages');


// Настройки
Route::group(['prefix' => 'main_settings'], function(){

    // shell
    Route::module('shells');

    // Соц. сети
    Route::module('socials');

});











// для связей
Route::module('projects');
Route::module('aboutDirections');
Route::module('aboutAwards');
Route::module('aboutClients');
Route::module('jobs');
Route::module('facts');
Route::module('clients');
Route::module('socials');
Route::module('services');

