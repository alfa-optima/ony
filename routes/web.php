<?php

use App\Classes\Helper;
use App\Classes\Site;
use App\Classes\Media;
use App\Classes\MyFile;
use App\Classes\VimeoHelper;
use Illuminate\Support\Facades\DB;
use App\Models\Project;
use App\Classes\MySetting;
use A17\Twill\Models\Setting;

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoRequestException;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Cocur\Slugify\Slugify;
use Jcupitt\Vips;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});


// Просмотр файлов ID
Route::get('/file/{file_id}', function ( $file_id ){
    MyFile::outputFileByID( $file_id );
})->where([ 'file_id' => '[1-9][0-9]*' ]);
Route::get('/file/{uuid}', function ( $uuid ){
    $file = DB::table('files')->where('uuid', $uuid)->first();
    if( isset($file->id) ){
        MyFile::outputFileByID( $file->id );
    } else {
        abort(404);
    }
})->where([ 'uuid' => '.+' ]);



Route::get('/dbActions', function () {

    ini_set('max_execution_time', 600);
    $conn = Site::getPDO();





    //phpinfo(); die();

    //Cache::flush();  die();

//    Schema::create( 'about_award_home_page', function (Blueprint $table){
//        $table->bigIncrements('id');
//        $table->timestamps();
//        $table->integer('position')->unsigned();
//        createDefaultRelationshipTableFields($table, 'about_award', 'home_page');
//    });



//    $sql = "SELECT id,uuid,filename FROM medias";
//    //$sql .= " WHERE filename REGEXP '\.jpg$' AND id = 17936";
//    $sql .= " WHERE filename REGEXP '\.jpg$' AND id > 18401 AND id < 18600";
//    $sql .= " ORDER BY id";
//    $result = $conn->query($sql);
//    while( $row = $result->fetch() ){
//
//        $picPath = '/var/www/ony/htdocs/storage/app/public/media_uploads/'.$row['uuid'];
//
//        echo "<pre>"; print_r( $picPath ); echo "</pre>";
////
//        $im = Vips\Image::newFromFile($picPath, ['access' => 'sequential']);
//        $n_bins = 10;
//        $hist = $im->hist_find_ndim(['bins' => $n_bins]);
//
//        [$v, $x, $y] = $hist->maxpos();
//        $pixel = $hist->getpoint($x, $y);
//        $z = array_search($v, $pixel);
//        $r = round(($x + 0.5) * 256 / $n_bins, 0);
//        $g = round(($y + 0.5) * 256 / $n_bins, 0);
//        $b = round(($z + 0.5) * 256 / $n_bins, 0);
//
//        echo "<pre>"; print_r( [ $r, $g, $b ] ); echo "</pre>";
//    }
//
//    die();


//    $cnt = 0;
//    $sql = "SELECT id,uuid,filename FROM medias";
//    $sql .= " WHERE filename REGEXP '\.png$'";
//    $result = $conn->query($sql);
//    while( $row = $result->fetch() ){  $cnt++;
//        if( $cnt <= 50 ){
//            $picPath = '/var/www/ony/htdocs/storage/app/public/media_uploads/'.$row['uuid'];
//            if( file_exists( $picPath ) ){
//                preg_match("/(.+\/)(([^\/]*)\.png)$/", $picPath, $matches, PREG_OFFSET_CAPTURE);
//                if(
//                    isset($matches[1][0])
//                    &&
//                    isset($matches[2][0])
//                    &&
//                    isset($matches[3][0])
//                ){
//
//                    $picDir = $matches[1][0];
//                    $fullFileName = $matches[2][0];
//                    $fileName = $matches[3][0];
//                    $newFileName = md5($picPath);
//                    $newFullFileName = md5($picPath).".jpg";
//                    $newUUID = str_replace($fullFileName, $newFileName.".jpg", $row['uuid']);
//
//                    echo "<pre>"; print_r( $picDir ); echo "</pre>";
//                    echo "<pre>"; print_r( $fullFileName ); echo "</pre>";
//                    echo "<pre>"; print_r( $fileName ); echo "</pre>";
//                    echo "<pre>"; print_r( $newFileName ); echo "</pre>";
//                    echo "<pre>"; print_r( $newFullFileName ); echo "</pre>";
//                    echo "<pre>"; print_r( $newUUID ); echo "</pre>";
//
//                    $image = imagecreatefrompng($picPath);
//                    $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
//                    imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
//                    imagealphablending($bg, TRUE);
//                    imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
//                    imagedestroy($image);
//                    $quality = 90;
//                    imagejpeg($bg, $picDir.$newFileName.".jpg", $quality);
//                    imagedestroy($bg);
//
//                    $sql2 = "UPDATE medias SET uuid = '".$newUUID."', filename = '".$newFullFileName."' WHERE id = ".$row['id'];
//                    $conn->query($sql2);
//
//                    echo "<pre>"; print_r( $sql2 ); echo "</pre>";
//                    echo '<hr>';
//                }
//            }
//        }
//    }
//
//
//    die();


//    $pics = [
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/02098a46-a761-4eb4-8658-10851428aef7/.png',
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/project_poster/ca696d94d9dc83dde3e352a4a9efc079/50931a1466d991e80184e54278a12c8e.jpg',
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/project_block_images/482e899715005aad72756abcba3b39ec/3_pictleft.jpg',
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/project_block_images/1a1f9d99cf195acaa6f0e77012a4e12a/df31d57630dde4a6919e35a4dbd7d343.jpg',
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/project_block_images/57716d7bf1c7aecbe762862691ce2490/9_pictleft.jpg',
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/project_block_images/ec80b74239cea4feb8672da6fd95de64/11_mobile.jpg',
//        '/var/www/ony/htdocs/storage/app/public/media_uploads/project_block_images/98d176cfc760894872d2cff87b429454/12_fin.jpg'
//    ];
//
//    foreach ( $pics as $picPath ){
//        if( file_exists($picPath) ){
//
//            $im = Vips\Image::newFromFile($picPath, ['access' => 'sequential']);
//            $n_bins = 10;
//            $hist = $im->hist_find_ndim(['bins' => $n_bins]);
//
//            [$v, $x, $y] = $hist->maxpos();
//            $pixel = $hist->getpoint($x, $y);
//            $z = array_search($v, $pixel);
//            $r = round(($x + 0.5) * 256 / $n_bins, 0);
//            $g = round(($y + 0.5) * 256 / $n_bins, 0);
//            $b = round(($z + 0.5) * 256 / $n_bins, 0);
//            echo "<pre>"; print_r( [ $r, $g, $b ] ); echo "</pre>";
//
//        }
//    }
//
//
//    die();


    //Project::updateVimeoVideosStatuses();


//    $info = VimeoHelper::infoByURL('/videos/405329801');
//    dd($info);


//    Schema::create('contact_pages', function (Blueprint $table) {
//        createDefaultTableFields($table);
//        $table->string('map_lat', 200)->nullable();
//        $table->string('map_lng', 200)->nullable();
//        $table->string('map_zoom', 200)->nullable();
//        $table->timestamp('publish_start_date')->nullable();
//        $table->timestamp('publish_end_date')->nullable();
//        $table->integer('position')->unsigned()->nullable();
//    });
//    Schema::create('contact_page_translations', function (Blueprint $table) {
//        createDefaultTranslationsTableFields($table, 'contact_page');
//        $table->text('lead_text')->nullable();
//        $table->string('address_title', 200)->nullable();
//        $table->text('address')->nullable();
//
//    });
//    Schema::create('contact_page_slugs', function (Blueprint $table) {
//        createDefaultSlugsTableFields($table, 'contact_page');
//    });

//    Schema::create( 'about_page_client', function (Blueprint $table){
//        $table->bigIncrements('id');
//        $table->timestamps();
//        $table->integer('position')->unsigned();
//        createDefaultRelationshipTableFields($table, 'client', 'about_page');
//    });


//Schema::create( 'client_client_category', function ( Blueprint $table ) {
//    $table->increments('id');
//    $table->integer('client_id');
//    $table->integer('client_category_id');
//});


//    Schema::create('shells', function (Blueprint $table) {
//        createDefaultTableFields($table);
//        $table->string('title', 200)->nullable();
//        $table->timestamp('publish_start_date')->nullable();
//        $table->timestamp('publish_end_date')->nullable();
//        $table->integer('position')->unsigned()->nullable();
//    });
//    Schema::create('shell_translations', function (Blueprint $table) {
//        createDefaultTranslationsTableFields($table, 'shell');
//        $table->string('seo_title', 200)->nullable();
//        $table->string('seo_description', 200)->nullable();
//        $table->string('seo_ogDescription', 200)->nullable();
//        $table->string('footer_address_title', 200)->nullable();
//        $table->text('footer_address_content')->nullable();
//        $table->string('footer_newBusinessContacts_title', 200)->nullable();
//        $table->text('footer_newBusinessContacts_content')->nullable();
//        $table->string('footer_otherContacts_title', 200)->nullable();
//        $table->text('footer_otherContacts_content')->nullable();
//        $table->string('footer_social_block_title', 200)->nullable();
//    });
//    Schema::create('shell_slugs', function (Blueprint $table) {
//        createDefaultSlugsTableFields($table, 'shell');
//    });



    // Запись переводов
//    Site::setTranslations ( $conn,
//        'project',
//        'projects'
//    );


    // Запись слагов
//    Site::setSlugs ( $conn,
//        'project',
//        'projects'
//    );


// Замена time из старой базы на timestamp (mysql)
//$sql = "SELECT * FROM projects";
//$result = $conn->query($sql);
//while( $row = $result->fetch() ){
//    $sql = "UPDATE projects SET date_new = '".date( "Y-m-d H:i:s", $row['date'] )."' WHERE id=".$row['id'];
//    $conn->query($sql);
//}


// Простановка published из active
//$sql = "SELECT * FROM projects";
//$result = $conn->query($sql);
//while( $row = $result->fetch() ){
//    $sql = "UPDATE projects SET published = ".$row['active']." WHERE id=".$row['id'];
//    $conn->query($sql);
//}


    // Заполнение таблицы-связи проектов с категориями проектов
//    $sql = "DELETE FROM project_project_category";
//    $conn->query($sql);
//    $sql = "SELECT * FROM projects";
//    $result = $conn->query($sql);
//    while( $row = $result->fetch() ){
//        $ar = [];
//        if( strlen($row['categorys']) > 0 ){     $ar = unserialize($row['categorys']);     }
//        foreach ( $ar as $key => $category_id ){
//            $sql = "INSERT INTO project_project_category (id, project_id, project_category_id) VALUES (null, ".$row['id'].", ".$category_id.");";
//            $conn->query($sql);
//        }
//    }



// Перебираем все проекты
//$sql = "SELECT * FROM projects";
//$projects = $conn->query($sql);
//while( $project = $projects->fetch() ){
//    // Получаем блоки, привязанные к данному проекту
//    $sql = "SELECT * FROM wf_blocks";
//    $sql .= " WHERE (NOT is_index = 1) AND (work_id = ".$project['id'].") AND active=1";
//    $sql .= " ORDER BY grid_row ASC, grid_col ASC, spec_order ASC";
//    $wf_blocks = $conn->query($sql);
//    while( $wf_block = $wf_blocks->fetch() ){
//
//    }
//}


//class Wf_Files_File {
//
//}
//class Wf_Files {
//
//}
//
//// Заполнение блоков
////$sql = "DELETE FROM blocks WHERE type = 'project_block'";
////$conn->query($sql);
//// Перебираем все проекты
//$sql = "SELECT * FROM projects";
//$sql .= " WHERE id > 36";
//$projects = $conn->query($sql);
//while( $project = $projects->fetch() ){
//
//    // Получаем блоки, привязанные к данному проекту
//    $cnt = 0;
//    $sql = "SELECT * FROM wf_blocks";
//    $sql .= " WHERE (NOT is_index = 1) AND (work_id = ".$project['id'].") AND active=1";
//    $sql .= " ORDER BY grid_row ASC, grid_col ASC, spec_order ASC";
//    $wf_blocks = $conn->query($sql);
//    while( $wf_block = $wf_blocks->fetch() ){ $cnt++;
//
////        $text = '';   $text_en = '';
////        if( strlen($wf_block['text']) > 0 ){
////            $text = strip_tags($wf_block['text']);
////            $text_en = strip_tags($wf_block['text_en']);
////        } else if( strlen($wf_block['html']) > 0 ){
////            $text = strip_tags($wf_block['html']);
////            $text_en = strip_tags($wf_block['html_en']);
////        }
////
////        $video_src_ru = null;
////        if( isset($wf_block['html']) && strlen($wf_block['html']) > 0 ){
////            preg_match('/src\="([^"]+)"/', $wf_block['html'], $matches, PREG_OFFSET_CAPTURE);
////            if( isset($matches[1][0]) ){
////                $video_src_ru = rawurldecode($matches[1][0]);
////            }
////        }
////        $video_src_en = null;
////        if( isset($wf_block['html_en']) && strlen($wf_block['html_en']) > 0 ){
////            preg_match('/src\="([^"]+)"/', $wf_block['html_en'], $matches, PREG_OFFSET_CAPTURE);
////            if( isset($matches[1][0]) ){
////                $video_src_en = rawurldecode($matches[1][0]);
////            }
////        }
////
////        $params = [
////            'blockable_id' => $wf_block['work_id'],
////            'blockable_type' => 'App\Models\Project',
////            'position' => $cnt,
////            'content' => json_encode([
////                'active' => $wf_block['active']==1?true:false,
////                'text' => [ 'ru' => $text, 'en' => $text_en ],
////                'gallery_style' => $wf_block['gallery_style'],
////                'video_src_ru' => $video_src_ru,
////                'video_src_en' => $video_src_en,
////            ]),
////            'type' => 'project_block',
////            'child_key' => null,
////            'parent_id' => null,
////            'video_src_ru' => $video_src_ru,
////            'video_src_en' => $video_src_en,
////            'grid_row' => $wf_block['grid_row'],
////            'grid_col' => $wf_block['grid_col'],
////        ];
////        $sql = "INSERT INTO blocks (".implode(', ', array_keys($params)).") VALUES (:".implode(', :', array_keys($params)).");";
////        $stmt = $conn->prepare($sql);
////        $stmt->execute( $params );
////
////        $block_id = $conn->lastInsertId();
////
////        echo "Блок ID=".$block_id." - создан";
//
//        ///////////////////////////////////////////
//        // Регистрация фото в БД
//        ///////////////////////////////////////////
//        $DB_model = 'blocks';
//        $DB_role = 'project_block_images';
//        $subPath = $DB_role;
//        $DBcolumnName = 'file_image';
//        ///////////////////////////////////////////
//        if(
//            isset($wf_block[$DBcolumnName])
//            &&
//            strlen($wf_block[$DBcolumnName]) > 0
//        ){
//
//            $str = $wf_block[$DBcolumnName];
//
//            preg_match('/modelName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $moduleName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/fieldName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $fieldName = isset($matches[1][0])?$matches[1][0]:false;
//
//            preg_match('/^C\:[0-9]+\:"Wf_Files"\:[0-9]+\:\{(.+)\}$/', $str, $matches, PREG_OFFSET_CAPTURE);
//            if( isset($matches[1][0]) ){
//                $unAr = unserialize($matches[1][0]);
//                if(
//                    isset($unAr['arFiles'])
//                    &&
//                    is_array($unAr['arFiles'])
//                    &&
//                    count($unAr['arFiles']) > 1
//                ){
//                    echo '<h3>Проект ID='.$project['id'].' ('.$project['slug_field'].')</h3>';
//                    echo "<div style='margin-bottom: 30px; padding-bottom: 30px; border-bottom: 1px solid black'>";
//                    $k = 0;
//                    foreach ( $unAr['arFiles'] as $obFile ){ $k++;
//                        $url = 'https://ony.ru/upload/'.$moduleName.'/'.$fieldName.'/'.$obFile->innerName.'.'.$obFile->extension;
//                        echo "<img src='".$url."' style='width: 150px; margin-right: 15px;'>";
//
//                        if( !file_exists('/var/www/ony/htdocs/images/'.$project['id'].'/') ){
//                            mkdir( '/var/www/ony/htdocs/images/'.$project['id'].'/', 0700 );
//                        }
//
//                        if( !file_exists('/var/www/ony/htdocs/images/'.$project['id'].'/'.$cnt.'/') ){
//                            mkdir( '/var/www/ony/htdocs/images/'.$project['id'].'/'.$cnt.'/', 0700 );
//                        }
//
//                        $ReadFile = fopen( $url, "rb");
//                        if ( $ReadFile ){
//                            $WriteFile = fopen( '/var/www/ony/htdocs/images/'.$project['id'].'/'.$cnt.'/'.$k.'.'.$obFile->extension, "wb" );
//                            if ( $WriteFile ){
//                                while( !feof($ReadFile) ){
//                                    fwrite($WriteFile, fread($ReadFile, 4096));
//                                }
//                                fclose($WriteFile);
//                            }
//                            fclose($ReadFile);
//                        }
//
//
//                    }
//                    echo "</div>";
//                }
//            }
//
//
//
//
//            preg_match('/originalName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $originalName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/innerName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $innerName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/fieldName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $fieldName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/extension"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $extension = isset($matches[1][0])?$matches[1][0]:false;
//
////            if(
////                strlen($moduleName) > 0
////                &&
////                strlen($originalName) > 0
////                &&
////                strlen($innerName) > 0
////                &&
////                strlen($fieldName) > 0
////                &&
////                strlen($extension) > 0
////            ){
////
////                $filePath = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$moduleName.'/'.$fieldName.'/'.$innerName.".".$extension;
////                $filePath = str_replace( '/var/www/ony/htdocs/public/upload/', '/var/www/ony/htdocs/upload/', $filePath );
////
////                $fileParams = [
////                    'moduleName' => $moduleName,
////                    'originalName' => $originalName,
////                    'innerName' => $innerName,
////                    'fieldName' => $fieldName,
////                    'extension' => $extension,
////                    'DB_role' => $DB_role,
////                    'subPath' => $subPath,
////                    ////////////////////////////////////////
////                    'DB_model' => $DB_model,
////                    'mediable_id' => $block_id,
////                    ////////////////////////////////////////
////                ];
////
////                $media_id = Media::add( $filePath, $fileParams );
////
////                echo "Блок ID=".$block_id." - медиа ID=".$media_id."<br>";
////
////            } else {
////
////                if( !$moduleName || strlen($moduleName) == 0 ){   echo '$moduleName не определён'."<br>";   }
////                if( !$originalName || strlen($originalName) == 0 ){   '$originalName не определён'."<br>";   }
////                if( !$innerName || strlen($innerName) == 0 ){   echo '$innerName не определён'."<br>";   }
////                if( !$fieldName || strlen($fieldName) == 0 ){   echo '$fieldName не определён'."<br>";   }
////                if( !$extension || strlen($extension) == 0 ){   echo '$extension не определён'."<br>";   }
////
////            }
//        }
//
////        echo "<hr>";
//    }
//}
//// Объединение блоков по заданной логике
//// Перебираем все проекты
//$sql = "SELECT * FROM projects";
//$projects = $conn->query($sql);
//while( $project = $projects->fetch() ){
//    $blocks_to_join = [];
//    // Получаем блоки проекта
//    $sql = "SELECT id,position,grid_row FROM blocks WHERE blockable_id='".$project['id']."' ORDER BY position";
//    $blocks = $conn->query($sql);
//    while( $block = $blocks->fetch() ){
//        // Определим является ли данный блок картинкой
//        $sql = "SELECT * FROM mediables WHERE mediable_type='blocks' AND mediable_id=".$block['id'];
//        $mediables = $conn->query($sql);
//        while( $mediable = $mediables->fetch() ){
//            $blocks_to_join[$block['grid_row']][] = $block['id'];
//        }
//    }
//    foreach ( $blocks_to_join as $grid_row => $blocks ){
//        if( count($blocks) > 1 ){
//            $firstBlockID = $blocks[0];
//            foreach ( $blocks as $key => $block_id ){
//                if(
//                    $key > 0
//                    &&
//                    $block_id != $firstBlockID
//                ){
//                    // Определим mediable для данного блока
//                    $sql = "SELECT id,mediable_id,mediable_type FROM mediables WHERE mediable_type='blocks' AND mediable_id=".$block_id;
//                    echo $sql."<br>";
//                    $mediables = $conn->query($sql);
//                    if( $m = $mediables->fetch() ){
//                        // Изменим привязку mediable к первому блоку
//                        $sql = "UPDATE mediables SET mediable_id = ".$firstBlockID." WHERE id = ".$m['id'];
//                        $conn->query($sql);
//                        echo $sql."<br>";
//                        // Удалим текущий блок из БД
//                        $sql = "DELETE FROM `blocks` WHERE id = ".$block_id;
//                        $conn->query($sql);
//                        echo $sql."<br>";
//                    }
//                }
//            }
//        }
//    }
//    $blocks_to_join = [];   $blocksContents = [];
//    $imageBlocks = [];   $videoBlocks = [];
//    // Получаем блоки проекта
//    $sql = "SELECT id,position,content,grid_row FROM blocks WHERE blockable_id='".$project['id']."' ORDER BY position";
//    $blocks = $conn->query($sql);
//    while( $block = $blocks->fetch() ){
//        $content = \App\Classes\Helper::json_to_array($block['content']);
//        $blocksContents[$block['id']] = $content;
//        // Определим является ли данный блок картинкой
//        $sql = "SELECT * FROM mediables WHERE mediable_type='blocks' AND mediable_id=".$block['id'];
//        $mediables = $conn->query($sql);
//        if( $mediable = $mediables->fetch() ){
//            $blocks_to_join[$block['grid_row']][] = $block['id'];
//            $blocks_to_join[$block['grid_row']] = array_unique($blocks_to_join[$block['grid_row']]);
//            $imageBlocks[$block['id']] = $block['id'];
//        }
//        if(
//            isset($content['project_block_video_mp4_link'])
//            &&
//            strlen($content['project_block_video_mp4_link']) > 0
//        ){
//            preg_match(
//                Project::VIMEO_LINK_REGEX,
//                $content['project_block_video_mp4_link'],
//                $matches, PREG_OFFSET_CAPTURE
//            );
//            if( isset( $matches[2][0] ) ){
//                $blocks_to_join[$block['grid_row']][] = $block['id'];
//                $blocks_to_join[$block['grid_row']] = array_unique($blocks_to_join[$block['grid_row']]);
//                $videoBlocks[$block['id']] = $block['id'];
//            }
//        }
//    }
//    foreach ( $blocks_to_join as $grid_row => $blocksIDS ){
//        if( count($blocksIDS) > 1 ){
//            //echo "<pre>"; print_r( $blocksIDS ); echo "</pre>";
//            $imageBlockID = null;   $videoBlockID = null;
//            foreach ( $blocksIDS as $blockID ){
//                if( isset($imageBlocks[$blockID]) ){   $imageBlockID = $blockID;   }
//                if( isset($videoBlocks[$blockID]) ){   $videoBlockID = $blockID;   }
//            }
//            if( isset($imageBlockID) && isset($videoBlockID) ){
//                //echo "<pre>"; print_r( 'Image block - '.$imageBlockID ); echo "</pre>";
//                //echo "<pre>"; print_r( 'Video block - '.$videoBlockID ); echo "</pre>";
//                // Добавим в блок с картинкой ссылку на видео Vimeo
//                // взятую с блока с видео
//                $imageBlockContent = $blocksContents[$imageBlockID];
//                $videoBlockContent = $blocksContents[$videoBlockID];
//                $imageBlockContent['project_block_video_mp4_link'] = $videoBlockContent['project_block_video_mp4_link'];
//                $imageBlockContentJSON = json_encode($imageBlockContent, JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
//                $sql = "UPDATE blocks SET content = '".$imageBlockContentJSON."' WHERE id = ".$imageBlockID;
//                $conn->query($sql);
//                echo $sql."<br>";
//                // Блок с видео удалим
//                $sql = "DELETE FROM `blocks` WHERE id = ".$videoBlockID;
//                $conn->query($sql);
//                echo $sql."<br>";
//            }
//            echo "<hr>";
//        }
//    }
//}





// Скачивание удалённых видео и привязка их к блокам
//$sql = "SELECT id,content,video_src_ru FROM blocks WHERE id BETWEEN 38871 AND 39584";
//$blocks = $conn->query($sql);
//while( $block = $blocks->fetch() ){
//
//    $content = \App\Classes\Helper::json_to_array($block['content']);
//
//    if( strlen($block['video_src_ru']) > 0 ){
//
//        echo "Блок id = ".$block['id']."<br>";
//
//        preg_match("/player\.vimeo\.com\/video\/([0-9]+)/", $block['video_src_ru'], $matches, PREG_OFFSET_CAPTURE);
//
//        // Если это ссылка на плеер Vimeo
//        if( isset($matches[1][0]) ){
//
//            $vimeo_video_id = $matches[1][0];
//            $videoInfo = VimeoHelper::infoByURL( '/videos/'.$vimeo_video_id );
//
//            if( isset($videoInfo['status']) && $videoInfo['status'] == 200 ){
//
//                $status = 'in_progress';
//                if( isset($videoInfo['body']['transcode']['status']) ){
//                    $status = $videoInfo['body']['transcode']['status'];
//                }
//                $arInfo = [
//                    'uri' => $videoInfo['body']['uri'],
//                    'link' => $videoInfo['body']['link'],
//                    'name' => $videoInfo['body']['name'],
//                    'description' => $videoInfo['body']['description'],
//                    'pictures' => $videoInfo['body']['pictures'],
//                    'status' => $status,
//                    'upload_timestamp' => time(),
//                ];
//                if(
//                    isset($videoInfo['body']['files'])
//                    &&
//                    is_array($videoInfo['body']['files'])
//                    &&
//                    count($videoInfo['body']['files']) > 0
//                ){
//                    $arInfo['files'] = $videoInfo['body']['files'];
//                }
//
//                // Сохранение инфы по видео в БД
//                $content['project_block_video_mp4_link'] = $videoInfo['body']['link'];
//                Project::saveVimeoInfo( $vimeo_video_id, $arInfo );
//
//                echo "player.vimeo ЗАПИСАНО - Блок id = ".$block['id']."<br>";
//
//            }
//
//        } else {
//
//            $res = \App\Classes\MyFile::downloadExternalFile(
//                $block['video_src_ru'],
//                'project_blocks_videos'
//            );
//
//            if( $res['status'] == 'ok' ){
//
//                $arParams = [
//                    'uuid' => str_replace(
//                        '/var/www/ony/htdocs/storage/app/public/file_uploads/', '',
//                        $res['file_path']
//                    ),
//                    'filename' => $res['fileName'],
//                    'size' => $res['fileSize'],
//                ];
//
//                $sql = "INSERT INTO files (created_at, updated_at, deleted_at, uuid, size, filename) VALUES(NOW(), NOW(), NULL, :uuid, :size, :filename)";
//                $stmt = $conn->prepare($sql);
//                $stmt->execute( $arParams );
//
//                $file_id = $conn->lastInsertId();
//
//                if( intval($file_id) > 0 ){
//
//                    $arParams = [
//                        'fileable_id' => $block['id'],
//                        'fileable_type' => 'blocks',
//                        'file_id' => $file_id,
//                        'role' => 'project_block_video_mp4',
//                        'locale' => "ru"
//                    ];
//
//                    $sql = "INSERT INTO fileables (created_at, updated_at, deleted_at, fileable_id, fileable_type, file_id, role, locale) VALUES(NOW(), NOW(), NULL, :fileable_id, :fileable_type, :file_id, :role, :locale )";
//
//                    $stmt = $conn->prepare($sql);
//                    $stmt->execute( $arParams );
//
//                    echo "Блок ID=".$block['id']." - файл ID=".$file_id."<br>";
//
//                } else {
//
//                    unlink($res['file_path']);
//                    rmdir($res['file_path']);
//
//                    echo "Блок ID=".$block['id']." - НЕ УДАЛОСЬ ПРИВЯЗАТЬ"."<br>";
//                }
//
//            } else {
//
//                echo "Блок ID=".$block['id']." - ".strtoupper($res['text'])."<br>";
//            }
//
//        }
//    }
//
//    DB::table('blocks')
//    ->where('id', $block['id'])
//    ->update([
//        'content' => json_encode($content, JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE)
//    ]);
//}



// Загрузка видео из проектов на Vimeo
//$sql = "SELECT * FROM projects WHERE id BETWEEN 126 AND 126";
//$projects = $conn->query($sql);
//while( $project = $projects->fetch() ){
//    Project::uploadVideosToVimeo( $project['id'] );
//}



//    $sql = "SELECT b.id,v.vimeo_videos_json FROM blocks b";
//    $sql .= " LEFT JOIN blocks_vimeo_videos v ON v.block_id = b.id";
//    $sql .= " WHERE CHAR_LENGTH(v.vimeo_videos_json)>2";
//    $blocks = $conn->query($sql);
//    while( $block = $blocks->fetch() ){
//
//        echo "<pre>"; print_r( $block ); echo "</pre>";
//
//    }


//$current_section_id = 34;
//$new_section_id = 33;
//$sql = "SELECT * FROM projects WHERE section_id = ".$current_section_id;
//$projects = $conn->query($sql);
//while( $project = $projects->fetch() ){
//    $sql = "UPDATE projects SET section_id = ".$new_section_id." WHERE id = ".$project['id'];
//    $conn->query($sql);
//}

//$sql = "SELECT * FROM project_translations";
//$items = $conn->query($sql);
//while( $item = $items->fetch() ){
//    $sql = "UPDATE project_translations SET description = \"".strip_tags($item['description'])."\" WHERE id = ".$item['id'];
//    $conn->query($sql);
//}

//    $project_direction_id = 2;
//    $project_list_id = 1;
//
//    $last_position = 0;   $list_projects = [];
//    $sql = "SELECT * FROM project_project_list WHERE project_list_id = ".$project_list_id;
//    $sql .= " ORDER BY position ASC";
//    $list_items = $conn->query($sql);
//    while( $list_item = $list_items->fetch() ){
//        $last_position = $list_item['position'];
//        $list_projects[] = $list_item['project_id'];
//    }
//    echo "<pre>"; print_r( $list_projects ); echo "</pre>";
//
//    $sql = "SELECT * FROM projects WHERE project_direction_id = ".$project_direction_id;
//    $projects = $conn->query($sql);
//    while( $project = $projects->fetch() ){
//        if( !in_array($project['id'], $list_projects) ){
//            $list_projects[] = $project['id'];
//            $last_position++;
//            $params = [
//                'position' => $last_position,
//                'project_id' => $project['id'],
//                'project_list_id' => $project_list_id
//            ];
//            $sql = "INSERT project_project_list (position, project_id, project_list_id) VALUES(:position, :project_id, :project_list_id)";
//            $stmt = $conn->prepare($sql);
//            $stmt->execute( $params );
//        }
//    }
//    echo "<pre>"; print_r( $list_projects ); echo "</pre>";
//    echo "<pre>"; print_r( $last_position ); echo "</pre>";


//    $project_list_id = 2;   $pos = 0;
//    $sql = "SELECT id, project_id, project_list_id, position FROM project_project_list";
//    $sql .= " WHERE project_list_id = ".$project_list_id;
//    $sql .= " ORDER BY project_id DESC";
//    $items = $conn->query($sql);
//    while( $item = $items->fetch() ){
//        echo "<pre>"; print_r( $item ); echo "</pre>";
//        $pos++;
//        $sql = "UPDATE project_project_list SET position = ".$pos." WHERE id = ".$item['id'];
//        $conn->query($sql);
//        echo "<pre>"; print_r( $sql ); echo "</pre>";
//    }


//    Project::updateVimeoVideosStatuses(true);
//    die();


//    // Создание новых обложек для видео Vimeo (time = 0)
//    $sql = "SELECT * FROM vimeo_videos";
//    $sql .= " WHERE vimeo_id='63240092'";
//    //$sql .= " WHERE id BETWEEN 571 AND 590";
//    $items = $conn->query($sql);
//    while( $item = $items->fetch() ){
//        // Распаковка JSON
//        $dbVideoInfo = Helper::json_to_array( $item['json'] );
//        if( isset($dbVideoInfo['uri']) ){
//
//            // Получим актуальную инфу по видео
//            $guzzleClient = new Client([ 'timeout' => 10 ]);
//            try {
//                $response = $guzzleClient->request(
//                    'GET',
//                    'https://api.vimeo.com'.$dbVideoInfo['uri'],
//                    [
//                        'headers' => [
//                            'Authorization' => 'bearer 72d38df901d3d69f39b36755d973247b',
//                            'Accept' => 'application/vnd.vimeo.*+json;version=3.4'
//                        ]
//                    ]
//                );
//                $code = $response->getStatusCode();
//                if( $code == 200 ){
//
//                    $body = $response->getBody();
//                    $arVideo = Helper::json_to_array( $body->getContents() );
//
//                    // URL для запроса инфы по обложкам
//                    $picsURI = $arVideo['metadata']['connections']['pictures']['uri'];
//
//                    // Запрос всех обложек видео
//                    $guzzleClient = new Client([ 'timeout' => 10 ]);
//                    try {
//                        $response = $guzzleClient->request(
//                            'GET',
//                            'https://api.vimeo.com'.$picsURI,
//                            [
//                                'headers' => [
//                                    'Authorization' => 'bearer 72d38df901d3d69f39b36755d973247b',
//                                    'Accept' => 'application/vnd.vimeo.*+json;version=3.4'
//                                ]
//                            ]
//                        );
//                        $code = $response->getStatusCode();
//                        if( $code == 200 ){
//
//                            $response->getStatusCode();
//                            $body = $response->getBody();
//
//                            $arPictures = Helper::json_to_array( $body->getContents() );
//
//                            echo "<pre>"; print_r( $arPictures ); echo "</pre>";
//
//                            // Деактивируем активную на данный момент обложку
////                            if(
////                                isset($arPictures['data'])
////                                &&
////                                is_array($arPictures['data'])
////                                &&
////                                count($arPictures['data']) > 0
////                            ){
////                                foreach ( $arPictures['data'] as $arPicture ){
////                                    if( $arPicture['active'] == 1 ){
////                                        $guzzleClient = new Client([ 'timeout' => 10 ]);
////                                        try {
////                                            $response = $guzzleClient->request(
////                                                'PATCH',
////                                                'https://api.vimeo.com'.$arPicture['uri'],
////                                                [
////                                                    'headers' => [
////                                                        'Authorization' => 'bearer 72d38df901d3d69f39b36755d973247b',
////                                                        'Content-Type' => 'application/json',
////                                                        'Accept' => 'application/vnd.vimeo.*+json;version=3.4'
////                                                    ],
////                                                    'body' => '{"active":false}'
////                                                ]
////                                            );
////                                        } catch(RequestException $ex){
////                                            echo $ex->getMessage();
////                                        }
////                                    }
////                                }
////                            }
////
////                            // Создаём новую обложку
////                            $guzzleClient = new Client([ 'timeout' => 10 ]);
////                            try {
////                                $response = $guzzleClient->request(
////                                    'POST',
////                                    'https://api.vimeo.com'.$picsURI,
////                                    [
////                                        'headers' => [
////                                            'Authorization' => 'bearer 72d38df901d3d69f39b36755d973247b',
////                                            'Content-Type' => 'application/json',
////                                            'Accept' => 'application/vnd.vimeo.*+json;version=3.4'
////                                        ],
////                                        'body' => '{"active":true, "time":0}'
////                                    ]
////                                );
////                            } catch(RequestException $ex){
////                                echo $ex->getMessage();
////                            }
//
//                        }
//                    } catch(RequestException $ex){
//                        echo $ex->getMessage();
//                    }
//                }
//            } catch(RequestException $ex){
//                echo $ex->getMessage();
//            }
//        }
//    }





});





Route::get('/fileActions', function () {

//    $res = App\Classes\VimeoHelper::infoByURL( '/videos/397929056' );
//    dd($res);





    die();

//    ///////////////////////////////////////////
//    $DB_model = 'App\Models\Project';
//    $DB_role = 'video_mobile_mp4';
//    $subPath = 'project_'.$DB_role;
//    $DBcolumnName = 'file_video_mobile_mp4';
//    ///////////////////////////////////////////
//
//    $conn = Site::getPDO();
//
//    $sql = "SELECT * FROM projects";
//    //$sql .= " WHERE id=125";
//    $result = $conn->query($sql);
//    while( $project = $result->fetch() ){
//
//        if(
//            isset($project[$DBcolumnName])
//            &&
//            strlen($project[$DBcolumnName])
//        ){
//
//            $str = $project[$DBcolumnName];
//
//            preg_match('/modelName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $moduleName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/originalName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $originalName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/innerName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $innerName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/fieldName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $fieldName = isset($matches[1][0])?$matches[1][0]:false;
//            preg_match('/extension"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
//            $extension = isset($matches[1][0])?$matches[1][0]:false;
//
//            if(
//                strlen($moduleName) > 0
//                &&
//                strlen($originalName) > 0
//                &&
//                strlen($innerName) > 0
//                &&
//                strlen($fieldName) > 0
//                &&
//                strlen($extension) > 0
//            ){
//
//                $filePath = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$moduleName.'/'.$fieldName.'/'.$innerName.".".$extension;
//                $filePath = str_replace( '/var/www/ony/htdocs/public/upload/', '/var/www/ony/htdocs/upload/', $filePath );
//
//                $fileParams = [
//                    'moduleName' => $moduleName,
//                    'originalName' => $originalName,
//                    'innerName' => $innerName,
//                    'fieldName' => $fieldName,
//                    'extension' => $extension,
//                    'DB_role' => $DB_role,
//                    'subPath' => $subPath,
//                    ////////////////////////////////////////
//                    'DB_model' => $DB_model,
//                    'fileable_id' => $project['id'],
//                    ////////////////////////////////////////
//                ];
//
//                $file_id = MyFile::add( $filePath, $fileParams );
//
//                echo "Проект ID=".$project['id']." - файл ID=".$file_id."<br>";
//
//            } else {
//
//                if( !$moduleName || strlen($moduleName) == 0 ){   echo '$moduleName не определён'."<br>";   }
//                if( !$originalName || strlen($originalName) == 0 ){   '$originalName не определён'."<br>";   }
//                if( !$innerName || strlen($innerName) == 0 ){   echo '$innerName не определён'."<br>";   }
//                if( !$fieldName || strlen($fieldName) == 0 ){   echo '$fieldName не определён'."<br>";   }
//                if( !$extension || strlen($extension) == 0 ){   echo '$extension не определён'."<br>";   }
//
//            }
//
//            echo "<hr>";
//        }
//    }


});




Route::get('/mediaActions', function () {

    die();

    ///////////////////////////////////////////
    // Регистрация фото в БД
    ///////////////////////////////////////////
    $DB_model = 'App\Models\Project';
    $DB_role = 'block_image';
    $subPath = 'project_'.$DB_role;
    $DBcolumnName = 'file_block_image';
    ///////////////////////////////////////////

    $conn = Site::getPDO();

    // Непосредственно из таблицы проектов
    $sql = "SELECT * FROM projects";

    // Из таблицы блоков
//    $fields = [ 'p.id', 'b.is_index', 'b.file_image' ];
//    $sql = "SELECT ".implode(",", $fields)." FROM projects p";
//    $sql .= " LEFT JOIN wf_blocks b ON p.id = b.work_id AND b.is_index=1";
//    //$sql .= " WHERE p.id=125";

    $result = $conn->query($sql);
    while( $project = $result->fetch() ){

        if(
            isset($project[$DBcolumnName])
            &&
            strlen($project[$DBcolumnName])
        ){

            $str = $project[$DBcolumnName];

            preg_match('/modelName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
            $moduleName = isset($matches[1][0])?$matches[1][0]:false;
            preg_match('/originalName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
            $originalName = isset($matches[1][0])?$matches[1][0]:false;
            preg_match('/innerName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
            $innerName = isset($matches[1][0])?$matches[1][0]:false;
            preg_match('/fieldName"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
            $fieldName = isset($matches[1][0])?$matches[1][0]:false;
            preg_match('/extension"[^"]+"([^"]+)"/', $str, $matches, PREG_OFFSET_CAPTURE);
            $extension = isset($matches[1][0])?$matches[1][0]:false;

            if(
                strlen($moduleName) > 0
                &&
                strlen($originalName) > 0
                &&
                strlen($innerName) > 0
                &&
                strlen($fieldName) > 0
                &&
                strlen($extension) > 0
            ){

                $filePath = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$moduleName.'/'.$fieldName.'/'.$innerName.".".$extension;
                $filePath = str_replace( '/var/www/ony/htdocs/public/upload/', '/var/www/ony/htdocs/upload/', $filePath );

                $fileParams = [
                    'moduleName' => $moduleName,
                    'originalName' => $originalName,
                    'innerName' => $innerName,
                    'fieldName' => $fieldName,
                    'extension' => $extension,
                    'DB_role' => $DB_role,
                    'subPath' => $subPath,
                    ////////////////////////////////////////
                    'DB_model' => $DB_model,
                    'mediable_id' => $project['id'],
                    ////////////////////////////////////////
                ];

                $media_id = Media::add( $filePath, $fileParams );

                echo "Проект ID=".$project['id']." - фото ID=".$media_id."<br>";

            } else {

                if( !$moduleName || strlen($moduleName) == 0 ){   echo '$moduleName не определён'."<br>";   }
                if( !$originalName || strlen($originalName) == 0 ){   '$originalName не определён'."<br>";   }
                if( !$innerName || strlen($innerName) == 0 ){   echo '$innerName не определён'."<br>";   }
                if( !$fieldName || strlen($fieldName) == 0 ){   echo '$fieldName не определён'."<br>";   }
                if( !$extension || strlen($extension) == 0 ){   echo '$extension не определён'."<br>";   }

            }

            echo "<hr>";
        }
    }


});






