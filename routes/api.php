<?php

use App\Classes\Media;
use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Classes\Helper;
use App\Models\Project;
use App\Models\Client;
use App\Models\ProjectList;
use App\Models\ProjectCategory;
use App\Models\ClientCategory;
use App\Models\AboutPage;
use App\Models\HomePage;
use App\Models\ContactPage;
use App\Models\ServicesPage;
use App\Models\Shell;
use App\Classes\MySetting;
use App\Transformers\ProjectTransformer;
use App\Transformers\ProjectListsTransformer;
use App\Transformers\ProjectCategoriesTransformer;
use App\Transformers\ClientCategoriesTransformer;
use App\Transformers\HomePageTransformer;
use App\Transformers\ProjectsTransformer;
use App\Transformers\ContactsTransformer;
use App\Transformers\AboutPageTransformer;
use App\Transformers\ShellPageTransformer;
use App\Transformers\ContactPageTransformer;
use App\Transformers\ServicesPageTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Illuminate\Http\JsonResponse;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});





// Проекты (список)
Route::get('/v1/projects', function(Request $request) {

    $lang = Helper::lang();

    // Применяем общее кэширование на всю выборку
    // согласовано 21.04.2020
    $cachHash = md5($lang.'_'.json_encode($_GET));
    $cacheID = 'api_projects_'.$cachHash;
    if ( !Cache::has($cacheID) ){

        // Получим перечень проектов
        $filter = [
            [ 'published', 1 ]
        ];
        $projects = Project::where($filter)->orderBy('id', 'desc')->get();
        // Фильтрация проектов по списку + сортировка в порядке списка
        if( isset($_GET['list_slug']) && strlen($_GET['list_slug']) > 0 ){
            $projects = Project::filterByListSlug( $projects, $_GET['list_slug'] );
        }
        if( isset($_GET['client_type']) && strlen($_GET['client_type']) > 0 ){
            $projects = Project::filterByClientType( $projects, $_GET['client_type'] );
        }
        if( isset($_GET['work_type']) && strlen($_GET['work_type']) > 0 ){
            $projects = Project::filterByWorkType( $projects, $_GET['work_type'] );
        }

        $projectsFractal = Fractal::create( $projects, new ProjectsTransformer() );
        // Перечень списков проектов
        $projectLists = ProjectList::where('published', 1)->orderBy('position', 'asc')->get();
        $projectListsFractal = Fractal::create( $projectLists, new ProjectListsTransformer() );
        // Перечень категорий проектов
        $projectCategories = ProjectCategory::where('published', 1)->orderBy('position', 'asc')->get();
        $projectCategoriesFractal = Fractal::create( $projectCategories, new ProjectCategoriesTransformer() );
        // Перечень категорий клиентов
        $clientCategories = ClientCategory::where('published', 1)->orderBy('position', 'asc')->get();
        $clientCategoriesFractal = Fractal::create( $clientCategories, new ClientCategoriesTransformer() );
        /////////////////////
        $project_listsNames = [ 'ru' => 'Списки проектов', 'en' => 'Project lists' ];
        $client_typesNames = [ 'ru' => 'Тип клиента', 'en' => 'Type of client' ];
        $work_typesNames = [ 'ru' => 'Тип работ', 'en' => 'Type of work' ];

        $arResult = [
            'data' => [
                'query_parameters' => [
                    'project_lists' => [
                        'title' => !is_null($project_listsNames[$lang])?$project_listsNames[$lang]:$project_listsNames['ru'],
                        "query_parameter" => "list_slug",
                        'items' => $projectListsFractal->toArray()['data'],
                    ],
                    'client_types' => [
                        'title' => !is_null($client_typesNames[$lang])?$client_typesNames[$lang]:$client_typesNames['ru'],
                        "query_parameter" => "client_type",
                        'items' => $clientCategoriesFractal->toArray()['data'],
                    ],
                    'work_types' => [
                        'title' => !is_null($work_typesNames[$lang])?$work_typesNames[$lang]:$work_typesNames['ru'],
                        "query_parameter" => "work_type",
                        'items' => $projectCategoriesFractal->toArray()['data'],
                    ],
                ],
                'projects' => $projectsFractal->toArray()['data'],
            ]
        ];
        Cache::put($cacheID, $arResult, 1);
    } else {
        $arResult = Cache::get($cacheID);
    }
    //return $arResult;
    return Response::json( $arResult, 200, [], JSON_UNESCAPED_UNICODE );
});



// Проект детально
Route::get('/v1/projects/{project_code}', function( Request $request, $project_code ) {

    $project = Project::where( 'slug_field', $project_code )->first();
    if( $project ){

        if (
            isset($project->login) && strlen($project->login) > 0
            &&
            isset($project->password) && strlen($project->password) > 0
        ){

            $headers = getallheaders();
            $shellPage = DB::table('shells')->first();
            if(
                isset( $shellPage->api_secret_key )
                &&
                strlen( $shellPage->api_secret_key ) > 0
            ){   $api_secret_key = $shellPage->api_secret_key;   }

            if(
                isset($headers['x-front-access-key'])
                &&
                isset($api_secret_key)
                &&
                $headers['x-front-access-key'] == $api_secret_key
            ){

                $projectFractal = Fractal::create( $project, new ProjectTransformer() );
                $projectInfo = $projectFractal->toArray()['data'];
                $projectInfo['login'] = $project->login;
                $projectInfo['password'] = $project->password;

                //return $projectInfo;
                return Response::json( $projectInfo, 200, [], JSON_UNESCAPED_UNICODE );

            } else {

                if ( !isset($_SERVER['PHP_AUTH_USER']) ){

                    header('WWW-Authenticate: Basic realm="Need authorization"');
                    header('HTTP/1.0 401 Unauthorized');
                    exit;

                } else {

                    $valid_passwords = [ $project->login => $project->password ];
                    $valid_users = array_keys($valid_passwords);

                    $user = $_SERVER['PHP_AUTH_USER'];
                    $pass = $_SERVER['PHP_AUTH_PW'];

                    $validated = in_array($user, $valid_users) && $pass == $valid_passwords[$user];

                    if ( $validated ){

                        $projectFractal = Fractal::create( $project, new ProjectTransformer() );
                        //return $projectFractal->toArray()['data'];
                        return Response::json( $projectFractal->toArray()['data'], 200, [], JSON_UNESCAPED_UNICODE );

                    } else {

                        header('WWW-Authenticate: Basic realm="My Realm"');
                        header('HTTP/1.0 401 Unauthorized');
                        die ("Ошибка авторизации!");
                    }
                }

            }

        } else {

            $projectFractal = Fractal::create( $project, new ProjectTransformer() );
            //return $projectFractal->toArray()['data'];
            return Response::json( $projectFractal->toArray()['data'], 200, [], JSON_UNESCAPED_UNICODE );
        }

    } else {
        $project = [];
        $projectFractal = Fractal::create( $project, new ProjectTransformer() )
        ->respond(function(JsonResponse $response) {
            $response->setStatusCode(404);
        });
        $projectFractal->setData(null);
        //return $projectFractal;
        return Response::json( $projectFractal, 200, [], JSON_UNESCAPED_UNICODE );
    }
});



// Главная страница
Route::get('/v1/home', function( Request $request ){
    $homePage = HomePage::where('published', 1)->orderBy('position')->first();
    if( isset($homePage) ){
        $homePageFractal = Fractal::create( $homePage, new HomePageTransformer() );
        //return $homePageFractal->toArray()['data'][0];
        return Response::json( $homePageFractal->toArray()['data'][0], 200, [], JSON_UNESCAPED_UNICODE );
    }
});



// About
Route::get('/v1/about', function( Request $request ){
    $aboutPage = AboutPage::where('published', 1)->orderBy('position')->first();
    if( isset($aboutPage) ){
        $aboutFractal = Fractal::create( $aboutPage, new AboutPageTransformer() );
        //return $aboutFractal->toArray()['data'][0];
        return Response::json( $aboutFractal->toArray()['data'][0], 200, [], JSON_UNESCAPED_UNICODE );
    }
});


// Services
Route::get('/v1/services', function( Request $request ){
    $servicesPage = ServicesPage::where('published', 1)->orderBy('position')->first();
    if( isset($servicesPage) ){
        $servicesFractal = Fractal::create( $servicesPage, new ServicesPageTransformer() );
        //return $servicesFractal->toArray()['data'][0];
        return Response::json( $servicesFractal->toArray()['data'][0], 200, [], JSON_UNESCAPED_UNICODE );
    }
});


// Контакты
Route::get('/v1/contacts', function( Request $request ){
    $contactPage = ContactPage::where('published', 1)->orderBy('position')->first();
    if( isset($contactPage) ){
        $contactsFractal = Fractal::create( $contactPage, new ContactPageTransformer() );
        //return $contactsFractal->toArray()['data'];
        return Response::json( $contactsFractal->toArray()['data'][0], 200, [], JSON_UNESCAPED_UNICODE );
    }
});



// Shell
Route::get('/v1/shell', function( Request $request ){
    $shellPage = Shell::where('published', 1)->orderBy('position')->first();
    if( isset($shellPage) ){
        $shellFractal = Fractal::create( $shellPage, new ShellPageTransformer() );
        //return $shellFractal->toArray()['data'][0];
        return Response::json( $shellFractal->toArray()['data'][0], 200, [], JSON_UNESCAPED_UNICODE );
    }
});


