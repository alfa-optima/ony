<?php

namespace App\Classes;
use App\Classes\Site;
use Illuminate\Support\Facades\DB;


class MyFile {

    const TABLE_NAME = 'files';

    const UPLOAD_PATH = '/var/www/ony/htdocs/storage/app/public/file_uploads';

    static $contentTypes = [];




    static function add( $filePath, $fileParams ){

        if( !file_exists( static::UPLOAD_PATH ) ){
            mkdir(static::UPLOAD_PATH, 0700);
        }

        $filePath = trim(strip_tags($filePath));
        if( file_exists($filePath) ){

            $fileInfo = pathinfo($filePath);
            $filePathDir = str_replace("/".$fileInfo['basename'], "", $filePath);

            $hash = md5( $filePath.rand(1, 1000000).time() );
            $newPath = static::UPLOAD_PATH.'/'.$hash;
            $uuid = $hash."/".$fileParams['originalName'];

            $subPath = false;
            if( strlen($fileParams['subPath']) > 0 ){
                $subPath = static::UPLOAD_PATH.'/'.$fileParams['subPath'];
                $newPath = static::UPLOAD_PATH.'/'.$fileParams['subPath'].'/'.$hash;
                $uuid = $fileParams['subPath'].'/'.$hash."/".$fileParams['originalName'];
                if( !file_exists($subPath) ){
                    mkdir($subPath, 0700);
                }
            }

            $newFilePath = $newPath."/".$fileParams['originalName'];
            $fileSize = filesize($filePath);

            mkdir($newPath, 0700);

            if( copy($filePath, $newFilePath) ){

                $conn = Site::getPDO();

                $arParams = [
                    'uuid' => $uuid,
                    'filename' => $fileParams['originalName'],
                    'size' => $fileSize,
                ];

                $sql = "INSERT INTO ".static::TABLE_NAME." (created_at, updated_at, deleted_at, uuid, size, filename) VALUES(NOW(), NOW(), NULL, :uuid, :size, :filename)";
                $stmt = $conn->prepare($sql);
                $stmt->execute( $arParams );

                $file_id = $conn->lastInsertId();

                if( intval($file_id) > 0 ){

                    $arParams = [
                        'fileable_id' => $fileParams['fileable_id'],
                        'fileable_type' => $fileParams['DB_model'],
                        'file_id' => $file_id,
                        'role' => $fileParams['DB_role'],
                        'locale' => "ru"
                    ];

                    $sql = "INSERT INTO fileables (created_at, updated_at, deleted_at, fileable_id, fileable_type, file_id, role, locale) VALUES(NOW(), NOW(), NULL, :fileable_id, :fileable_type, :file_id, :role, :locale )";

                    $stmt = $conn->prepare($sql);
                    $stmt->execute( $arParams );

                    return $file_id;

                } else {

                    unlink($newPath);
                    if( $subPath ){    unlink($subPath);    }
                }
            } else {

                unlink($newPath);
                if( $subPath ){    unlink($subPath);    }
            }
        }
        return false;
    }





    static function downloadExternalFile( $url, $subPath ){

        $url = Helper::pureURL($url);

        if( !file_exists( '/var/www/ony/htdocs/storage/app/public/file_uploads' ) ){
            mkdir($_SERVER['DOCUMENT_ROOT'].'/var/www/ony/htdocs/storage/app/public/file_uploads', 0700);
        }
        $path = '/var/www/ony/htdocs/storage/app/public/file_uploads/'.$subPath;
        if( !file_exists( $path ) ){    mkdir($path, 0700);    }

        $urlHeaders = @get_headers( $url );

        $fileSize = false;
        $contentType = false;

        if( strpos($urlHeaders[0], '200') ){

            $fileSize = static::getFileSize($urlHeaders);

            if( intval($fileSize) > 0 ){

                $contentType = static::getContentType($urlHeaders);

                $hash = md5( $url.rand(1, 1000000).time() );
                $path = $path.'/'.$hash;

                if( preg_match("/video\/([a-z0-9]+)/", $contentType, $matches, PREG_OFFSET_CAPTURE) ){
                    $ext = $matches[1][0];
                } else {
                    $ext = 'mp4';
                }

                $file_name = md5(time().rand(1, 1000000));
                $filePath = $path.'/'.$file_name.".".$ext;

                mkdir($path, 0700);

                $ReadFile = fopen( $url, "rb");
                if ( $ReadFile ){
                    $WriteFile = fopen( $filePath, "wb" );
                    if ( $WriteFile ){
                        while( !feof($ReadFile) ){
                            fwrite($WriteFile, fread($ReadFile, 4096));
                        }
                        fclose($WriteFile);
                    }
                    fclose($ReadFile);
                }

                if( file_exists($filePath) ){

                    return [
                        'status' => 'ok',
                        'file_path' => $filePath,
                        'file_dir' => $path,
                        'fileSize' => $fileSize,
                        'fileName' => $file_name.'.'.$ext,
                    ];

                } else {

                    rmdir($path);

                    return [
                        'status' => 'error',
                        'text' => 'Ошибка загрузки файла'
                    ];
                }
            } else {

                return [
                    'status' => 'error',
                    'text' => 'Размер файла - '.$fileSize
                ];
            }
        } else {

            return [
                'status' => 'error',
                'text' => 'Код ответа - '.$urlHeaders[0]
            ];
        }
        return [
            'status' => 'error',
            'text' => 'Данный файл скачать не получится'
        ];
    }




    static function getFileSize( $headers ){
        foreach ( $headers as $key => $header ){
            if( substr_count($header, 'Content-Length') ){
                $matchesRes = preg_match("/^Content-Length\: (.*)$/", $header, $matches);
                if( $matchesRes ){
                    return $matches[1];
                }
            }
        }
        return false;
    }

    static function getContentType( $headers ){
        foreach ( $headers as $key => $header ){
            if( substr_count($header, 'Content-Type') ){
                $matchesRes = preg_match("/^Content-Type\: (.*)$/", $header, $matches);
                if( $matchesRes ){
                    return $matches[1];
                }
            }
        }
        return false;
    }



    static function bytesToMB( $bytes ){
        return round($bytes/1024/1024, 1);
    }



    static function getFileByID($file_id){
        $file = DB::table('files')->where('id', $file_id)->first();
        return $file;
    }



    static function outputFileByID( $file_id ){
        $obFile = static::getFileByID($file_id);
        if( !is_null($obFile) ){
            $file_path = __DIR__ . '/../../storage/app/public/file_uploads/'.$obFile->uuid;
            if( file_exists( $file_path ) ){
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $file_type = finfo_file($finfo, $file_path);
                finfo_close($finfo);
                $fp = fopen($file_path, 'rb');
                header("Content-Type: " . $file_type );
                header("Content-Length: " . $obFile->size);
                fpassthru($fp);
                exit;
            }
        }
        abort(404);
    }



    static function outputFileByIncorrectLink( $uuid ){
        $file_path = __DIR__ . '/../../storage/app/public/file_uploads/'.$uuid;
        if( file_exists( $file_path ) ){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $file_type = finfo_file($finfo, $file_path);
            finfo_close($finfo);
            $fp = fopen($file_path, 'rb');
            header("Content-Type: " . $file_type );
            header("Content-Length: " . filesize($file_path));
            fpassthru($fp);
            exit;
        }
        abort(404);
    }



}
