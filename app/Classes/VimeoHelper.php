<?php

namespace App\Classes;
use App\Classes\Site;

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoRequestException;



class VimeoHelper {


    const CLIENT_ID = '558c6fba520b0a334ec68cd300611ae55bbede42';
    const SECRET = 'IFNf352tOW8Ft52Ikn7GR3dtwi/DO5ZRUcd0NsgCEzzZbXWLGvdWkWo7AuCf9ztC86MXozmP2PGXc/LNt9R7hv7kd69xj7N3Fz1yf1hmrshOPrsTI2GCG4Jukln+MAtC';
    const TOKEN = '72d38df901d3d69f39b36755d973247b';



    static function uploadVideo( $file_path, $name, $description ){
        if( file_exists( $file_path ) ){
            try {
                $client = new Vimeo(static::CLIENT_ID, static::SECRET, static::TOKEN);
                $video_url = $client->upload($file_path, array(
                    "name" => $name,
                    "description" => $description,
                    "privacy" => [ "embed" => "private" ]
                ));
                return $video_url;
            } catch ( VimeoRequestException $ex ){

            }
        }
        return false;
    }



    static function infoByURL( $video_url ){
        try {
            $client = new Vimeo( static::CLIENT_ID, static::SECRET, static::TOKEN );
            $response = $client->request( $video_url );
            return $response;
        } catch ( VimeoRequestException $ex ){

        }
        return false;
    }



}
