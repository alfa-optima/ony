<?php

namespace App\Classes;



class Site {


    static $modules = [
//        'news' => [
//            'title' => 'Новости',
//            'model' => 'ProjectCategory',
//        ],
        'worksCategories' => [
            'title' => 'Категории работ',
            'model' => 'ProjectCategory',
        ],
//        'agency' => [
//            'title' => 'Агентство',
//            'model' => 'ProjectCategory',
//        ],
//        'contacts' => [
//            'title' => 'Контакты',
//            'model' => 'ProjectCategory',
//        ],
//        'blocks' => [
//            'title' => 'Блоки',
//            'model' => 'ProjectCategory',
//        ],
        'works' => [
            'title' => 'Работы',
            'model' => 'Project',
        ],
//        'indexSlides' => [
//            'title' => 'Слайды',
//            'model' => 'ProjectCategory',
//        ],
//        'textHtml' => [
//            'title' => 'Текст верстка',
//            'model' => 'ProjectCategory',
//        ],
//        'sitemap' => [
//            'title' => 'Карта сайта',
//            'model' => 'ProjectCategory',
//        ],
//        'indexPage' => [
//            'title' => 'Главная страница',
//            'model' => 'ProjectCategory',
//        ],
//        'text' => [
//            'title' => 'Текст',
//            'model' => 'ProjectCategory',
//        ],
//        'link' => [
//            'title' => 'Ссылка',
//            'model' => 'ProjectCategory',
//        ]
    ];


    


    static function getPDO(){
        $dsn = "mysql:host=localhost;dbname=ony_db;charset=utf8";
        $opt = array(
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        );
        $pdo = new \PDO($dsn, 'ony', 'pvd3R&0C!zL0', $opt);
        return $pdo;
    }




    // Перенос переводов в отдельную таблицу
    static function setTranslations ( $conn, $singleName, $pluralName ){
        $fields = [
            'name' => 'title',
            'header' => 'header',
            'description' => 'description',
            'long_description' => 'long_description',
            //'jobs_list' => 'jobs_list',
            //'html' => 'html',
        ];
        $sql = "SELECT * FROM ".$pluralName;
        $result = $conn->query($sql);
        while( $row = $result->fetch() ){
            $sql = "DELETE FROM ".$singleName."_translations WHERE ".$singleName."_id = ".$row['id'];
            $conn->query($sql);
            $valuesRU = [
                'created_at' => date("Ymdhms"),
                'updated_at' => date("Ymdhms"),
                'locale' => 'ru',
                'active' => 1,
                $singleName."_id" => $row['id']
            ];
            $valuesEN = [
                'created_at' => date("Ymdhms"),
                'updated_at' => date("Ymdhms"),
                'locale' => 'en',
                'active' => 1,
                $singleName."_id" => $row['id']
            ];
            $firstValuesCNT = count($valuesRU);
            foreach ( $fields as $keyOld => $keyNew ){
                if( isset($row[$keyOld]) ){
                    $valuesRU[$keyNew] = strip_tags($row[$keyOld]);
                }
                if( isset($row[$keyOld.'_en']) ){
                    $valuesEN[$keyNew] = strip_tags($row[$keyOld.'_en']);
                }
            }
            if( count($valuesRU) > $firstValuesCNT ){
                $sql = "INSERT INTO ".$singleName."_translations (".implode(', ', array_keys($valuesRU)).") VALUES (:".implode(', :', array_keys($valuesRU)).");";
                $stmt = $conn->prepare($sql);
                $stmt->execute( $valuesRU );
            }
            if( count($valuesEN) > $firstValuesCNT ){
                $sql = "INSERT INTO ".$singleName."_translations (".implode(', ', array_keys($valuesEN)).") VALUES (:".implode(', :', array_keys($valuesEN)).");";
                $stmt = $conn->prepare($sql);
                $stmt->execute( $valuesEN );
            }
        }
    }

    // Перенос переводов в отдельную таблицу
    static function setSlugs ( $conn, $singleName, $pluralName ){
        $sql = "SELECT * FROM ".$pluralName;
        $result = $conn->query($sql);
        while( $row = $result->fetch() ){
            $sql = "DELETE FROM ".$singleName."_slugs WHERE ".$singleName."_id = ".$row['id'];
            $conn->query($sql);
            $slug = null;
            if( isset($row['uri']) ){
                $slug = $row['uri'];
            }
            if( isset($slug) ){
                $valuesRU = [
                    'created_at' => date("Ymdhms"),
                    'updated_at' => date("Ymdhms"),
                    'locale' => 'ru',
                    'active' => 1,
                    $singleName."_id" => $row['id'],
                    "slug" => $slug,
                ];
                $valuesEN = [
                    'created_at' => date("Ymdhms"),
                    'updated_at' => date("Ymdhms"),
                    'locale' => 'en',
                    'active' => 0,
                    $singleName."_id" => $row['id'],
                    "slug" => $slug,
                ];
                $sql = "INSERT INTO ".$singleName."_slugs (".implode(', ', array_keys($valuesRU)).") VALUES (:".implode(', :', array_keys($valuesRU)).");";
                $stmt = $conn->prepare($sql);
                $stmt->execute( $valuesRU );
                $sql = "INSERT INTO ".$singleName."_slugs (".implode(', ', array_keys($valuesEN)).") VALUES (:".implode(', :', array_keys($valuesEN)).");";
                $stmt = $conn->prepare($sql);
                $stmt->execute( $valuesEN );
            }
        }
    }




}
