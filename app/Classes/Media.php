<?php

namespace App\Classes;
use App\Classes\Site;
use ColorThief\ColorThief;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Jcupitt\Vips;


class Media {

    const TABLE_NAME = 'medias';

    const UPLOAD_PATH = '/var/www/ony/htdocs/storage/app/public/media_uploads';

    static $contentTypes = [
        'image/jpeg', 'image/jpg',
        'image/png', 'image/gif'
    ];




    static function add( $filePath, $fileParams ){

        if( !file_exists( static::UPLOAD_PATH ) ){
            mkdir(static::UPLOAD_PATH, 0700);
        }

        $filePath = trim(strip_tags($filePath));
        if( file_exists($filePath) ){

            $fileInfo = pathinfo($filePath);
            $filePathDir = str_replace("/".$fileInfo['basename'], "", $filePath);

            $hash = md5( $filePath.rand(1, 1000000).time() );
            $newPath = static::UPLOAD_PATH.'/'.$hash;
            $uuid = $hash."/".$fileParams['originalName'];

            $subPath = false;
            if( strlen($fileParams['subPath']) > 0 ){
                $subPath = static::UPLOAD_PATH.'/'.$fileParams['subPath'];
                $newPath = static::UPLOAD_PATH.'/'.$fileParams['subPath'].'/'.$hash;
                $uuid = $fileParams['subPath'].'/'.$hash."/".$fileParams['originalName'];
                if( !file_exists($subPath) ){
                    mkdir($subPath, 0700);
                }
            }

            $newFilePath = $newPath."/".$fileParams['originalName'];
            $fileSize = filesize($filePath);

            $imageInfo = getimagesize($filePath);
            $width = $imageInfo[0];
            $height = $imageInfo[1];
            $fileType = $imageInfo['mime'];

            if( in_array($fileType, static::$contentTypes) ){

                mkdir($newPath, 0700);

                if( copy($filePath, $newFilePath) ){

                    $conn = Site::getPDO();

                    $arParams = [
                        'uuid' => $uuid,
                        'filename' => $fileParams['originalName'],
                        'width' => $width,
                        'height' => $height,
                        'alt_text' => "",
                    ];

                    $sql = "INSERT INTO ".static::TABLE_NAME." (created_at, updated_at, deleted_at, uuid, alt_text, width, height, caption, filename) VALUES(NOW(), NOW(), NULL, :uuid, :alt_text, :width, :height, NULL, :filename )";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute( $arParams );

                    $media_id = $conn->lastInsertId();

                    if( intval($media_id) > 0 ){

                        $arParams = [
                            'mediable_id' => $fileParams['mediable_id'],
                            'mediable_type' => $fileParams['DB_model'],
                            'media_id' => $media_id,
                            'crop_x' => 0,
                            'crop_y' => 0,
                            'crop_w' => $width,
                            'crop_h' => $height,
                            'role' => $fileParams['DB_role'],
                            'crop' => "default",
                            'lqip_data' => null,
                            'ratio' => "landscape",
                            'metadatas' => "{\"video\": null, \"altText\": null, \"caption\": null}",
                            'locale' => "ru"
                        ];

                        $sql = "INSERT INTO mediables (created_at, updated_at, deleted_at, mediable_id, mediable_type, media_id, crop_x, crop_y, crop_w, crop_h, role, crop, lqip_data, ratio, metadatas, locale) VALUES(NOW(), NOW(), NULL, :mediable_id, :mediable_type, :media_id, :crop_x, :crop_y, :crop_w, :crop_h, :role, :crop, :lqip_data, :ratio, :metadatas, :locale )";

                        $stmt = $conn->prepare($sql);
                        $stmt->execute( $arParams );

                        return $media_id;

                    } else {

                        unlink($newPath);
                        if( $subPath ){    unlink($subPath);    }
                    }
                } else {

                    unlink($newPath);
                    if( $subPath ){    unlink($subPath);    }
                }
            }
        }
        return false;
    }





    static function getPictureInfo( $model_name, $image_code, $el_id ){
        $info = [];
        $mediable_type = ($model_name=='blocks'?'':'App\Models\\').$model_name;
        $mediables = DB::table('mediables')
        ->where( 'mediable_type', $mediable_type )
        ->where( 'mediable_id', $el_id )
        ->where( 'role', $image_code )->get()->toArray();
        if( count($mediables) > 0 ){
            if( is_array($mediables) && count($mediables) > 0 ){
                $mediable = $mediables[0];
                $cacheID = 'mediaInfo_'.$mediable->media_id;
                if ( !Cache::has($cacheID) ){
                    $medias = DB::table('medias')->where('id', $mediable->media_id)->get()->toArray();
                    if( count($medias) > 0 ){
                        $mediaItem = $medias[0];
                        $info['image_url'] = '/img/'.$mediaItem->uuid;
                        $info['image_url'] = str_replace("//", "/", $info['image_url']);
                        $info['image_width'] = $mediaItem->width;
                        $info['image_height'] = $mediaItem->height;
                        $info['imageDominantColor'] = static::getDominantColor($mediaItem);
                    }
                    Cache::put($cacheID, $info, 60*60);
                } else {
                    $info = Cache::get($cacheID);
                }
                $info['crop_x'] = $mediable->crop_x;
                $info['crop_y'] = $mediable->crop_y;
                $info['crop_w'] = $mediable->crop_w;
                $info['crop_h'] = $mediable->crop_h;
                $info['image_url'] = env('REQUEST_SCHEME').'://'.env('APP_URL').$info['image_url'];
                // отключено по просьбе фронта
                if(
                    isset($info['crop_w']) && !is_null($info['crop_w'])
                    &&
                    $info['crop_w'] > 0 && $info['crop_w'] != $info['image_width']
                    &&
                    isset($info['crop_h']) && !is_null($info['crop_h']) && $info['crop_h'] > 0
                    &&
                    $info['crop_h'] > 0 && $info['crop_h'] != $info['image_height']
                ){
                    $info['image_url'] = $info['image_url']."?crop=".$info['crop_w'].",".$info['crop_h'].",".$info['crop_x'].",".$info['crop_y'];
                }
            }
        }
        return $info;
    }


    static function getPicturesInfo( $model_name, $image_code, $el_id ){
        $pictures = [];
        $mediable_type = ($model_name=='blocks'?'':'App\Models\\').$model_name;
        $mediables = DB::table('mediables')
            ->where( 'mediable_type', $mediable_type )
            ->where( 'mediable_id', $el_id )
            ->where( 'role', $image_code )->get()->toArray();
        if( count($mediables) > 0 ){
            if( is_array($mediables) && count($mediables) > 0 ){
                foreach ( $mediables as $mediable ){
                    $cacheID = 'mediaInfo_'.$mediable->media_id;
                    if ( !Cache::has($cacheID) ){
                        $medias = DB::table('medias')
                        ->where('id', $mediable->media_id)->get()->toArray();
                        if( count($medias) > 0 ){
                            $mediaItem = $medias[0];
                            $info['image_url'] = '/img/'.$mediaItem->uuid;
                            $info['image_url'] = str_replace("//", "/", $info['image_url']);
                            $info['image_width'] = $mediaItem->width;
                            $info['image_height'] = $mediaItem->height;
                            $info['imageDominantColor'] = static::getDominantColor($mediaItem);
                        }
                        Cache::put($cacheID, $info, 24*60*60);
                    } else {
                        $info = Cache::get($cacheID);
                    }
                    $info['crop_x'] = $mediable->crop_x;
                    $info['crop_y'] = $mediable->crop_y;
                    $info['crop_w'] = $mediable->crop_w;
                    $info['crop_h'] = $mediable->crop_h;
                    $info['image_url'] = env('REQUEST_SCHEME').'://'.env('APP_URL').$info['image_url'];
                    // отключено по просьбе фронта
                    if(
                        isset($info['crop_w']) && !is_null($info['crop_w'])
                        &&
                        $info['crop_w'] > 0 && $info['crop_w'] != $info['image_width']
                        &&
                        isset($info['crop_h']) && !is_null($info['crop_h']) && $info['crop_h'] > 0
                        &&
                        $info['crop_h'] > 0 && $info['crop_h'] != $info['image_height']
                    ){
                        $info['image_url'] = $info['image_url']."?crop=".$info['crop_w'].",".$info['crop_h'].",".$info['crop_x'].",".$info['crop_y'];
                    }
                    $pictures[] = $info;
                }
            }
        }
        return $pictures;
    }




    static function getDominantColor( $mediaItem ){
        if( isset($mediaItem) && !is_object($mediaItem) && intval($mediaItem) > 0 ){
            $medias = DB::table('medias')->where('id', intval($mediaItem))->get()->toArray();
            if( count($medias) > 0 ){    $mediaItem = $medias[0];    }
        }
        if( isset($mediaItem) && is_object($mediaItem) ){
            if( isset($mediaItem->color) && strlen($mediaItem->color) > 0 ){
                return Helper::json_to_array($mediaItem->color);
            } else if(
                isset($mediaItem->uuid)
                &&
                !substr_count($mediaItem->uuid, '.gif')
            ){
                $doc_root = str_replace('/public', '',  $_SERVER['DOCUMENT_ROOT']);
                $url = $doc_root.'/storage/app/public/media_uploads/'.$mediaItem->uuid;
                $url = str_replace('//', '/',  $url);

                if( file_exists($url) ){

// libvips отдавал 502 в случае некоторых картинок
// особенно png - разобраться в причинах не удалось (15.06.2020)
// вернулись к ColorThief
//                    try {
//                        $im = Vips\Image::newFromFile($url, ['access' => 'sequential']);
//                        $n_bins = 10;
//                        $hist = $im->hist_find_ndim(['bins' => $n_bins]);
//                        [$v, $x, $y] = $hist->maxpos();
//                        $pixel = $hist->getpoint($x, $y);
//                        $z = array_search($v, $pixel);
//                        $r = round(($x + 0.5) * 256 / $n_bins, 0);
//                        $g = round(($y + 0.5) * 256 / $n_bins, 0);
//                        $b = round(($z + 0.5) * 256 / $n_bins, 0);
//
//                        // Сохраним цвет в БД
//                        DB::table('medias')
//                        ->where('id', $mediaItem->id)
//                        ->update(['color' => json_encode([ $r, $g, $b ])]);
//
//                        return [ $r, $g, $b ];
//
//                    } catch (Exception $ex){
//                        //$ex->getMessage();
//                    }


try {
    $dc = ColorThief::getColor($url);
    if( is_array($dc) ){
// Сохраним цвет в БД
        DB::table('medias')
        ->where('id', $mediaItem->id)
        ->update(['color' => json_encode($dc)]);
        return $dc;
    }
} catch ( \RuntimeException $e ){}

                }
            }
        }
        return false;
    }





}
