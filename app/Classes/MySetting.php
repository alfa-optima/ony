<?php

namespace App\Classes;
use Illuminate\Support\Facades\DB;
use A17\Twill\Models\Setting;



class MySetting {



    // Получить значение настройки по ключу
    static function getValue(
        $section,       // категория настроек
        $key,           // ключ
        $lang = 'ru'    // язык
    ){
        $setting = Setting::where('section', $section)
        ->where('key', $key)->first();
        if(  isset($setting)  &&  intval($setting->id) > 0  ){
            $transSetting = DB::table('setting_translations')
            ->where('setting_id', $setting->id)
            ->where('locale', $lang)
            ->first();
            if(  isset($transSetting)  &&  intval($transSetting->id) > 0  ){
                return $transSetting->value;
            }
        }
        return null;
    }



}
