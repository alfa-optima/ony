<?php

namespace App\Classes;
use App\Models;
use App\Models\Project;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;



class Helper {



    static function getIDFromVimeoLink( $link ){
        $vimeo_video_id = null;
        preg_match(
            Project::VIMEO_LINK_REGEX,
            $link,
            $matches, PREG_OFFSET_CAPTURE
        );
        if( isset( $matches[2][0] ) ){
            $vimeo_video_id = $matches[2][0];
        }
        return $vimeo_video_id;
    }



    static function lang(){
        $langs = ['ru', 'en'];
        $lang = env('LANG_DEFAULT');
        if( isset($_GET['lng']) && strlen(trim($_GET['lng'])) > 0 ){
            $lang = trim($_GET['lng']);
        }
        if( isset($lang) && strlen($lang) > 0 && in_array($lang, $langs) ){
            return $lang;
        }
        return 'ru';
    }





//    static function getPictureInfo( $model_name, $image_code, $el_id ){
//        $info = [];
//        $mediables = DB::table('mediables')
//            ->where( 'mediable_type', 'App\Models\\'.$model_name )
//            ->where( 'mediable_id', $el_id )
//            ->where( 'role', $image_code )->get()->toArray();
//        if( count($mediables) > 0 ){
//            $cacheID = 'mediaInfo_'.$mediables[0]->media_id;
////            Cache::forget($cacheID);
////            Cache::flush();
//            if ( !Cache::has($cacheID) ){
//                $medias = DB::table('medias')->where('id', $mediables[0]->media_id)->get()->toArray();
//                if( count($medias) > 0 ){
//                    $mediaItem = $medias[0];
//                    $info['image_url'] = '/img/'.$mediaItem->uuid;
//                    $info['image_url'] = str_replace("//", "/", $info['image_url']);
//                    $info['image_width'] = $mediaItem->width;
//                    $info['image_height'] = $mediaItem->height;
//                    $info['padding_top'] = round($mediaItem->height/$mediaItem->width*100, 3);
//                    $info['imageDominantColor'] = static::getMediaDominantColor($mediaItem);
//                }
//                Cache::put($cacheID, $info, 60*60);
//            } else {
//                $info = Cache::get($cacheID);
//            }
//        }
//        return $info;
//    }



    static function getFileByID($file_id){
        $file = DB::table('files')->where('id', $file_id)->first();
        return $file;
    }



    static function outputFile( $file_id ){
        $obFile = static::getFileByID($file_id);
        if( !is_null($obFile) ){
            $file_path = __DIR__ . '/../../storage/app/public/file_uploads/'.$obFile->uuid;
            if( file_exists( $file_path ) ){
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $file_type = finfo_file($finfo, $file_path);
                finfo_close($finfo);
                $fp = fopen($file_path, 'rb');
                header("Content-Type: " . $file_type );
                header("Content-Length: " . $obFile->size);
                fpassthru($fp);
                exit;
            }
        }
        abort(404);
    }



    static function pureURL($url = false){
        if ( !$url ){   $url = $_SERVER["REQUEST_URI"];   }
        $pureURL = $url;
        if (substr_count($pureURL, "?")){
            $pos = strpos($pureURL, "?");
            $pureURL = substr($pureURL, 0, $pos);
        }
        return $pureURL;
    }



    static function pureImageURL( $imageURL ){
        $imagePureURL = static::pureURL($imageURL);
        $imagePureURL = str_replace( [ 'http://'.env('APP_URL'), 'https://'.env('APP_URL') ], "", $imagePureURL );
        return $imagePureURL;
    }



    static function json_to_array($json_string){
        $json_array = array();
        $error = false;
        if ($json_string && strlen($json_string) > 0){
            $json = htmlspecialchars_decode($json_string);
            if(strlen($json) > 0){
                $json_array = json_decode($json, true);
                if ( is_null($json_array) ){
                    switch (json_last_error()) {
                        case JSON_ERROR_DEPTH:
                            $error = 'Достигнута максимальная глубина стека';
                            break;
                        case JSON_ERROR_STATE_MISMATCH:
                            $error = 'Некорректные разряды или не совпадение режимов';
                            break;
                        case JSON_ERROR_CTRL_CHAR:
                            $error = 'Некорректный управляющий символ';
                            break;
                        case JSON_ERROR_SYNTAX:
                            $error = 'Синтаксическая ошибка, не корректный JSON';
                            break;
                        case JSON_ERROR_UTF8:
                            $error = 'Некорректные символы UTF-8, возможно неверная кодировка';
                            break;
                        case JSON_ERROR_RECURSION:
                            $error = 'Одна или несколько зацикленных ссылок в кодируемом значении';
                            break;
                        case JSON_ERROR_INF_OR_NAN:
                            $error = 'Одно или несколько значений NAN или INF в кодируемом значении';
                            break;
                        case JSON_ERROR_UNSUPPORTED_TYPE:
                            $error = 'Передано значение с неподдерживаемым типом';
                            break;
                        default:
                            $error = 'Неизвестная ошибка';
                            break;
                    }
                }

            }
        }
        return $error?$error:$json_array;
    }



    public static function getJsonRepeater($fields, $repeaterName, $serializedData) {
        $repeater_items = $fields[$serializedData];
        $repeatersConfig = config('twill.block_editor.repeaters');
        foreach( $repeater_items as $index => $repeater_item ){
            $id = $repeater_item['id'] ?? $index;
            $repeaters[] = [
                'id' => $id,
                'type' => $repeatersConfig[$repeaterName]['component'],
                'title' => $repeatersConfig[$repeaterName]['title'],
            ];
            $repeater_fields = array_except($repeater_item, ['id', 'medias', 'browsers', 'blocks']);
            foreach($repeater_fields as $index => $repeater_field) {
                $repeater_data[] = [
                    'name' => 'blocks['.$id.']['.$index.']',
                    'value' => $repeater_field
                ];
            }
        }
        $fields['repeaters'][$repeaterName] = $repeaters;
        $fields['repeaterFields'][$repeaterName] = $repeater_data;
        return $fields;
    }




}
