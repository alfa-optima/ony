<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use App\Classes\Site;
use App\Models\Project;



class Kernel extends ConsoleKernel {



    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */

    protected $commands = [
        //
    ];



    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */

    protected function schedule(Schedule $schedule){


        // Каждые 5 минут
        $schedule->call(function () {

            // Берём N проектов, отмеченных для загрузки на Vimeo
            $projectsToUpload = DB::table('projects')
            ->where('add_to_upload_list', 1)
            ->orderBy('id', 'desc')->limit(1)->get();

            // Если есть проекты для загрузки
            if( count($projectsToUpload) > 0 ){

                // Загружаем видео проектов на Vimeo
                foreach ( $projectsToUpload as $projectToUpload ){
                    Project::uploadVideosToVimeo( $projectToUpload->id );
                }

            // Если нет проектов для загрузки на Vimeo
            } else {

                // Занимаемся обновлением статусов видео
                Project::updateVimeoVideosStatuses();

            }

        })->everyFiveMinutes();

    }



    /**
     * Register the commands for the application.
     *
     * @return void
    */

    protected function commands(){
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }



}
