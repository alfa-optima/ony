<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class MyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('A17\Twill\Http\Requests\Admin\UserRequest', 'App\Http\Requests\Admin\UserRequest' );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
