<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Models\Project;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //



    }


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){


        // Проверка формата видео в проектах
        Validator::extend('project_check_video_format', function ($attribute, $value, $parameters, $validator) {
            $inputs = $validator->getData();
            $is_mp4_video = true;
            if( isset($inputs['medias']['video_mp4[ru]']['name']) ){
                $is_mp4_video = preg_match("/\.mp4$/", $inputs['medias']['video_mp4[ru]']['name'], $matches, PREG_OFFSET_CAPTURE);
            }
            return $is_mp4_video;
        });


        // Проверка слага
        Validator::extend('ProjectCheckUniqueSlug', function (
            $attribute, $value, $parameters, $validator
        ){
            $is_unuque = true;
            if( isset($inputs['slug']) ){

            }
            return $is_mp4_video;
        });


        // created
        Project::created(function( $project ){
            if( isset($project->id) && intval($project->id) > 0 ){

            }
        });


    }
}
