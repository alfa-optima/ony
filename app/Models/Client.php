<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Models\Project;
use App\Models\ClientCategory;



class Client extends Model implements Sortable {

    use HasTranslation, HasMedias, HasFiles, HasPosition;


    public function projects(){
        return $this->hasMany('App\Models\Project');
    }


    public function client_categories(){
        return $this->belongsToMany('App\Models\ClientCategory');
    }


    protected $fillable = [
        'published',
        'title',
        'description',
        'link',
        'client_category_id',
        'position',
        'test',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
    public $translatedAttributes = [
         'title',
         'description'
    ];

    // uncomment and modify this as needed if you use the HasSlug trait
    // public $slugAttributes = [
    //     'title',
    // ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];


     public $mediasParams = [
         'logo' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     //'ratio' => 16 / 9,
                 ]
             ]
         ],
         'logo_dark' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     //'ratio' => 16 / 9,
                 ]
             ]
         ],
     ];

}
