<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
//use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class HomePage extends Model implements Sortable
{
    use HasBlocks, HasTranslation/*, HasSlug*/, HasMedias, HasFiles, HasPosition;

    public $casts = [
        'map_coords' => 'array',
    ];

    public function clients(){
        return $this->belongsToMany(\App\Models\Client::class);
    }

    public function aboutAwards(){
        return $this->belongsToMany(\App\Models\AboutAward::class);
    }

//    public function projects(){
//        return $this->belongsToMany(\App\Models\Project::class);
//    }

    protected $fillable = [
        'published',
        'position',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
     public $translatedAttributes = [
         'title',
         'lead_text',
         'contacts',
         'jobs_block_title',
         'facts_block_title',
         'clients_block_title',
         'about_block_title',
         'about_block_text',
         'projects_block_title',
         'projects_block_text',
         'awards_block_title',
         'awards_block_text',
         'active',
     ];

    // uncomment and modify this as needed if you use the HasSlug trait
    // public $slugAttributes = [
    //     'title',
    // ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];


    public $mediasParams = [
        'about_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
    ];


    public $filesParams = [
        'about_video',
    ];


}
