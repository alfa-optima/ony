<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class AboutAward extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasPosition;

    public function projects() {
        return $this->belongsToMany('App\Models\Project');
    }

    protected $fillable = [
        'published',
        'count',
        'year',
        'title_without_year',
        // 'position',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
     public $translatedAttributes = [
         'title',
         'title_without_year',
     ];

    // uncomment and modify this as needed if you use the HasSlug trait
     public $slugAttributes = [
         'title',
     ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];


     public $mediasParams = [
         'image' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     //'ratio' => 16 / 9,
                 ]
             ]
         ],
         'image_dark' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     //'ratio' => 16 / 9,
                 ]
             ]
         ],
     ];


}
