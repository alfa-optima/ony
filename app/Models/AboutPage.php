<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;



class AboutPage extends Model implements Sortable {

    use HasTranslation, HasMedias, HasFiles, HasPosition, HasBlocks;

    public function aboutDirections(){
        return $this->belongsToMany(\App\Models\AboutDirection::class);
    }

    public function aboutAwards(){
        return $this->belongsToMany(\App\Models\AboutAward::class);
    }

    public function aboutClients(){
        return $this->belongsToMany(\App\Models\Client::class);
    }

    public function facts(){
        return $this->belongsToMany(\App\Models\Fact::class);
    }

    public function jobs(){
        return $this->belongsToMany(\App\Models\Job::class);
    }

    protected $casts = [
        'services_items_json' => 'array'
    ];

    protected $fillable = [
        'published',
        'header_vimeo_video_link',
        'showreel_vimeo_video_link',
        'services_items_json',
    ];


     public $translatedAttributes = [
         'title',
         'lead_text',
         'contacts',
         'about_block_title',
         'about_block_text',
         'departments_block_title',
         'client_block_title',
         'client_block_text',
         'awards_block_title',
         'awards_block_text',
         'facts_block_title',
         'jobs_block_title',
     ];


     public $slugAttributes = [
         //'title',
     ];


    public $checkboxes = [
        //'published'
    ];


    public $mediasParams = [
        'header_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
        'showreel_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
    ];








}
