<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Shell extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasMedias, HasFiles, HasPosition;


    public function socials(){
        return $this->belongsToMany(\App\Models\Social::class);
    }


    protected $fillable = [
        //'published',
        'position',
        'api_secret_key',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
     public $translatedAttributes = [
         'title',
         'seo_title',
         'seo_description',
         'seo_ogDescription',
         'footer_address_title',
         'footer_address_content',
         'footer_newBusinessContacts_title',
         'footer_newBusinessContacts_content',
         'footer_otherContacts_title',
         'footer_otherContacts_content',
         'footer_social_block_title',
         'active',
     ];

    // uncomment and modify this as needed if you use the HasSlug trait
    // public $slugAttributes = [
    //     'title',
    // ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        //'published'
    ];

     public $mediasParams = [
         'seo_ogImage' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     //'ratio' => 16 / 9,
                 ]
             ]
         ],
     ];


}
