<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class ContactPage extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasMedias, HasFiles, HasPosition;

    public $casts = [
        'map_coords' => 'array',
    ];

    protected $fillable = [
        'published',
        'map_lat',
        'map_lng',
        'map_zoom',
        'map_coords',
        'position',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
     public $translatedAttributes = [
         'title',
         'lead_text',
         'address_title',
         'address',
         'active',
     ];

    // uncomment and modify this as needed if you use the HasSlug trait
    // public $slugAttributes = [
    //     'title',
    // ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        //'published'
    ];


    public $mediasParams = [
        'contacts_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
    ];


}
