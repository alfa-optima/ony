<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class ServicesPage extends Model implements Sortable
{

    use HasBlocks, HasTranslation, HasMedias, HasFiles, HasPosition;

    public function services() {
        return $this->belongsToMany('App\Models\Service');
    }

    protected $fillable = [
        'published',
        'header_vimeo_video_link',
        // 'publish_start_date',
        // 'publish_end_date',
    ];


    public $translatedAttributes = [
         'title',
         'page_title',
         'lead_text',
         'body_title',
         'body_text',
         'services_title',
         'items_title',
         'items_description',
         'contacts',
    ];

    public $slugAttributes = [
         //'title',
    ];

    public $checkboxes = [
        'published'
    ];


     public $mediasParams = [
         'header_image' => [
             'default' => [
                 [
                     'name' => 'landscape',
                     //'ratio' => 16 / 9,
                 ]
             ]
         ],
     ];


}
