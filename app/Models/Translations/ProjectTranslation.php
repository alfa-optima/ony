<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class ProjectTranslation extends Model
{
    protected $fillable = [
        'title',
        'description',
        'header',
        'long_description',
        'active',
        'locale',
    ];
}
