<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class ContactPageTranslation extends Model
{
    protected $fillable = [
        'title',
        'lead_text',
        'address_title',
        'address',
        'active',
        'locale',
    ];
}
