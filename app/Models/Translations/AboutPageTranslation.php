<?php

namespace App\Models\Translations;
use A17\Twill\Models\Model;



class AboutPageTranslation extends Model {

    protected $fillable = [
        'title',
        'lead_text',
        'contacts',
        'about_block_title',
        'about_block_text',
        'departments_block_title',
        'client_block_title',
        'client_block_text',
        'awards_block_title',
        'awards_block_text',
        'services_title',
        'services_text',
        'facts_block_title',
        'jobs_block_title',
    ];



}
