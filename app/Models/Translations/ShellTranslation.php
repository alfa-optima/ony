<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class ShellTranslation extends Model
{
    protected $fillable = [
        'title',
        'seo_title',
        'seo_description',
        'seo_ogDescription',
        'footer_address_title',
        'footer_address_content',
        'footer_newBusinessContacts_title',
        'footer_newBusinessContacts_content',
        'footer_otherContacts_title',
        'footer_otherContacts_content',
        'footer_social_block_title',
        'active',
        'locale',
    ];
}
