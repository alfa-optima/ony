<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class HomePageTranslation extends Model
{
    protected $fillable = [
        'title',
        'lead_text',
        'contacts',
        'jobs_block_title',
        'facts_block_title',
        'clients_block_title',
        'about_block_title',
        'about_block_text',
        'projects_block_title',
        'projects_block_text',
        'awards_block_title',
        'awards_block_text',
        'active',
        'locale',
    ];
}
