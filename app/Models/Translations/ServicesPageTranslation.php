<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class ServicesPageTranslation extends Model
{
    protected $fillable = [
        'title',
        'page_title',
        'lead_text',
        'body_title',
        'body_text',
        'services_title',
        'items_title',
        'items_description',
        'contacts',
    ];
}
