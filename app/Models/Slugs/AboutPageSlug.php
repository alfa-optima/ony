<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class AboutPageSlug extends Model
{
    protected $table = "about_page_slugs";
}
