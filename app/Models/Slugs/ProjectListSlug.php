<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ProjectListSlug extends Model
{
    protected $table = "project_list_slugs";
}
