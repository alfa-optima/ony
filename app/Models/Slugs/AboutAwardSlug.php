<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class AboutAwardSlug extends Model
{
    protected $table = "about_award_slugs";
}
