<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class AboutDirectionSlug extends Model
{
    protected $table = "about_direction_slugs";
}
