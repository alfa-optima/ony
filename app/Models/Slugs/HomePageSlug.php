<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class HomePageSlug extends Model
{
    protected $table = "home_page_slugs";
}
