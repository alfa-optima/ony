<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ProjectDirectionSlug extends Model
{
    protected $table = "project_direction_slugs";
}
