<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Models\Client;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\DB;


class ClientCategory extends Model implements Sortable
{
    use HasTranslation, HasMedias, HasFiles, HasSlug, HasPosition;

    const EDIT_PAGE_REGEX = "/\/admin\/proj\/clientCategories\/([1-9][0-9]*)\/edit/";

    public function clients(){
        return $this->hasMany('App\Models\Client');
    }


    protected $fillable = [
        'published',
        'title',
        'position',
        'slug_field',

        // 'public',
        // 'featured',
        // 'publish_start_date',
        // 'publish_end_date',
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
     public $translatedAttributes = [
         'title',
         'description',
     ];

    // uncomment and modify this as needed if you use the HasSlug trait
     public $slugAttributes = [
         'title',
     ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];

    // uncomment and modify this as needed if you use the HasMedias trait
    // public $mediasParams = [
    //     'cover' => [
    //         'default' => [
    //             [
    //                 'name' => 'landscape',
    //                 'ratio' => 16 / 9,
    //             ],
    //             [
    //                 'name' => 'portrait',
    //                 'ratio' => 3 / 4,
    //             ],
    //         ],
    //         'mobile' => [
    //             [
    //                 'name' => 'mobile',
    //                 'ratio' => 1,
    //             ],
    //         ],
    //     ],
    // ];





    static function getUniqueSlug( $title = null, $slug = null, $item_id = null ){
        $table_name = 'client_categories';
        $slug_field = null;
        if( isset($title) ){
            $slugify = new Slugify();
            $slug_field_orig = $slugify->slugify($title);
            $slug_field = $slug_field_orig;
        } else if( isset($slug) ){
            $slug_field_orig = $slug;
            $slug_field = $slug_field_orig;
        }
        if( isset($slug_field) ){
            $isUniqueSlug = false;   $cnt = 1;
            while( !$isUniqueSlug ){
                if( isset( $item_id ) ){
                    $otherProject = DB::table($table_name)
                        ->where('slug_field', $slug_field)
                        ->where('id', '<>', $item_id)->first();
                } else {
                    $otherProject = DB::table($table_name)
                        ->where('slug_field', $slug_field)->first();
                }
                if( isset($otherProject) ){
                    $cnt++;
                    $slug_field = $slug_field_orig.'-'.$cnt;
                } else {
                    $isUniqueSlug = true;
                }
            }
        }
        return $slug_field;
    }



    public static function getBySlug( $code ){
        $item = false;
        if( strlen($code) > 0 ){
            $item = static::where('slug_field', $code)->first();
        }
        return $item;
    }




//    public static function getSlug( $id, $lang = 'ru' ){
//        $slug = false;
//        if( intval($id) > 0 ){
//            $slug = DB::table('client_category_slugs')
//            ->where('client_category_id', $id)
//            ->where('locale', $lang)
//            ->where('active', 1)
//            ->first();
////            if( isset($slug) ){
////                $item = static::find($slugs[0]->project_id);
////            }
//        }
//        return $slug;
//    }



}
