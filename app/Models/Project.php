<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
//use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use App\Classes\Helper;
use App\Models\ProjectList;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Classes\VimeoHelper;
use App\Classes\Site;
use App\Models\Client;
use App\Models\ClientCategory;




class Project extends Model /*implements Sortable*/ {

    use HasBlocks, HasTranslation/*, HasSlug*/, HasMedias, HasFiles, HasPosition;

    const EDIT_PAGE_REGEX = "/\/admin\/proj\/projects\/([1-9][0-9]*)\/edit/";
    const VIMEO_LINK_REGEX = "/^(https\:\/\/){0,1}vimeo\.com\/([0-9]+)(\/[a-z0-9]+){0,1}/";


    public function project_categories() {
        return $this->belongsToMany('App\Models\ProjectCategory');
    }


    public function client(){
        return $this->belongsTo('App\Models\Client');
    }

    public function about_awards() {
        return $this->belongsToMany('App\Models\AboutAward');
    }


    protected $fillable = [
        'published',

        'slug_field',
        'date',
        'section_id',
        'project_direction_id',
        'client_id',
        'show_admin_only',
        'add_to_upload_list',
        'video_mp4_link',
        'site_link',
        'login',
        'password',
        'publish_start_date',
        'publish_end_date',
    ];


    public $translatedAttributes = [
         'title',
         'header',
         'description',
         'long_description',
    ];


    public $slugAttributes = [
         'title',
    ];


    public $checkboxes = [
        'published'
    ];





    public $mediasParams = [
        'poster' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
        'poster_mobile' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
        'list_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
        'block_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    //'ratio' => 16 / 9,
                ]
            ]
        ],
    ];



    public $filesParams = [
        'video_mp4',
        'video_webm',
        'video_mobile_mp4',
        'video_mobile_webm',
    ];





    // Фильтрация+сортировка проектов по слагу списка
    static function filterByListSlug( $projects, $list_slug ){
        $list_slug = strip_tags(trim($list_slug));
        $project_ids = [];   $positions = [];
        // Получим инфу о списке
        $project_list = ProjectList::getBySlug( $list_slug );
        if( isset($project_list->id) ){
            // Получим ID проектов, привязанных к данному списку
            $projectListItems = DB::table('project_project_list')
                ->where('project_list_id', $project_list->id)
                ->get();
            if( count($projectListItems) > 0 ){
                foreach ( $projectListItems as $projectListItem ){
                    $project_ids[] = $projectListItem->project_id;
                    $positions[] = $projectListItem->position;
                }
            }
        }
        array_multisort($positions, SORT_ASC, SORT_NUMERIC, $project_ids);
        // Отсортируем проекты в порядке, который задан в списке
        $newProjects = clone $projects;
        foreach ( $newProjects as $key => $project ){    unset($newProjects[$key]);    }
        /////////////////
        foreach ( $project_ids as $k1 => $project_id ){
            foreach ( $projects as $k2 => $project ){
                if( $project_id == $project->id ){
                    $project->position = $positions[$k1];
                    $newProjects[$project->position] = $project;
                }
            }
        }
        $projects = clone $newProjects;
        unset($newProjects);
        return $projects;
    }



    // Фильтрация проектов по типу клиента
    static function filterByClientType( $projects, $client_type ){
        $newProjects = clone $projects;
        foreach ( $newProjects as $key => $project ){    unset($newProjects[$key]);    }
        $client_type = strip_tags(trim($client_type));
        foreach ( $projects as $key => $project ){
            $client = $project->client()->first();
            if( isset($client) ){
                $clientCategories = $client->client_categories()->get();
                if( count($clientCategories) > 0 ){
                    foreach ( $clientCategories as $key => $clientCategory ){
                        if(
                            isset($clientCategory->slug_field)
                            &&
                            $client_type == $clientCategory->slug_field
                        ){
                            $newProjects[$project->id] = $project;
                        }
                    }
                }
            }
        }
        $projects = clone $newProjects;
        unset($newProjects);
        return $projects;
    }



    // Фильтрация проектов по категории проектов
    static function filterByWorkType( $projects, $work_type ){
        $newProjects = clone $projects;
        foreach ( $newProjects as $key => $project ){    unset($newProjects[$key]);    }
        $work_type = strip_tags(trim($work_type));
        foreach ( $projects as $key => $project ){
            $project_categories = $project->project_categories()->get();
            foreach ( $project_categories as $key => $project_category ){
                if(
                    isset($project_category->slug_field)
                    &&
                    $project_category->slug_field == $work_type
                ){     $newProjects[] = $project;     }
            }
        }
        $projects = clone $newProjects;
        unset($newProjects);
        return $projects;
    }




    // Загрузка видео на Vimeo
    static function uploadVideosToVimeo( $project_id ){

        $doc_root = env('DOC_ROOT');
        if( isset($doc_root) ){

            $project = static::find($project_id);
            $arProject = $project->toArray();

            if( !is_null($project) ){

                $title_ru = $project->translatedAttribute('title')['ru'];

                $roles = [ 'video_mp4' ];

                foreach ( $roles as $role ){

                    $fileUploaded = false;
                    $vimeoLink = '';

                    // Ищём привязки проекта к файлу галереи в БД
                    $videoAble = false;
                    $videoAbles = DB::table('fileables')
                        ->where('fileable_id', $arProject['id'])
                        ->where('fileable_type', 'App\Models\Project')
                        ->where('role', $role)
                        ->get();
                    if( count($videoAbles) > 0 ){    $videoAble = $videoAbles[0];    }

                    /////////////////////
                    //$videoAble = false;
                    /////////////////////

                    // Если найдена привязки проекта к файлу галереи в БД
                    if( $videoAble ){
                        // Получим запись файла в БД
                        $file = DB::table('files')
                        ->where('id', $videoAble->file_id)
                        ->first();
                        // Если найдена запись файла в БД
                        if( isset($file) ){

                            // Загружаем видео на Vimeo
                            $filePath = $doc_root.'/storage/app/public/file_uploads/'.$file->uuid;
                            $fileDir = str_replace( '/'.$file->filename, '', $filePath);

                            $name = 'Video `'.$role.'` for project `'.$title_ru.'` [ID='.$arProject['id'].']';
                            $description = $name;

                            $videoURI = VimeoHelper::uploadVideo( $filePath, $name, $description );

                            if( isset($videoURI) && strlen($videoURI) > 0 ){

                                // Получим инфо о загруженном видео
                                $videoInfo = VimeoHelper::infoByURL( $videoURI );

                                if( isset($videoInfo['body']['link']) ){

                                    preg_match(
                                        static::VIMEO_LINK_REGEX,
                                        $videoInfo['body']['link'],
                                        $matches, PREG_OFFSET_CAPTURE
                                    );
                                    if( isset( $matches[2][0] ) ){

                                        $vimeo_video_id = $matches[2][0];

                                        $status = 'in_progress';
                                        if( isset($videoInfo['body']['transcode']['status']) ){
                                            $status = $videoInfo['body']['transcode']['status'];
                                        }

                                        $arInfo = [
                                            'uri' => $videoInfo['body']['uri'],
                                            'link' => $videoInfo['body']['link'],
                                            'name' => $videoInfo['body']['name'],
                                            'description' => $videoInfo['body']['description'],
                                            'pictures' => $videoInfo['body']['pictures'],
                                            'status' => $status,
                                            'upload_timestamp' => time(),
                                        ];
                                        if(
                                            isset($videoInfo['body']['files'])
                                            &&
                                            is_array($videoInfo['body']['files'])
                                            &&
                                            count($videoInfo['body']['files']) > 0
                                        ){
                                            $arInfo['files'] = $videoInfo['body']['files'];
                                        }

                                        // Сохранение инфы по видео в БД
                                        $vimeoLink = $videoInfo['body']['link'];
                                        static::saveVimeoInfo( $vimeo_video_id, $arInfo );

                                        $fileUploaded = true;
                                    }
                                }
                            }

                            // Удаляем привязку файла к проекту
                            DB::table('fileables')
                            ->where('id', $videoAble->id)
                            ->delete();

                            // Если нет других привязок к этому файлу
                            $otherFileAbles = DB::table('fileables')
                            ->where('file_id', $file->id)->get();
                            if( count($otherFileAbles) == 0 ){
                                // удалим запись файла в БД
                                DB::table('files')
                                ->where('id', $file->id)
                                ->delete();
                                // удалим сам файл физически с сервера
                                unlink($filePath);
                                rmdir($fileDir);
                            }

                        }
                    }

                    // ПОЛУЧИМ ИНФУ О ВИДЕО НА VIMEO
                    if(
                        // Если новый файл сейчас не загружался на Vimeo
                        !$fileUploaded
                        &&
                        // Если заполнено поле "video_mp4_link"
                        isset($arProject[$role.'_link'])
                        &&
                        strlen($arProject[$role.'_link']) > 0
                    ){

                        preg_match(
                            static::VIMEO_LINK_REGEX,
                            $arProject[$role.'_link'],
                            $matches, PREG_OFFSET_CAPTURE
                        );

                        if( isset( $matches[2][0] ) ){

                            $vimeo_video_id = $matches[2][0];
                            $vimeo_video_url = '/videos/'.$vimeo_video_id;
                            $videoInfo = VimeoHelper::infoByURL( $vimeo_video_url );
                            if(
                                isset($videoInfo)
                                &&
                                is_array($videoInfo)
                            ){

                                $status = 'in_progress';
                                if( isset($videoInfo['body']['transcode']['status']) ){
                                    $status = $videoInfo['body']['transcode']['status'];
                                }
                                $arInfo = [
                                    'uri' => $videoInfo['body']['uri'],
                                    'link' => $videoInfo['body']['link'],
                                    'name' => $videoInfo['body']['name'],
                                    'description' => $videoInfo['body']['description'],
                                    'pictures' => $videoInfo['body']['pictures'],
                                    'status' => $status,
                                    'upload_timestamp' => time(),
                                ];
                                if(
                                    isset($videoInfo['body']['files'])
                                    &&
                                    is_array($videoInfo['body']['files'])
                                    &&
                                    count($videoInfo['body']['files']) > 0
                                ){
                                    $arInfo['files'] = $videoInfo['body']['files'];
                                }

                                // Сохранение инфы по видео в БД
                                $vimeoLink = $videoInfo['body']['link'];
                                static::saveVimeoInfo( $vimeo_video_id, $arInfo );

                            } else {
                                $vimeoLink = '';
                            }
                        } else {
                            $vimeoLink = '';
                        }
                    }

                    // Сохранение инфы в проект
                    DB::table('projects')
                        ->where('id', $arProject['id'])
                        ->update([ $role.'_link' => $vimeoLink ]);
                }


                // БЛОКИ

                // Получим блоки для данного проекта
                $blocks = DB::table('blocks')
                    ->where('blockable_id', $arProject['id'])
                    ->where('blockable_type', 'App\Models\Project')
                    ->get();

                if( count($blocks) > 0 ){

                    foreach ( $blocks as $block ){

                        $content = Helper::json_to_array($block->content);

                        $roles = [ 'project_block_video_mp4' ];

                        foreach ( $roles as $role ){

                            $fileUploaded = false;

                            // Ищём привязки блока к файлу галереи в БД
                            $videoAble = false;
                            $videoAbles = DB::table('fileables')
                                ->where('fileable_id', $block->id)
                                ->where('fileable_type', 'blocks')
                                ->where('role', $role)
                                ->get();
                            if( count($videoAbles) > 0 ){    $videoAble = $videoAbles[0];    }

                            /////////////////////
                            //$videoAble = false;
                            /////////////////////

                            // Если найдена привязки проекта к файлу галереи в БД
                            if( $videoAble ){
                                // Получим запись файла в БД
                                $file = DB::table('files')
                                    ->where('id', $videoAble->file_id)
                                    ->first();
                                // Если найдена запись файла в БД
                                if( !is_null($file) ){

                                    // Загружаем видео на Vimeo
                                    $filePath = $doc_root.'/storage/app/public/file_uploads/'.$file->uuid;
                                    $fileDir = str_replace( '/'.$file->filename, '', $filePath);

                                    $name = 'Video `'.$role.'` for project `'.$title_ru.'` [ID='.$arProject['id'].']';
                                    $description = $name;

                                    $videoURI = VimeoHelper::uploadVideo( $filePath, $name, $description );

                                    if( isset($videoURI) && strlen($videoURI) > 0 ){

                                        $videoInfo = VimeoHelper::infoByURL( $videoURI );

                                        if( isset($videoInfo['body']['link']) ){

                                            preg_match(
                                                static::VIMEO_LINK_REGEX,
                                                $videoInfo['body']['link'],
                                                $matches, PREG_OFFSET_CAPTURE
                                            );
                                            if( isset( $matches[2][0] ) ){

                                                $vimeo_video_id = $matches[2][0];

                                                $status = 'in_progress';
                                                if( isset($videoInfo['body']['transcode']['status']) ){
                                                    $status = $videoInfo['body']['transcode']['status'];
                                                }

                                                $arInfo = [
                                                    'uri' => $videoInfo['body']['uri'],
                                                    'link' => $videoInfo['body']['link'],
                                                    'name' => $videoInfo['body']['name'],
                                                    'description' => $videoInfo['body']['description'],
                                                    'pictures' => $videoInfo['body']['pictures'],
                                                    'status' => $status,
                                                    'upload_timestamp' => time(),
                                                ];
                                                if(
                                                    isset($videoInfo['body']['files'])
                                                    &&
                                                    is_array($videoInfo['body']['files'])
                                                    &&
                                                    count($videoInfo['body']['files']) > 0
                                                ){
                                                    $arInfo['files'] = $videoInfo['body']['files'];
                                                }

                                                // Сохранение инфы по видео в БД
                                                $content[$role.'_link'] = $videoInfo['body']['link'];
                                                static::saveVimeoInfo( $vimeo_video_id, $arInfo );

                                                $fileUploaded = true;
                                            }
                                        }
                                    }

                                    // Удаляем привязку файла к проекту
                                    DB::table('fileables')
                                    ->where('id', $videoAble->id)
                                    ->delete();

                                    // Если нет других привязок к этому файлу
                                    $otherFileAbles = DB::table('fileables')
                                    ->where('file_id', $file->id)->get();
                                    if( count($otherFileAbles) == 0 ){
                                        // удалим запись файла в БД
                                        DB::table('files')
                                        ->where('id', $file->id)
                                        ->delete();
                                        // удалим сам файл физически с сервера
                                        unlink($filePath);
                                        rmdir($fileDir);
                                    }

                                }
                            }

                            // ПОЛУЧИМ ИНФУ О ВИДЕО НА VIMEO
                            if(
                                // Если новый файл сейчас не загружался на Vimeo
                                !$fileUploaded
                                &&
                                // Если заполнено поле "video_mp4_link"
                                isset( $content[$role.'_link'] )
                                &&
                                strlen( $content[$role.'_link'] ) > 0
                            ){

                                preg_match(
                                    static::VIMEO_LINK_REGEX,
                                    $content[$role.'_link'],
                                    $matches, PREG_OFFSET_CAPTURE
                                );
                                if( isset( $matches[2][0] ) ){
                                    $vimeo_video_id = $matches[2][0];
                                    $vimeo_video_url = '/videos/'.$vimeo_video_id;
                                    $videoInfo = VimeoHelper::infoByURL( $vimeo_video_url );
                                    if(
                                        isset($videoInfo)
                                        &&
                                        is_array($videoInfo)
                                    ){
                                        $status = 'in_progress';
                                        if( isset($videoInfo['body']['transcode']['status']) ){
                                            $status = $videoInfo['body']['transcode']['status'];
                                        }
                                        $arInfo = [
                                            'uri' => $videoInfo['body']['uri'],
                                            'link' => $videoInfo['body']['link'],
                                            'name' => $videoInfo['body']['name'],
                                            'description' => $videoInfo['body']['description'],
                                            'pictures' => $videoInfo['body']['pictures'],
                                            'status' => $status,
                                            'upload_timestamp' => time(),
                                        ];
                                        if(
                                            isset($videoInfo['body']['files'])
                                            &&
                                            is_array($videoInfo['body']['files'])
                                            &&
                                            count($videoInfo['body']['files']) > 0
                                        ){
                                            $arInfo['files'] = $videoInfo['body']['files'];
                                        }

                                        // Сохранение инфы по видео в БД
                                        $content[$role.'_link'] = $videoInfo['body']['link'];
                                        static::saveVimeoInfo( $vimeo_video_id, $arInfo );

                                    } else {
                                        $content[$role.'_link'] = '';
                                    }
                                } else {
                                    $content[$role.'_link'] = '';
                                }
                            }

                            // Сохранение инфы в блок
                            DB::table('blocks')
                            ->where('id', $block->id)
                            ->update([
                                'content' => json_encode($content, JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
                            ]);

                        }
                    }
                }

                // Убираем галочку
                DB::table('projects')
                ->where('id', $arProject['id'])
                ->update([ 'add_to_upload_list' => 0 ]);

            }

        }
    }



    static function getVimeoInfo( $vimeo_id ){
        $cacheID = 'VimeoInfoByID_'.$vimeo_id;
        if ( !Cache::has($cacheID) ){
            $item = DB::table('vimeo_videos')
                ->where('vimeo_id', $vimeo_id)->first();
            if(
                isset($item)
                &&
                isset($item->json)
                &&
                strlen($item->json) > 0
            ){
                $info = Helper::json_to_array($item->json);
            }
            Cache::put($cacheID, $info, 5*60);
        } else {
            $info = Cache::get($cacheID);
        }
        return $info;
    }



    static function saveVimeoInfo( $vimeo_id, $info ){
        $item = DB::table('vimeo_videos')
        ->where('vimeo_id', $vimeo_id)->first();
        if( isset($item) ){
            DB::table('vimeo_videos')
            ->where('id', $item->id)
            ->update([
                'json' => json_encode($info, JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
            ]);
        } else {
            DB::table('vimeo_videos')->insert([
                'vimeo_id' => $vimeo_id,
                'json' => json_encode($info, JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
                'completed' => 0,
            ]);
        }
    }



    static function updateVimeoVideosStatuses( $force = false ){
        $conn = Site::getPDO();
        // Поиск в загруженных видео Vimeo с незавершённым статусом обработки
        $sql = "SELECT id,vimeo_id,json,last_update FROM vimeo_videos";
        //$sql .= " WHERE id BETWEEN 300 AND 399";
        if( !$force ){
            $sql .= " WHERE";
            $sql .= " completed = 0";
            $sql .= " AND CHAR_LENGTH(json) > 2";
            $sql .= " AND CHAR_LENGTH(vimeo_id)";
            $sql .= " AND (last_update IS NULL OR last_update > '".date( "Y-m-d H:i:s", time() - 3*24*60*60 )."')";
        }
        $sql .= " ORDER BY id ASC";
        $items = $conn->query($sql);
        while( $item = $items->fetch() ){

            $json = $item['json'];
            $arInfo = \App\Classes\Helper::json_to_array($json);
            if( is_array($arInfo) ){
                if(
                    !isset($arInfo['status'])
                    ||
                    $arInfo['status'] != 'complete'
                    ||
                    !isset($arInfo['files'])
                    ||
                    (
                        is_array($arInfo['files'])
                        &&
                        count($arInfo['files']) == 0
                    )
                    ||
                    $force
                ){

                    if( !isset($arInfo['upload_timestamp']) ){
                        $arInfo['upload_timestamp'] = time();
                    }

                    // Запрос свежей инфы
                    $videoInfo = VimeoHelper::infoByURL( $arInfo['uri'] );

                    if( isset($videoInfo) && is_array($videoInfo) ){
                        $newArInfo = [
                            'uri' => $videoInfo['body']['uri'],
                            'link' => $videoInfo['body']['link'],
                            'name' => $videoInfo['body']['name'],
                            'description' => $videoInfo['body']['description'],
                            'pictures' => $videoInfo['body']['pictures'],
                            'status' => $videoInfo['body']['transcode']['status'],
                            'upload_timestamp' => $arInfo['upload_timestamp'],
                        ];
                        if(
                            isset($videoInfo['body']['files'])
                            &&
                            is_array($videoInfo['body']['files'])
                            &&
                            count($videoInfo['body']['files']) > 0
                        ){
                            $newArInfo['files'] = $videoInfo['body']['files'];
                        }
                        $completed = 0;
                        if(
                            isset($newArInfo['status'])
                            &&
                            $newArInfo['status'] == 'complete'
                            &&
                            isset($newArInfo['files'])
                        ){   $completed = 1;   }
                        // Сохранение инфы в проект
                        DB::table('vimeo_videos')
                        ->where('id', $item['id'])
                        ->update([
                            'json' => json_encode($newArInfo, JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
                            'completed' => $completed,
                        ]);
                    }

                } else {
                    // Сохранение инфы в проект
                    DB::table('vimeo_videos')
                    ->where('id', $item['id'])
                    ->update([ 'completed' => 1 ]);
                }
            }
        }
    }




    static function getVideoInfo( $info ){
        $arInfo = null;
        if(
            isset($info['files'])
            &&
            is_array($info['files'])
            &&
            isset($info['pictures']['sizes'])
            &&
            is_array($info['pictures']['sizes'])
        ){
            $arInfo = [
                'pictures' => [],
                'files' => [],
            ];
            foreach ( $info['pictures']['sizes'] as $key => $arSize ){
                $arInfo['pictures'][] = [
                    'width' => $arSize['width'],
                    'height' => $arSize['height'],
                    'link' => $arSize['link'],
                ];
            }
            foreach ( $info['files'] as $key => $arFile ){
                $arInfo['files'][] = $arFile;
            }
        }
        return $arInfo;
    }





    static function customAfterSaveActions( $object, $fields ){
        //static::uploadVideosToVimeo( $object->id );
    }



    static function getUniqueSlug( $title = null, $slug = null, $item_id = null ){
        $table_name = 'projects';
        $slug_field = null;
        if( isset($title) ){
            $slugify = new Slugify();
            $slug_field_orig = $slugify->slugify($title);
            $slug_field = $slug_field_orig;
        } else if( isset($slug) ){
            $slug_field_orig = $slug;
            $slug_field = $slug_field_orig;
        }
        if( isset($slug_field) ){
            $isUniqueSlug = false;   $cnt = 1;
            while( !$isUniqueSlug ){
                if( isset( $item_id ) ){
                    $otherProject = DB::table($table_name)
                        ->where('slug_field', $slug_field)
                        ->where('id', '<>', $item_id)->first();
                } else {
                    $otherProject = DB::table($table_name)
                        ->where('slug_field', $slug_field)->first();
                }
                if( isset($otherProject) ){
                    $cnt++;
                    $slug_field = $slug_field_orig.'-'.$cnt;
                } else {
                    $isUniqueSlug = true;
                }
            }
        }
        return $slug_field;
    }



    public static function getBySlug( $code ){
        $item = false;
        if( strlen($code) > 0 ){
            $item = static::where('slug_field', $code)->first();
        }
        return $item;
    }



}
