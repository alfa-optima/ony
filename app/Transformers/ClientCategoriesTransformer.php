<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use League\Fractal\TransformerAbstract;
use App\Models\ClientCategory;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ClientCategoriesTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
    */

    public function transform( ClientCategory $clientCategory ){

        $lang = Helper::lang();

        $transTitles = $clientCategory->translatedAttribute('title');

        $arItem = [
            'id' => (int) $clientCategory->id,
            'slug' => isset($clientCategory->slug_field)?$clientCategory->slug_field:null,
            'title' => !is_null($transTitles[$lang])?$transTitles[$lang]:$transTitles['ru'],
        ];

        return $arItem;
    }



}
