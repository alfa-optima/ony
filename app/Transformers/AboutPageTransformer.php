<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use App\Models\Client;
use App\Models\Fact;
use App\Models\Job;
use App\Models\Project;
use App\Models\Service;
use League\Fractal\TransformerAbstract;
use App\Models\AboutPage;
use App\Models\ServicesPage;
use App\Models\AboutDirection;
use App\Models\AboutAward;
use App\Models\HomePage;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class AboutPageTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( AboutPage $aboutPage ){

        $lang = Helper::lang();

        $lead_textTR = $aboutPage->translatedAttribute('lead_text');
        $about_block_titleTR = $aboutPage->translatedAttribute('about_block_title');
        $about_block_textTR = $aboutPage->translatedAttribute('about_block_text');
        $departments_block_titleTR = $aboutPage->translatedAttribute('departments_block_title');
        $client_block_titleTR = $aboutPage->translatedAttribute('client_block_title');
        $client_block_textTR = $aboutPage->translatedAttribute('client_block_text');
        $awards_block_titleTR = $aboutPage->translatedAttribute('awards_block_title');
        $awards_block_textTR = $aboutPage->translatedAttribute('awards_block_text');
        $servicesTitleTR = $aboutPage->translatedAttribute('services_title');
        $servicesDescriptionTR = $aboutPage->translatedAttribute('services_text');
        $facts_block_titleTR = $aboutPage->translatedAttribute('facts_block_title');
        $jobs_block_titleTR = $aboutPage->translatedAttribute('jobs_block_title');

        $services = [];
        if (
            isset($aboutPage->services_items_json)
            &&
            is_array($aboutPage->services_items_json)
            &&
            count($aboutPage->services_items_json) > 0
        ){
            foreach( $aboutPage->services_items_json as $services_item ){
                $services[] = !is_null($services_item['title'][$lang])?$services_item['title'][$lang]:(!is_null($services_item['title']['ru'])?$services_item['title']['ru']:null);
            }
        }

        // Тянуть напрямую из ServicesPage попросили, так надо
        $servicesHeaderImage = null;
        $servicesHeaderVideo = null;
        $servicesPage = ServicesPage::where('published', 1)->orderBy('position')->first();
        if( isset($servicesPage) ){

            $shImage = Media::getPictureInfo( 'ServicesPage', 'header_image', $servicesPage->id );
            if( count($shImage) > 0 ){
                $servicesHeaderImage = [
                    'width' => $shImage['image_width'],
                    'height' => $shImage['image_height'],
                    'url' => $shImage['image_url'],
                    'bgColor' => $shImage['imageDominantColor'],
                ];
            }

            if(
                isset($servicesPage->header_vimeo_video_link)
                &&
                strlen($servicesPage->header_vimeo_video_link) > 0
            ){
                $vimeo_video_id = Helper::GetIDFromVimeoLink( $servicesPage->header_vimeo_video_link );

                if( isset( $vimeo_video_id ) ){
                    $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                    if(
                        isset($arInfo) && is_array($arInfo)
                        &&
                        isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                        &&
                        isset($arInfo['files']) && is_array($arInfo['files'])
                    ){
                        $servicesHeaderVideo = Project::getVideoInfo($arInfo);
                    }
                }
            }
        }

        // Facts
        $facts = [];
        $sortedItems = DB::table('about_page_fact')
        ->where('about_page_id', $aboutPage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = Fact::find($sortedItem->fact_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $descrTR = $item->translatedAttribute('description');
                $cover = null;
                $arImage = Media::getPictureInfo('Fact', 'image', $item->id);
                if( count($arImage) > 0 ){
                    $cover = [
                        'url' => $arImage['image_url'],
                        'width' => $arImage['image_width'],
                        'height' => $arImage['image_height'],
                    ];
                    if(
                        isset($arImage['imageDominantColor'])
                        &&
                        is_array($arImage['imageDominantColor'])
                    ){
                        $cover['bgColor'] = $arImage['imageDominantColor'];
                    }
                }
                $facts[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'description' => !is_null($descrTR[$lang])?$descrTR[$lang]:$descrTR['ru'],
                    'cover' => $cover,
                ];
            }
        }

        // Jobs
        $jobs = [];
        $sortedItems = DB::table('about_page_job')
        ->where('about_page_id', $aboutPage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = Job::find($sortedItem->job_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $email = $item->email;
                $jobs[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'email' => $email,
                ];
            }
        }

        // Тянуть напрямую из HomePage попросили, так надо
        $clients = [];  $directions = [];
        $clients_block_titleTR = null;
        $homePage = HomePage::where('published', 1)->orderBy('position')->first();
        if( isset($homePage) ){

            $clients_block_titleTR = $homePage->translatedAttribute('clients_block_title');

            // Clients
            $sortedItems = DB::table('client_home_page')
            ->where('home_page_id', $homePage->id)
            ->orderBy('position')->get();
            foreach ( $sortedItems as $key => $sortedItem ){
                $item = Client::find($sortedItem->client_id);
                if( isset($item) ){
                    $titleTR = $item->translatedAttribute('title');
                    $clients[] = [
                        'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                        'url' => isset($item->link)?$item->link:null,
                    ];
                }
            }

            // Contacts
            $contactsTR = $homePage->translatedAttribute('contacts');
        }

        // Направления
        $sortedItems = DB::table('about_direction_about_page')
        ->where('about_page_id', $aboutPage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = AboutDirection::find($sortedItem->about_direction_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $dirItems = [];
                $blocks = DB::table('blocks')
                ->where('blockable_type', 'App\Models\AboutDirection')
                ->where('blockable_id', $item->id)
                ->orderBy('position')->get();
                if( count($blocks) > 0 ){
                    foreach ( $blocks as $block ){
                        $content = $block->content;
                        $fields = Helper::json_to_array($content);
                        $dirItems[] = [
                            'title' => isset($fields['title'][$lang])?$fields['title'][$lang]:$fields['title']['ru'],
                            'blockId' => $block->id,
                        ];
                    }
                }
                $directions[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'slug' => isset($item->slug_field)?$item->slug_field:null,
                    'items' => $dirItems,
                ];
            }
        }

        // Награды
        $awards = [];
        $sortedItems = DB::table('about_award_about_page')
        ->where('about_page_id', $aboutPage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = AboutAward::find($sortedItem->about_award_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $titleWY_TR = $item->translatedAttribute('title_without_year');
                $image = null;   $imageDark = null;
                $arImage = Media::getPictureInfo('AboutAward', 'image', $item->id);
                if( count($arImage) > 0 ){
                    $image = [
                        'url' => $arImage['image_url'],
                        'width' => $arImage['image_width'],
                        'height' => $arImage['image_height'],
                    ];
                    if(
                        isset($arImage['imageDominantColor'])
                        &&
                        is_array($arImage['imageDominantColor'])
                    ){
                        $image['bgColor'] = $arImage['imageDominantColor'];
                    }
                }
                $arImageDark = Media::getPictureInfo('AboutAward', 'image_dark', $item->id);
                if( count($arImageDark) > 0 ){
                    $imageDark = [
                        'url' => $arImageDark['image_url'],
                        'width' => $arImageDark['image_width'],
                        'height' => $arImageDark['image_height'],
                    ];
                    if(
                        isset($arImageDark['imageDominantColor'])
                        &&
                        is_array($arImageDark['imageDominantColor'])
                    ){
                        $imageDark['bgColor'] = $arImageDark['imageDominantColor'];
                    }
                }

                $projects = [];
                $obProjects = $item->projects()->get();
                foreach ( $obProjects as $project ){
                    $titleTR = $project->translatedAttribute('title');
                    $descriptionTR = $project->translatedAttribute('description');
                    $projectImage = null;
                    $projectBgColor = null;
                    $arImage = Media::getPictureInfo('Project', 'list_image', $project->id);
                    if( count($arImage) > 0 ){
                        $projectBgColor = $arImage['imageDominantColor'];
                        $projectImage = [
                            'url' => $arImage['image_url'],
                            'width' => $arImage['image_width'],
                            'height' => $arImage['image_height'],
                        ];
                    }
                    $projects[] = [
                        'id' => $project->id,
                        'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                        'slug' => isset($project->slug_field)?$project->slug_field:null,
                        'description' => !is_null($descriptionTR[$lang])?$descriptionTR[$lang]:$descriptionTR['ru'],
                        'cover' => $projectImage,
                        'bgColor' => $projectBgColor,
                    ];
                }

                $awards[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'title_without_year' => !is_null($titleWY_TR[$lang])?$titleWY_TR[$lang]:$titleWY_TR['ru'],
                    'year' => $item->year,
                    'count' => $item->count,
                    'image' => $image,
                    'image_dark' => $imageDark,
                    'projects' => $projects,
                ];
            }
        }

        // Клиенты
        $clients = [];
        $sortedItems = DB::table('about_page_client')
        ->where('about_page_id', $aboutPage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = Client::find($sortedItem->client_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');

                $types = [];
                $clCategories = $item->client_categories()->get();
                if( isset($clCategories) && count($clCategories) > 0 ){
                    foreach ( $clCategories as $clCategory ){
                        $clCatTitleTR = $clCategory->translatedAttribute('title');
                        $types[] = [
                            'title' => !is_null($clCatTitleTR[$lang])?$clCatTitleTR[$lang]:$clCatTitleTR['ru'],
                            'slug' => $clCategory->slug_field,
                        ];
                    }
                }

                $clients[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'url' => isset($item->link)?$item->link:null,
                    'types' => $types,
                ];
            }
        }

        $headerImage = null;
        $hImage = Media::getPictureInfo( 'AboutPage', 'header_image', $aboutPage->id );
        if( count($hImage) > 0 ){
            $headerImage = [
                'width' => $hImage['image_width'],
                'height' => $hImage['image_height'],
                'url' => $hImage['image_url'],
                'bgColor' => $hImage['imageDominantColor'],
            ];
        }

        $headerVideo = null;
        if(
            isset($aboutPage->header_vimeo_video_link)
            &&
            strlen($aboutPage->header_vimeo_video_link) > 0
        ){
            $vimeo_video_id = Helper::GetIDFromVimeoLink( $aboutPage->header_vimeo_video_link );
            if( isset( $vimeo_video_id ) ){
                $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                if(
                    isset($arInfo) && is_array($arInfo)
                    &&
                    isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                    &&
                    isset($arInfo['files']) && is_array($arInfo['files'])
                ){
                    $headerVideo = Project::getVideoInfo($arInfo);
                }
            }
        }

        $showreelImage = null;
        $shImage = Media::getPictureInfo( 'AboutPage', 'header_image', $aboutPage->id );
        if( count($shImage) > 0 ){
            $showreelImage = [
                'width' => $shImage['image_width'],
                'height' => $shImage['image_height'],
                'url' => $shImage['image_url'],
                'bgColor' => $shImage['imageDominantColor'],
            ];
        }

        $showreelVideo = null;
        if(
            isset($aboutPage->showreel_vimeo_video_link)
            &&
            strlen($aboutPage->showreel_vimeo_video_link) > 0
        ){
            $vimeo_video_id = Helper::GetIDFromVimeoLink( $aboutPage->showreel_vimeo_video_link );
            if( isset( $vimeo_video_id ) ){
                $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                if(
                    isset($arInfo) && is_array($arInfo)
                    &&
                    isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                    &&
                    isset($arInfo['files']) && is_array($arInfo['files'])
                ){
                    $showreelVideo = Project::getVideoInfo($arInfo);
                }
            }
        }

        $arItem = [[
            'data' => [
                'header_video' => $headerVideo,
                'header_image' => $headerImage,
                'showreel' => [
                    'cover' => $showreelImage,
                    'video' => $showreelVideo,
                ],
                'lead_text' => isset($lead_textTR[$lang])?$lead_textTR[$lang]:$lead_textTR['ru'],
                'jobs' => [
                    'title' => !is_null($jobs_block_titleTR[$lang])?$jobs_block_titleTR[$lang]:$jobs_block_titleTR['ru'],
                    'items' => $jobs,
                ],
                'facts' => [
                    'title' => !is_null($facts_block_titleTR[$lang])?$facts_block_titleTR[$lang]:$facts_block_titleTR['ru'],
                    'items' => $facts,
                ],
                'clients' => [
                    'title' => !is_null($clients_block_titleTR[$lang])?$clients_block_titleTR[$lang]:$clients_block_titleTR['ru'],
                    'items' => $clients,
                ],
                'services' => [
                    'title' => isset($servicesTitleTR[$lang])?$servicesTitleTR[$lang]:$servicesTitleTR['ru'],
                    'text' => isset($servicesDescriptionTR[$lang])?$servicesDescriptionTR[$lang]:$servicesDescriptionTR['ru'],
                    'items' => $services
                ],
                'contacts' => isset($contactsTR)?(isset($contactsTR[$lang])?$contactsTR[$lang]:$contactsTR['ru']):null,
                'about_block' => [
                    'title' => isset($about_block_titleTR[$lang])?$about_block_titleTR[$lang]:$about_block_titleTR['ru'],
                    'text' => isset($about_block_textTR[$lang])?$about_block_textTR[$lang]:$about_block_textTR['ru'],
                ],
                'departments_block' => [
                    'title' => isset($departments_block_titleTR[$lang])?$departments_block_titleTR[$lang]:$departments_block_titleTR['ru'],
                    'items' => $directions,
                ],
                'clients_block' => [
                    'title' => isset($client_block_titleTR[$lang])?$client_block_titleTR[$lang]:$client_block_titleTR['ru'],
                    'items' => $clients,
                ],
                'awards_block' => [
                    'title' => isset($awards_block_titleTR[$lang])?$awards_block_titleTR[$lang]:$awards_block_titleTR['ru'],
                    'text' => isset($awards_block_textTR[$lang])?$awards_block_textTR[$lang]:$awards_block_textTR['ru'],
                    'items' => $awards,
                ],
                'services_header' => [
                    'video' => $servicesHeaderVideo,
                    'image' => $servicesHeaderImage,
                ]
            ]
        ]];

        return $arItem;
    }



}
