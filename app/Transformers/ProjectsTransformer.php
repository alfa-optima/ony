<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use League\Fractal\TransformerAbstract;
use App\Models\Project;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ProjectsTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( Project $project ){

        $lang = Helper::lang();

        $transTitles = $project->translatedAttribute('title');
        //$transHeader = $project->translatedAttribute('header');
        $transDescription = $project->translatedAttribute('description');
        //$transLongDescription = $project->translatedAttribute('long_description');

        $default_category_name = null;
        if( isset($project->default_category) && intval($project->default_category) > 0 ){
            $default_category = DB::table('project_categories')
            ->where('id', $project->default_category)
            ->first();
            if( isset( $default_category ) ){
                $defCatTitles = $default_category->translatedAttribute('title');
                $default_category_name = !is_null($defCatTitles[$lang])?$defCatTitles[$lang]:$defCatTitles['ru'];
            }
        }

        $arItem = [
            'id' => (int) $project->id,
            'slug' => isset($project->slug_field)?$project->slug_field:null,
            'title' => !is_null($transTitles[$lang])?$transTitles[$lang]:$transTitles['ru'],
            //'header' => !is_null($transHeader[$lang])?$transHeader[$lang]:$transHeader['ru'],
            'description' => !is_null($transDescription[$lang])?$transDescription[$lang]:$transDescription['ru'],
            //'long_description' => !is_null($transLongDescription[$lang])?$transLongDescription[$lang]:$transLongDescription['ru'],
            'cover_image' => null,
            'cover_video' => null,
            'default_category' => $default_category_name,
        ];

        $picInfo = Media::getPictureInfo( 'Project', 'list_image', $project->id );
        if( is_array($picInfo) && count($picInfo) > 0 ){
            $arItem['cover_image'] = [
                'url' => $picInfo['image_url'],
                'width' => $picInfo['image_width'],
                'height' => $picInfo['image_height'],
            ];
            if(
                isset($picInfo['imageDominantColor'])
                &&
                is_array($picInfo['imageDominantColor'])
            ){
                $arItem['cover_image']['bgColor'] = $picInfo['imageDominantColor'];
            }
        }

        if(
            isset($project->video_mp4_link)
            &&
            strlen($project->video_mp4_link) > 0
        ){
            $vimeo_video_id = Helper::GetIDFromVimeoLink( $project->video_mp4_link );
            if( isset( $vimeo_video_id ) ){
                $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                if(
                    isset($arInfo) && is_array($arInfo)
                    &&
                    isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                    &&
                    isset($arInfo['files']) && is_array($arInfo['files'])
                ){
                    $arItem['cover_video'] = Project::getVideoInfo($arInfo);
                }
            }
        }

        return $arItem;
    }



}
