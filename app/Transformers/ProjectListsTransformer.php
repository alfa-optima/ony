<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use League\Fractal\TransformerAbstract;
use App\Models\ProjectList;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ProjectListsTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
    */

    public function transform( ProjectList $projectList ){

        $lang = Helper::lang();

        $transTitles = $projectList->translatedAttribute('title');
        $trans_link_title = $projectList->translatedAttribute('link_title');

        $arItem = [
            'id' => (int) $projectList->id,
            'slug' => isset($projectList->slug_field)?$projectList->slug_field:null,
            'title' => !is_null($transTitles[$lang])?$transTitles[$lang]:$transTitles['ru'],
        ];

        return $arItem;
    }



}
