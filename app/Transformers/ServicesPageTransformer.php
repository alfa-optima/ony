<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use App\Models\Client;
use App\Models\Project;
use App\Models\Service;
use League\Fractal\TransformerAbstract;
use App\Models\ServicesPage;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ServicesPageTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( ServicesPage $servicesPage ){

        $lang = Helper::lang();

        $page_titleTR = $servicesPage->translatedAttribute('page_title');
        $lead_textTR = $servicesPage->translatedAttribute('lead_text');
        $body_titleTR = $servicesPage->translatedAttribute('body_title');
        $body_textTR = $servicesPage->translatedAttribute('body_text');
        $services_titleTR = $servicesPage->translatedAttribute('services_title');
        $items_titleTR = $servicesPage->translatedAttribute('items_title');
        $contactsTR = $servicesPage->translatedAttribute('contacts');

        // Услуги
        $services = [];
        $sortedItems = DB::table('service_services_page')
        ->where('services_page_id', $servicesPage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = Service::find($sortedItem->service_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $textTR = $item->translatedAttribute('text');
                $subServices = [];
                if (
                    isset($item->items_json)
                    &&
                    is_array($item->items_json)
                    &&
                    count($item->items_json) > 0
                ){
                    foreach( $item->items_json as $subService ){
                        $subServices[] = [
                            'title' => !is_null($subService['title'][$lang])?$subService['title'][$lang]:(!is_null($subService['title']['ru'])?$subService['title']['ru']:null),
                            'text' => !is_null($subService['text'][$lang])?$subService['text'][$lang]:(!is_null($subService['text']['ru'])?$subService['text']['ru']:null),
                        ];
                    }
                }
                $services[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    //'text' => !is_null($textTR[$lang])?$textTR[$lang]:$textTR['ru'],
                    'items' => $subServices,
                ];
            }
        }

        $headerImage = null;
        $hImage = Media::getPictureInfo( 'ServicesPage', 'header_image', $servicesPage->id );
        if( count($hImage) > 0 ){
            $headerImage = [
                'width' => $hImage['image_width'],
                'height' => $hImage['image_height'],
                'url' => $hImage['image_url'],
                'bgColor' => $hImage['imageDominantColor'],
            ];
        }

        $headerVideo = null;
        if(
            isset($servicesPage->header_vimeo_video_link)
            &&
            strlen($servicesPage->header_vimeo_video_link) > 0
        ){
            $vimeo_video_id = Helper::GetIDFromVimeoLink( $servicesPage->header_vimeo_video_link );

            if( isset( $vimeo_video_id ) ){
                $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                if(
                    isset($arInfo) && is_array($arInfo)
                    &&
                    isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                    &&
                    isset($arInfo['files']) && is_array($arInfo['files'])
                ){
                    $headerVideo = Project::getVideoInfo($arInfo);
                }
            }
        }

        $arItem = [[
            'data' => [
                'page_title' => isset($page_titleTR[$lang])?$page_titleTR[$lang]:$page_titleTR['ru'],
                'header' => [
                    'video' => $headerVideo,
                    'image' => $headerImage,
                ],
                'lead_text' => isset($lead_textTR[$lang])?$lead_textTR[$lang]:$lead_textTR['ru'],
                'body' => [
                    'title' => isset($body_titleTR[$lang])?$body_titleTR[$lang]:$body_titleTR['ru'],
                    'text' => isset($body_textTR[$lang])?$body_textTR[$lang]:$body_textTR['ru'],
                ],
                'services' => [
                    'title' => isset($services_titleTR[$lang])?$services_titleTR[$lang]:$services_titleTR['ru'],
                    'items' => $services
                ],
                'contacts' => isset($contactsTR)?(isset($contactsTR[$lang])?$contactsTR[$lang]:$contactsTR['ru']):null,
            ]
        ]];

        return $arItem;
    }



}
