<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use App\Models\Social;
use App\Models\Shell;
use League\Fractal\TransformerAbstract;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ShellPageTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( Shell $shell ){

        $lang = Helper::lang();

        $seo_titleTR = $shell->translatedAttribute('seo_title');
        $seo_descriptionTR = $shell->translatedAttribute('seo_description');
        $seo_ogDescriptionTR = $shell->translatedAttribute('seo_ogDescription');
        $footer_address_titleTR = $shell->translatedAttribute('footer_address_title');
        $footer_address_contentTR = $shell->translatedAttribute('footer_address_content');
        $footer_newBusinessContacts_titleTR = $shell->translatedAttribute('footer_newBusinessContacts_title');
        $footer_newBusinessContacts_contentTR = $shell->translatedAttribute('footer_newBusinessContacts_content');
        $footer_otherContacts_titleTR = $shell->translatedAttribute('footer_otherContacts_title');
        $footer_otherContacts_contentTR = $shell->translatedAttribute('footer_otherContacts_content');
        $footer_social_block_titleTR = $shell->translatedAttribute('footer_social_block_title');

        $ogImage = null;
        $arImage = Media::getPictureInfo('Shell', 'seo_ogImage', $shell->id);
        if( count($arImage) > 0 ){
            $ogImage = [
                'url' => $arImage['image_url'],
                'width' => $arImage['image_width'],
                'height' => $arImage['image_height'],
            ];
            if(
                isset($arImage['imageDominantColor'])
                &&
                is_array($arImage['imageDominantColor'])
            ){
                $ogImage['bgColor'] = $arImage['imageDominantColor'];
            }
        }

        // Socials
        $socials = [];
        $sortedItems = DB::table('shell_social')
        ->where('shell_id', $shell->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = Social::find($sortedItem->social_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $socials[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'url' => isset($item->url)?$item->url:null,
                ];
            }
        }

        $arItem = [[
            'data' => [
                'seo' => [
                    'title' => !is_null($seo_titleTR[$lang])?$seo_titleTR[$lang]:$seo_titleTR['ru'],
                    'description' => !is_null($seo_descriptionTR[$lang])?$seo_descriptionTR[$lang]:$seo_descriptionTR['ru'],
                    'ogDescription' => $seo_ogDescriptionTR[$lang],
                    'ogImage' => $ogImage,
                ],
                'footer' => [
                    'address' => [
                        'title' => !is_null($footer_address_titleTR[$lang])?$footer_address_titleTR[$lang]:$footer_address_titleTR['ru'],
                        'content' => !is_null($footer_address_contentTR[$lang])?$footer_address_contentTR[$lang]:$footer_address_contentTR['ru'],
                    ],
                    'newBusinessContacts' => [
                        'title' => !is_null($footer_newBusinessContacts_titleTR[$lang])?$footer_newBusinessContacts_titleTR[$lang]:$footer_newBusinessContacts_titleTR['ru'],
                        'content' => !is_null($footer_newBusinessContacts_contentTR[$lang])?$footer_newBusinessContacts_contentTR[$lang]:$footer_newBusinessContacts_contentTR['ru'],
                    ],
                    'otherContacts' => [
                        'title' => !is_null($footer_otherContacts_titleTR[$lang])?$footer_otherContacts_titleTR[$lang]:$footer_otherContacts_titleTR['ru'],
                        'content' => !is_null($footer_otherContacts_contentTR[$lang])?$footer_otherContacts_contentTR[$lang]:$footer_otherContacts_contentTR['ru'],
                    ],
                    'socials' => $socials,
                ],
            ]
        ]];

        return $arItem;
    }



}
