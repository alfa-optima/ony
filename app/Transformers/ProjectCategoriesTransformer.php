<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use League\Fractal\TransformerAbstract;
use App\Models\ProjectCategory;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ProjectCategoriesTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
    */

    public function transform( ProjectCategory $projectCategory ){

        $lang = Helper::lang();

        $transTitles = $projectCategory->translatedAttribute('title');

        $arItem = [
            'id' => (int) $projectCategory->id,
            'slug' => isset($projectCategory->slug_field)?$projectCategory->slug_field:null,
            'title' => !is_null($transTitles[$lang])?$transTitles[$lang]:$transTitles['ru'],
        ];

        return $arItem;
    }



}
