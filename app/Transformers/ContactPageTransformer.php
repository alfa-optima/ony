<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use App\Models\ContactPage;
use League\Fractal\TransformerAbstract;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class ContactPageTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( ContactPage $contactPage ){

        $lang = Helper::lang();

        $image = null;
        $contactsImage = Media::getPictureInfo( 'ContactPage', 'contacts_image', $contactPage->id );
        if( count($contactsImage) > 0 ){
            $image = [
                'width' => $contactsImage['image_width'],
                'height' => $contactsImage['image_height'],
                'url' => $contactsImage['image_url'],
                'bgColor' => $contactsImage['imageDominantColor'],
            ];
        }

        $arItem = [
            'data' => [
                'cover' => $image,
                'items' => []
            ]
        ];

        // Блоки
        $blocks = DB::table('blocks')
        ->where('blockable_id', $contactPage->id)
        ->where('blockable_type', 'App\Models\ContactPage')
        ->orderBy('position', 'asc')->get();
        foreach ( $blocks as $block ){
            $content = Helper::json_to_array($block->content);
            $title = isset($content['title'][$lang])?$content['title'][$lang]:(isset($content['title']['ru'])?$content['title']['ru']:null);
            $text = isset($content['text'][$lang])?$content['text'][$lang]:(isset($content['text']['ru'])?$content['text']['ru']:null);
            $arBlock = [
                'title' => $title,
                'text' => $text,
            ];
            $arItem['data']['items'][] = $arBlock;
        }

        return $arItem;
    }



}
