<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use App\Models\AboutAward;
use App\Models\AboutPage;
use App\Models\ServicesPage;
use League\Fractal\TransformerAbstract;
use App\Models\HomePage;
use App\Models\Job;
use App\Models\Fact;
use App\Models\Client;
use App\Models\Project;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;


class HomePageTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( HomePage $homePage ){

        $lang = Helper::lang();

        $lead_textTR = $homePage->translatedAttribute('lead_text');
        $clients_block_titleTR = $homePage->translatedAttribute('clients_block_title');
        $about_block_titleTR = $homePage->translatedAttribute('about_block_title');
        $about_block_textTR = $homePage->translatedAttribute('about_block_text');

        // Clients
        $clients = [];
        $sortedItems = DB::table('client_home_page')
        ->where('home_page_id', $homePage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = Client::find($sortedItem->client_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $logo = null;
                $arLogo = Media::getPictureInfo('Client', 'logo', $item->id);
                if( count($arLogo) > 0 ){
                    $logo = [
                        'url' => $arLogo['image_url'],
                        'width' => $arLogo['image_width'],
                        'height' => $arLogo['image_height'],
                    ];
                    if(
                        isset($arLogo['imageDominantColor'])
                        &&
                        is_array($arLogo['imageDominantColor'])
                    ){
                        $logo['bgColor'] = $arLogo['imageDominantColor'];
                    }
                }
                $logoDark = null;
                $arLogoDark = Media::getPictureInfo('Client', 'logo_dark', $item->id);
                if( count($arLogoDark) > 0 ){
                    $logoDark = [
                        'url' => $arLogoDark['image_url'],
                        'width' => $arLogoDark['image_width'],
                        'height' => $arLogoDark['image_height'],
                    ];
                    if(
                        isset($arLogoDark['imageDominantColor'])
                        &&
                        is_array($arLogoDark['imageDominantColor'])
                    ){
                        $logoDark['bgColor'] = $arLogoDark['imageDominantColor'];
                    }
                }
                $clients[] = [
                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                    'url' => isset($item->link)?$item->link:null,
                    'logo' => $logo,
                    'logo_dark' => $logoDark,
                ];
            }
        }

        // Блоки
        $arBlocks = [];
        $blocks = DB::table('blocks')->where('blockable_type', 'App\Models\HomePage')->where('blockable_id', $homePage->id)->orderBy('position')->get();
        if( count($blocks) > 0 ){
            foreach ( $blocks as $block ){
                $content = $block->content;
                $fields = Helper::json_to_array($content);
                if( $block->type == 'home_text_block' ){
                    $arBlocks[] = [
                        'type' => 'text',
                        'data' => [
                            'text' => isset($fields['text'][$lang])?$fields['text'][$lang]:(isset($fields['text']['ru'])?$fields['text']['ru']:null),
                            'link_title' => isset($fields['link_title'][$lang])?$fields['link_title'][$lang]:(isset($fields['link_title']['ru'])?$fields['link_title']['ru']:null),
                            'link' => isset($fields['link'])?$fields['link']:null,
                        ]
                    ];
                } else if( $block->type == 'home_project_block' ){
                    if(
                        isset($fields['browsers']['projects'])
                        &&
                        is_array($fields['browsers']['projects'])
                        &&
                        count($fields['browsers']['projects']) > 0
                    ){
                        $blockProjects = [];
                        foreach ( $fields['browsers']['projects'] as $project_id ){
                            $project = Project::find($project_id);
                            if( isset($project) ){
                                $titleTR = $project->translatedAttribute('title');
                                $descriptionTR = $project->translatedAttribute('description');
                                $image = null;  $bgColor = null;
                                $arImage = Media::getPictureInfo('Project', 'list_image', $project->id);
                                if( count($arImage) > 0 ){
                                    $bgColor = $arImage['imageDominantColor'];
                                    $image = [
                                        'url' => $arImage['image_url'],
                                        'width' => $arImage['image_width'],
                                        'height' => $arImage['image_height'],
                                    ];
                                }
                                $blockProjects[] = [
                                    'id' => $project->id,
                                    'title' => !is_null($titleTR[$lang])?$titleTR[$lang]:$titleTR['ru'],
                                    'slug' => isset($project->slug_field)?$project->slug_field:null,
                                    'description' => !is_null($descriptionTR[$lang])?$descriptionTR[$lang]:$descriptionTR['ru'],
                                    'cover' => $image,
                                    'bgColor' => $bgColor,
                                ];
                            }
                        }
                        $arBlocks[] = [
                            'type' => 'projects',
                            'data' => [
                                'projects' => $blockProjects,
                            ]
                        ];
                    }
                }
            }
        }

        // videoPoster
        $videoPoster = null;
        $arImage = Media::getPictureInfo('HomePage', 'about_image', $homePage->id);
        if( count($arImage) > 0 ){
            $videoPoster = [
                'url' => $arImage['image_url'],
                'width' => $arImage['image_width'],
                'height' => $arImage['image_height'],
            ];
            if(
                isset($arImage['imageDominantColor'])
                &&
                is_array($arImage['imageDominantColor'])
            ){
                $videoPoster['bgColor'] = $arImage['imageDominantColor'];
            }
        }

        // video
        $video = null;
        $obVideo = DB::table('fileables as fa')
        ->leftJoin('files as f', 'f.id', '=', 'fa.file_id')
        ->select(
            'f.id as id', 'f.uuid as uuid', 'fa.fileable_id as fileable_id',
            'fa.file_id as file_id', 'fa.fileable_type as fileable_type', 'fa.role as role'
        )
        ->where('fa.fileable_type', 'App\Models\HomePage')
        ->where('fa.role', 'about_video')
        ->where('fa.fileable_id', $homePage->id)
        ->first();
        if( isset($obVideo->id) && intval($obVideo->id) > 0 ){
            $video = env('REQUEST_SCHEME').'://'.env('APP_URL').'/file/'.$obVideo->uuid;
        }


        // Награды
        $awards_block_titleTR = null;
        $awards_block_textTR = null;
        $aboutPage = AboutPage::where('published', 1)->orderBy('position')->first();
        if( isset($aboutPage) ){
            $awards_block_titleTR = $aboutPage->translatedAttribute('awards_block_title');
            $awards_block_textTR = $aboutPage->translatedAttribute('awards_block_text');
        }
        $awards = [];
        $sortedItems = DB::table('about_award_home_page')
        ->where('home_page_id', $homePage->id)
        ->orderBy('position')->get();
        foreach ( $sortedItems as $key => $sortedItem ){
            $item = AboutAward::find($sortedItem->about_award_id);
            if( isset($item) ){
                $titleTR = $item->translatedAttribute('title');
                $titleWY_TR = $item->translatedAttribute('title_without_year');
                $image = null;
                $arImage = Media::getPictureInfo('AboutAward', 'image', $item->id);
                if( count($arImage) > 0 ){
                    $image = [
                        'url' => $arImage['image_url'],
                        'width' => $arImage['image_width'],
                        'height' => $arImage['image_height'],
                    ];
                    if(
                        isset($arImage['imageDominantColor'])
                        &&
                        is_array($arImage['imageDominantColor'])
                    ){
                        $image['bgColor'] = $arImage['imageDominantColor'];
                    }
                }
                $imageDark = null;
                $arImageDark = Media::getPictureInfo('AboutAward', 'image_dark', $item->id);
                if( count($arImageDark) > 0 ){
                    $imageDark = [
                        'url' => $arImageDark['image_url'],
                        'width' => $arImageDark['image_width'],
                        'height' => $arImageDark['image_height'],
                    ];
                    if(
                        isset($arImageDark['imageDominantColor'])
                        &&
                        is_array($arImageDark['imageDominantColor'])
                    ){
                        $imageDark['bgColor'] = $arImageDark['imageDominantColor'];
                    }
                }
                $awards[] = [
                    'image' => $image,
                    'image_dark' => $imageDark,
                ];
            }
        }



        $arItem = [[
            'data' => [
                'lead_text' => !is_null($lead_textTR[$lang])?$lead_textTR[$lang]:$lead_textTR['ru'],
                'clients' => [
                    'title' => !is_null($clients_block_titleTR[$lang])?$clients_block_titleTR[$lang]:$clients_block_titleTR['ru'],
                    'items' => $clients,
                ],
                'about' => [
                    'title' => !is_null($about_block_titleTR[$lang])?$about_block_titleTR[$lang]:$about_block_titleTR['ru'],
                    'text' => !is_null($about_block_textTR[$lang])?$about_block_textTR[$lang]:$about_block_textTR['ru'],
                    'video' => $video,
                    'video_poster' => $videoPoster,
                ],
                'awards' => [
                    'title' => !is_null($awards_block_titleTR[$lang])?$awards_block_titleTR[$lang]:$awards_block_titleTR['ru'],
                    'text' => !is_null($awards_block_textTR[$lang])?$awards_block_textTR[$lang]:$awards_block_textTR['ru'],
                    'items' => $awards
                ],
                'blocks' => $arBlocks,
            ]
        ]];

        return $arItem;
    }



}
