<?php

namespace App\Transformers;
use App\Classes\Helper;
use App\Classes\Media;
use App\Models\Client;
use League\Fractal\TransformerAbstract;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\SiteSection;
use App\Transformers\SeminarTransformer;
use Illuminate\Support\Facades\DB;


class ProjectTransformer extends TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */

    protected $defaultIncludes = [];


    /**
     * List of resources possible to include
     *
     * @var array
     */

    protected $availableIncludes = [];


    /**
     * A Fractal transformer.
     *
     * @return array
     */

    public function transform( $project ){

        $lang = Helper::lang();

        if( isset($project) ){

            $transTitles = $project->translatedAttribute('title');
            $transHeader = $project->translatedAttribute('header');
            $transDescription = $project->translatedAttribute('description');
            $transLongDescription = $project->translatedAttribute('long_description');

            $header_video = null;
            if(
                isset($project->video_mp4_link)
                &&
                strlen($project->video_mp4_link) > 0
            ){
                preg_match(
                    Project::VIMEO_LINK_REGEX,
                    $project->video_mp4_link,
                    $matches, PREG_OFFSET_CAPTURE
                );
                if( isset( $matches[2][0] ) ){
                    $vimeo_video_id = $matches[2][0];
                    $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                    if(
                        isset($arInfo) && is_array($arInfo)
                        &&
                        isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                        &&
                        isset($arInfo['files']) && is_array($arInfo['files'])
                    ){
                        $header_video = Project::getVideoInfo($arInfo);
                    }
                }
            }

            $header_image = null;
            $posterImage = Media::getPictureInfo( 'Project', 'poster', $project->id );
            if( count($posterImage) > 0 ){
                $header_image = [
                    'width' => $posterImage['image_width'],
                    'height' => $posterImage['image_height'],
                    'url' => $posterImage['image_url'],
                    'bgColor' => $posterImage['imageDominantColor'],
                ];
            } else {
                $listImage = Media::getPictureInfo( 'Project', 'list_image', $project->id );
                if( count($listImage) > 0 ){
                    $header_image = [
                        'width' => $listImage['image_width'],
                        'height' => $listImage['image_height'],
                        'url' => $listImage['image_url'],
                        'bgColor' => $listImage['imageDominantColor'],
                    ];
                }
            }

            $default_category_name = null;
            if( isset($project->section_id) && intval($project->section_id) > 0 ){
                $SiteSection = SiteSection::find($project->section_id);
                if( isset($SiteSection) ){
                    $SiteSectionTransTitles = $SiteSection->translatedAttribute('title');
                    $default_category_name = !is_null($SiteSectionTransTitles[$lang])?$SiteSectionTransTitles[$lang]:$SiteSectionTransTitles['ru'];
                }
            }

            // Следующий проект
            $arNextProject = null;
            $next_project = Project::where('id', '<', $project->id)
            ->where('published', 1)->orderBy('id', 'desc')->first();
            if( isset($next_project) ){

                $next_transTitles = $next_project->translatedAttribute('title');
                $next_transHeader = $next_project->translatedAttribute('header');
                $next_transDescription = $next_project->translatedAttribute('description');
                $next_transLongDescription = $next_project->translatedAttribute('long_description');

                $next_header_video = null;
                if(
                    isset($next_project->video_mp4_link)
                    &&
                    strlen($next_project->video_mp4_link) > 0
                ){
                    preg_match(
                        Project::VIMEO_LINK_REGEX,
                        $next_project->video_mp4_link,
                        $matches, PREG_OFFSET_CAPTURE
                    );
                    if( isset( $matches[2][0] ) ){
                        $vimeo_video_id = $matches[2][0];
                        $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                        if(
                            isset($arInfo) && is_array($arInfo)
                            &&
                            isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                            &&
                            isset($arInfo['files']) && is_array($arInfo['files'])
                        ){
                            $next_header_video = Project::getVideoInfo($arInfo);
                        }
                    }
                }

                $next_header_image = null;
                $posterImage = Media::getPictureInfo( 'Project', 'poster', $next_project->id );
                if( count($posterImage) > 0 ){
                    $next_header_image = [
                        'width' => $posterImage['image_width'],
                        'height' => $posterImage['image_height'],
                        'url' => $posterImage['image_url'],
                        'bgColor' => $posterImage['imageDominantColor'],
                    ];
                }

                $arNextProject = [
                    'id' => (int) $next_project->id,
                    'slug' => isset($next_project->slug_field)?$next_project->slug_field:null,
                    'title' => !is_null($next_transTitles[$lang])?$next_transTitles[$lang]:$next_transTitles['ru'],
                    'header_video' => $next_header_video,
                    'header_image' => $next_header_image
                ];
            }

            // Получим другие проекты такого же клиента
            $other_projects_for_client = [];
            $client = null;
            if( isset($project->client_id) && intval($project->client_id) > 0 ){
                $other_cl_projects = DB::table('projects as p')
                ->leftJoin('project_translations as pt', 'pt.project_id', '=', 'p.id')
                ->select(
                    'p.id as id', 'p.client_id as client_id', 'p.slug_field as slug',
                    'p.video_mp4_link as video_mp4_link',
                    'pt.title as title', 'pt.description as description'
                )
                ->where('p.id', '<>', $project->id )
                ->where('pt.locale', 'ru' )
                ->where('p.client_id', $project->client_id )
                ->orderBy('id', 'desc')
                //->limit(10)
                ->get();
                foreach ( $other_cl_projects as $key => $otherClProject ){
                    // cover_image
                    $otherClProjectCoverImage = null;
                    $picInfo = Media::getPictureInfo( 'Project', 'list_image', $otherClProject->id );
                    if( is_array($picInfo) && count($picInfo) > 0 ){
                        $otherClProjectCoverImage = [
                            'url' => $picInfo['image_url'],
                            'width' => $picInfo['image_width'],
                            'height' => $picInfo['image_height'],
                        ];
                        if(
                            isset($picInfo['imageDominantColor'])
                            &&
                            is_array($picInfo['imageDominantColor'])
                        ){
                            $otherClProjectCoverImage['bgColor'] = $picInfo['imageDominantColor'];
                        }
                    }
                    // cover_video
                    $otherClProjectCoverVideo = null;
                    if(
                        isset($otherClProject->video_mp4_link)
                        &&
                        strlen($otherClProject->video_mp4_link) > 0
                    ){
                        $vimeo_video_id = Helper::GetIDFromVimeoLink( $otherClProject->video_mp4_link );
                        if( isset( $vimeo_video_id ) ){
                            $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                            if(
                                isset($arInfo) && is_array($arInfo)
                                &&
                                isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                                &&
                                isset($arInfo['files']) && is_array($arInfo['files'])
                            ){
                                $otherClProjectCoverVideo = Project::getVideoInfo($arInfo);
                            }
                        }
                    }
                    $other_projects_for_client[] = [
                        'title' => $otherClProject->title,
                        'description' => $otherClProject->description,
                        'cover_image' => $otherClProjectCoverImage,
                        'cover_video' => $otherClProjectCoverVideo,
                        'slug' => $otherClProject->slug,
                    ];
                }

                // Инфо по клиенту
                $obClient = Client::find($project->client_id);
                $clTitlesTr = $obClient->translatedAttribute('title');
                $clTypes = [];
                $clCategories = $obClient->client_categories()->get();
                if( count($clCategories) > 0 ){
                    foreach ( $clCategories as $clCategory ){
                        $clCatTitlesTr = $clCategory->translatedAttribute('title');
                        $clTypes[] = [
                            'title' => !is_null($clCatTitlesTr[$lang])?$clCatTitlesTr[$lang]:$clCatTitlesTr['ru'],
                            'slug_field' => $clCategory->slug_field,
                        ];
                    }
                }
                $client = [
                    'title' => !is_null($clTitlesTr[$lang])?$clTitlesTr[$lang]:$clTitlesTr['ru'],
                    'url' => $project->link,
                    'types' => $clTypes,
                ];

            }

            $arItem = [
                'data' => [
                    'id' => (int) $project->id,
                    'slug' => isset($project->slug_field)?$project->slug_field:null,
                    'url' => isset($project->site_link)?$project->site_link:null,
                    'title' => !is_null($transTitles[$lang])?$transTitles[$lang]:$transTitles['ru'],
                    'header' => !is_null($transHeader[$lang])?$transHeader[$lang]:$transHeader['ru'],
                    'description' => !is_null($transDescription[$lang])?$transDescription[$lang]:$transDescription['ru'],
                    'long_description' => !is_null($transLongDescription[$lang])?$transLongDescription[$lang]:$transLongDescription['ru'],
                    'default_category' => $default_category_name,
                    'header_video' => $header_video,
                    'header_image' => $header_image,
                    'next_project' => $arNextProject,
                    'client' => $client,
                    'other_projects_for_client' => $other_projects_for_client,
                    'blocks' => []
                ]
            ];

            // Блоки
            $blocks = DB::table('blocks')
            ->where('blockable_id', $project->id)
            ->where('blockable_type', 'App\Models\Project')
            ->orderBy('position', 'asc')->get();
            foreach( $blocks as $block ){

                $content = Helper::json_to_array($block->content);

                $video = null;
                if(
                    isset($content['project_block_video_mp4_link'])
                    &&
                    strlen($content['project_block_video_mp4_link']) > 0
                ){
                    $vimeo_video_id = Helper::getIDFromVimeoLink($content['project_block_video_mp4_link']);
                    if( isset( $vimeo_video_id ) ){
                        if(
                            isset($content['use_vimeo_player'])
                            &&
                            is_bool($content['use_vimeo_player'])
                            &&
                            $content['use_vimeo_player'] == true
                        ){
                            $video = [ 'vimeo_id' => $vimeo_video_id ];
                        } else {
                            $arInfo = Project::getVimeoInfo( $vimeo_video_id );
                            if(
                                isset($arInfo) && is_array($arInfo)
                                &&
                                isset($arInfo['pictures']) && is_array($arInfo['pictures'])
                                &&
                                isset($arInfo['files']) && is_array($arInfo['files'])
                            ){
                                $video = Project::getVideoInfo($arInfo);
                            }
                        }
                    }
                }

                $text = isset($content['text'][$lang])?$content['text'][$lang]:(isset($content['text']['ru'])?$content['text']['ru']:null);

                $images = null;
                $arImages = Media::getPicturesInfo( 'blocks', 'project_block_images', $block->id );
                if( count($arImages) > 0 ){
                    $images = [];
                    foreach ( $arImages as $arImage ){
                        $images[] = [
                            'width' => $arImage['image_width'],
                            'height' => $arImage['image_height'],
                            'url' => $arImage['image_url'],
                            'bgColor' => $arImage['imageDominantColor'],
                        ];
                    }
                }

                $gallery_style = null;
                if(
                    isset($content['gallery_style'])
                    &&
                    strlen($content['gallery_style']) > 0
                ){    $gallery_style = $content['gallery_style'];    }

                $arBlock = [
                    'text' => $text,
                    'images' => $images,
                    'use_slider' => (isset($content['use_slider']) && $content['use_slider']==1)?true:false,
                    'video' => $video,
                    'gallery_style' => $gallery_style,
                ];

                $arItem['data']['blocks'][] = $arBlock;
            }

            return $arItem;
        }

        return null;
    }












}
