<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ProjectDirectionController extends ModuleController
{
    protected $moduleName = 'projectDirections';



    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => true,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];



    protected $indexColumns = [
        'published' => [
            'title' => 'Active',
            'field' => 'published',
        ],
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'published' => [
            'title' => 'Активность',
            'field' => 'published',
        ],
        'slug_field' => [
            'title' => 'Слаг',
            'field' => 'slug_field',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];


}
