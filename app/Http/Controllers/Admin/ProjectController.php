<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Classes\Helper;
use Illuminate\Support\Facades\DB;
use App\Models\ProjectDirection;
use App\Models\Client;
use App\Models\Project;
use Cocur\Slugify\Slugify;



class ProjectController extends ModuleController {

    protected $moduleName = 'projects';


    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];



    protected $indexColumns = [
        'list_image' => [
            'thumb' => true,
        ],
        'published' => [
            'title' => 'Активность',
            'field' => 'published',
        ],
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'slug_field' => [
            'title' => 'Слаг',
            'field' => 'slug_field',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];



    protected $perPage = 10;

    protected $defaultOrders = ['id' => 'desc'];





    protected function formData($request){

        preg_match(Project::EDIT_PAGE_REGEX, $_SERVER['REQUEST_URI'], $matches, PREG_OFFSET_CAPTURE);
        $project_id = false;
        if( isset($matches[1][0]) && intval($matches[1][0]) > 0 ){
            $project_id = intval($matches[1][0]);
        }

        // Подтягиваем категории сайта
        $sSections = DB::table('site_sections')->orderBy('position')->get();
        $siteSections = [
            [ 'value' => null, 'label' => 'Нет привязки' ]
        ];
        foreach ( $sSections as $key => $siteSection ){
            $trRU = DB::table('site_section_translations')
                ->where('site_section_id', $siteSection->id)
                ->where('active', 1)
                ->where('locale', 'ru')
                ->first();
            $siteSections[] = [
                'value' => $siteSection->id,
                'label' => !is_null($trRU)?$trRU->title:$siteSection->name
            ];
        }

        // Подтягиваем типы работ
        $pCategories = DB::table('project_categories')->orderBy('position')->get();
        $projectCategories = [
            [ 'value' => null, 'label' => 'Нет привязки' ]
        ];
        $projectCategoriesMulti = [];
        foreach ( $pCategories as $key => $pCategory ){
            $proj_trRU = DB::table('project_category_translations')
                ->where('project_category_id', $pCategory->id)
                ->where('locale', 'ru')
                ->first();
            $siteSectionName = '';
            if( intval($pCategory->section_id) > 0 ){
                $siteSection = DB::table('site_sections')->where('id', $pCategory->section_id)->first();
                if( !is_null($siteSection) ){
                    $ssect_trRU = DB::table('site_section_translations')
                        ->where('site_section_id', $siteSection->id)
                        ->where('active', 1)
                        ->where('locale', 'ru')
                        ->first();
                    if( is_null($ssect_trRU) ){
                        $siteSectionName = ' ('.$siteSection->name.')';
                    } else {
                        $siteSectionName = ' ('.$ssect_trRU->title.')';
                    }
                }
            }
            $projectCategories[] = [
                'value' => $pCategory->id,
                'label' => (!is_null($proj_trRU)?$proj_trRU->title:$pCategory->name).$siteSectionName
            ];
            $projectCategoriesMulti[] = [
                'value' => $pCategory->id,
                'label' => (!is_null($proj_trRU)?$proj_trRU->title:$pCategory->name).$siteSectionName
            ];
        }

        // Подтягиваем направления работ
        $pDirections = ProjectDirection::orderBy('position')->get();
        $projectDirections = [
            [ 'value' => null, 'label' => 'Нет привязки' ]
        ];
        foreach ( $pDirections as $key => $pDirection ){
            $transTitles = $pDirection->translatedAttribute('title');
            $projectDirections[] = [ 'value' => $pDirection->id, 'label' => $transTitles['ru'] ];
        }

        // Подтягиваем клиентов (сортировка по алфавиту - title)
        $clients = DB::table('clients')
        ->leftJoin('client_translations', 'clients.id', '=', 'client_translations.client_id')
        ->select('clients.id as id', 'client_translations.title as title' )
        ->where('client_translations.locale', 'ru' )
        ->where('client_translations.active', 1 )
        ->orderBy('client_translations.title')->get();
        $arClients = [
            [ 'value' => null, 'label' => 'Нет привязки' ]
        ];
        foreach ( $clients as $key => $client ){
            $arClients[] = [ 'value' => $client->id, 'label' => $client->title ];
        }

        // Инфо по проекту
        $video_mp4_info = null;
        $project = Project::find($project_id);
        if(
            isset($project)
            &&
            isset($project->video_mp4_link)
            &&
            strlen($project->video_mp4_link) > 0
        ){
            preg_match(
                Project::VIMEO_LINK_REGEX,
                $project->video_mp4_link,
                $matches, PREG_OFFSET_CAPTURE
            );
            if( isset( $matches[2][0] ) ){
                $vimeo_id = $matches[2][0];
                $video_mp4_info = Project::getVimeoInfo($vimeo_id);
            }
        }

        return [
            'video_mp4_info' => $video_mp4_info,
            'siteSections' => $siteSections,
            'projectCategories' => $projectCategories,
            'projectDirections' => $projectDirections,
            'projectCategoriesMulti' => $projectCategoriesMulti,
            'clients' => $arClients,
        ];
    }





}
