<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Classes\Site;


class SiteSectionController extends ModuleController {

    protected $moduleName = 'siteSections';


    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];


    protected $indexColumns = [
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'module' => [
            'title' => 'Модуль',
            'field' => 'module',
        ],
//        'position' => [
//            'title' => 'Position',
//            'field' => 'position',
//        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];



    protected function formData($request){
        $modules = [
            [ 'value' => null, 'label' => 'Нет привязки' ]
        ];
        foreach ( Site::$modules as $module => $item ){
            $modules[] = [
                'value' => $module,
                'label' => $item['title']
            ];
        }
        return [ 'modules' => $modules ];
    }



}
