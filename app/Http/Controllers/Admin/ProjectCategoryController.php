<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use function Couchbase\defaultDecoder;
use A17\Twill\Models\Model;
use Illuminate\Support\Facades\DB;


class ProjectCategoryController extends ModuleController {

    protected $moduleName = 'projectCategories';

    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => true,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];

    protected $indexColumns = [
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'published' => [
            'title' => 'Active',
            'field' => 'published',
        ],
        'slug_field' => [
            'title' => 'Слаг',
            'field' => 'slug_field',
        ],
        'section_id' => [
            'title' => 'Категория сайта',
            'field' => 'parent_uri',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];





    protected function formData($request){
        $sSections = DB::table('site_sections')->orderBy('position')->get();
        $siteSections = [
            [ 'value' => null, 'label' => 'No value' ]
        ];
        foreach ( $sSections as $key => $siteSection ){
            $trRU = DB::table('site_section_translations')
                ->where('site_section_id', $siteSection->id)
                ->where('locale', 'ru')
                ->first();
            $siteSections[] = [
                'value' => $siteSection->id,
                'label' => !is_null($trRU)?$trRU->title:$siteSection->name
            ];
        }
        return [ 'siteSections' => $siteSections ];
    }


}
