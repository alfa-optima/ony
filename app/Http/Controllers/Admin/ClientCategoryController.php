<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ClientCategoryController extends ModuleController
{
    protected $moduleName = 'clientCategories';

    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => true,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];


    protected $indexColumns = [
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'published' => [
            'title' => 'Активность',
            'field' => 'published',
        ],
        'slug_field' => [
            'title' => 'Слаг',
            'field' => 'slug_field',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];


}
