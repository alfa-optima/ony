<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use App\Models\ClientCategory;



class ClientController extends ModuleController {

    protected $moduleName = 'clients';


    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];


    protected $indexColumns = [
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];


    protected function formData($request){
        // Подтягиваем категории клиентов
        $сSections = DB::table('client_categories')->orderBy('position')->get();
        $сSectionsMulti = [];
        foreach ( $сSections as $key => $сSection ){
            $trRU = DB::table('client_category_translations')
            ->where('client_category_id', $сSection->id)
            ->where('active', 1)
            ->where('locale', 'ru')
            ->first();
            $сSectionsMulti[] = [
                'value' => $сSection->id,
                'label' => !is_null($trRU)?$trRU->title:$сSection->name
            ];
        }
        return [
            'сSectionsMulti' => $сSectionsMulti
        ];
    }









}
