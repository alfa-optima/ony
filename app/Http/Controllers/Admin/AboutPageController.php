<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Classes\Helper;
use App\Models\Client;
use Illuminate\Support\Facades\DB;


class AboutPageController extends ModuleController
{

    protected $moduleName = 'aboutPages';



    protected $indexOptions = [
        'create' => false,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => false,
        'bulkRestore' => false,
        'delete' => false,
        'bulkDelete' => false,
        'reorder' => false,
        'permalink' => false,
        'bulkEdit' => false,
        'editInModal' => false,
    ];



    protected $indexColumns = [
//        'published' => [
//            'title' => 'Active',
//            'field' => 'published',
//        ],
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];







}
