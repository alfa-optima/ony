<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class SocialController extends ModuleController
{
    protected $moduleName = 'socials';





    protected $indexOptions = [
        'create' => true,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => true,
        'bulkRestore' => true,
        'delete' => true,
        'bulkDelete' => true,
        'reorder' => false,
        'permalink' => false,
        'bulkEdit' => true,
        'editInModal' => false,
    ];




    protected $indexColumns = [
        'published' => [
            'title' => 'Active',
            'field' => 'published',
        ],
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];




}
