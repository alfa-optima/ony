<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ServicesPageController extends ModuleController
{

    protected $moduleName = 'servicesPages';



    protected $indexOptions = [
        'create' => false,
        'edit' => true,
        'publish' => false,
        'bulkPublish' => false,
        'feature' => false,
        'bulkFeature' => false,
        'restore' => false,
        'bulkRestore' => false,
        'delete' => false,
        'bulkDelete' => false,
        'reorder' => false,
        'permalink' => false,
        'bulkEdit' => false,
        'editInModal' => false,
    ];



    protected $indexColumns = [
        'title' => [
            'title' => 'Название',
            'field' => 'title',
        ],
        'id' => [
            'title' => 'ID',
            'field' => 'id',
        ],
    ];



}
