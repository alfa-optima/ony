<?php

namespace App\Http\Requests\Admin;

use A17\Twill\Http\Requests\Admin\Request;
use App\Models\Project;

class ProjectRequest extends Request {


    public function rulesForCreate(){
        return [];
    }


    public function rulesForUpdate(){

        // Получим ID проекта из $headers['Referer']
        $headers = getallheaders();
        if( isset( $headers['Referer'] ) ){

            preg_match(Project::EDIT_PAGE_REGEX, $headers['Referer'], $matches, PREG_OFFSET_CAPTURE);
            $project_id = false;

            if( isset($matches[1][0]) && intval($matches[1][0]) > 0 ){

                $project_id = intval($matches[1][0]);

                return $this->rulesForTranslatedFields(
                    // >>>NOT<<< translated fields
                    [
                        'slug_field' => 'required|unique:projects,slug_field,'.$project_id,
                    ],
                    // translated fields
                    []
                );

            } else {   return [];   }
        } else {   return [];   }
    }


    public function messages(){
        return $this->messagesForTranslatedFields(
            // >>>NOT<<< translated fields
            [
                'slug_field.required' => 'Обязательное поле',
                'slug_field.unique' => 'В другом проекте уже есть такой слаг',
            ],
            // translated fields
            []
        );
    }


}
