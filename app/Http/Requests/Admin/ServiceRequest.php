<?php

namespace App\Http\Requests\Admin;

use A17\Twill\Http\Requests\Admin\Request;
use App\Models\Service;


class ServiceRequest extends Request
{
    public function rulesForCreate()
    {
        return [];
    }

    public function rulesForUpdate(){

        // Получим ID проекта из $headers['Referer']
        $headers = getallheaders();
        if( isset( $headers['Referer'] ) ){

            preg_match(Service::EDIT_PAGE_REGEX, $headers['Referer'], $matches, PREG_OFFSET_CAPTURE);
            $service_id = false;

            if( isset($matches[1][0]) && intval($matches[1][0]) > 0 ){

                $service_id = intval($matches[1][0]);

                return $this->rulesForTranslatedFields(
                // >>>NOT<<< translated fields
                    [
                        'slug_field' => 'required|unique:services,slug_field,'.$service_id,
                    ],
                    // translated fields
                    []
                );

            } else {   return [];   }
        } else {   return [];   }
    }


    public function messages(){
        return $this->messagesForTranslatedFields(
        // >>>NOT<<< translated fields
            [
                'slug_field.required' => 'Обязательное поле',
                'slug_field.unique' => 'В другой услуге уже есть такой слаг',
            ],
            // translated fields
            []
        );
    }

}
