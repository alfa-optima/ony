<?php

namespace App\Repositories;

use A17\Twill\Helpers\FlashLevel;
use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
//use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Project;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Cocur\Slugify\Slugify;


class ProjectRepository extends ModuleRepository {

    use HandleBlocks, HandleTranslations/*, HandleSlugs*/, HandleMedias, HandleFiles;


    public function __construct(Project $model){
        $this->model = $model;
    }



    public function beforeSave($object, $fields){
        // Подставляем уникальный слаг при сохранении
        if(
            isset($fields['title']['ru'])
            &&
            strlen($fields['title']['ru']) > 0
        ){
            if(
                !isset($fields['slug_field'])
                ||
                strlen($fields['slug_field']) == 0
            ){
                $slug = Project::getUniqueSlug(
                    $fields['title']['ru'], null,
                    isset($object->id)?$object->id:null
                );
            } else {
                $slug = Project::getUniqueSlug(
                    null, $fields['slug_field'],
                    isset($object->id)?$object->id:null
                );
            }
            if( isset($slug) ){
                $fields['slug_field'] = $slug;
                $object->slug_field = $slug;
            }
        }
        parent::beforeSave($object, $fields);
    }



    public function afterSave($object, $fields){
        $object->project_categories()->sync($fields['project_categories'] ?? []);
        $this->updateBrowser( $object, $fields, 'about_awards' );
        parent::afterSave($object, $fields);
    }



    public function getFormFields($object){
        $fields = parent::getFormFields($object);
        $fields['browsers']['about_awards'] = $this->getFormFieldsForBrowser($object, 'about_awards');
        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "about_award_project"
        $sortedItems = DB::table('about_award_project')
            ->where('project_id', $fields['id'])
            ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['about_awards'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['about_awards'])
            &&
            is_array($fields['browsers']['about_awards'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['about_awards'] as $k => $about_award ){
                    if( $sortedItem->about_award_id == $about_award['id'] ){
                        $newItems[] = $about_award;
                    }
                }
            }
            $fields['browsers']['about_awards'] = $newItems;
        }
        return $fields;
    }



}
