<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Client;

class ClientRepository extends ModuleRepository
{
    use HandleTranslations, HandleMedias, HandleFiles;

    public function __construct(Client $model)
    {
        $this->model = $model;
    }


    public function afterSave($object, $fields){
        $object->client_categories()->sync($fields['client_categories'] ?? []);
        parent::afterSave($object, $fields);
    }



}
