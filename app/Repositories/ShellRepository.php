<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Shell;
use Illuminate\Support\Facades\DB;

class ShellRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleMedias, HandleFiles;

    public function __construct(Shell $model)
    {
        $this->model = $model;
    }



    public function afterSave($object, $fields){
        $this->updateBrowser($object, $fields, 'socials');
        parent::afterSave($object, $fields);
    }




    public function getFormFields($object){

        $fields = parent::getFormFields($object);

        $fields['browsers']['socials'] = $this->getFormFieldsForBrowser($object, 'socials');

        ////////
        $sortedItems = DB::table('shell_social')
            ->where('shell_id', $fields['id'])
            ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['socials'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['socials'])
            &&
            is_array($fields['browsers']['socials'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['socials'] as $k => $item ){
                    if( $sortedItem->social_id == $item['id'] ){     $newItems[] = $item;     }
                }
            }
            $fields['browsers']['socials'] = $newItems;
        }

        return $fields;
    }



}
