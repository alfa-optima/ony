<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Project;
use App\Models\ProjectList;
use Illuminate\Support\Facades\DB;


class ProjectListRepository extends ModuleRepository
{
    use HandleTranslations, HandleSlugs, HandleMedias, HandleFiles;

    public function __construct(ProjectList $model)
    {
        $this->model = $model;
    }


    public function beforeSave($object, $fields){
        // Подставляем уникальный слаг при сохранении
        if(
            isset($fields['title']['ru'])
            &&
            strlen($fields['title']['ru']) > 0
        ){
            if(
                !isset($fields['slug_field'])
                ||
                strlen($fields['slug_field']) == 0
            ){
                $slug = ProjectList::getUniqueSlug(
                    $fields['title']['ru'], null,
                    isset($object->id)?$object->id:null
                );
            } else {
                $slug = ProjectList::getUniqueSlug(
                    null, $fields['slug_field'],
                    isset($object->id)?$object->id:null
                );
            }
            if( isset($slug) ){
                $fields['slug_field'] = $slug;
                $object->slug_field = $slug;
            }
        }
        parent::beforeSave($object, $fields);
    }


    public function afterSave($object, $fields){
        $this->updateBrowser($object, $fields, 'projects');
        parent::afterSave($object, $fields);
    }


    public function getFormFields($object){


        $fields = parent::getFormFields($object);
        $fields['browsers']['projects'] = $this->getFormFieldsForBrowser($object, 'projects');



        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "project_project_list"
        $sortedItems = DB::table('project_project_list')
            ->where('project_list_id', $fields['id'])
            ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['projects'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['projects'])
            &&
            is_array($fields['browsers']['projects'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['projects'] as $k => $item ){
                    if( $sortedItem->project_id == $item['id'] ){
                        $newItems[] = $item;
                    }
                }
            }
            $fields['browsers']['projects'] = $newItems;
        }
        return $fields;
    }


}
