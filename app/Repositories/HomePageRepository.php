<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\HomePage;
use Illuminate\Support\Facades\DB;

class HomePageRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles;

    public function __construct(HomePage $model)
    {
        $this->model = $model;
    }



    public function afterSave($object, $fields){
        //$this->updateBrowser($object, $fields, 'jobs');
        $this->updateBrowser($object, $fields, 'clients');
        $this->updateBrowser($object, $fields, 'aboutAwards');
        //$this->updateBrowser($object, $fields, 'projects');
        parent::afterSave($object, $fields);
    }



    public function getFormFields($object){

        $fields = parent::getFormFields($object);

        $fields['browsers']['clients'] = $this->getFormFieldsForBrowser($object, 'clients');
        $fields['browsers']['aboutAwards'] = $this->getFormFieldsForBrowser($object, 'aboutAwards');
        //$fields['browsers']['projects'] = $this->getFormFieldsForBrowser($object, 'projects');

        ////////
        $sortedItems = DB::table('client_home_page')
            ->where('home_page_id', $fields['id'])
            ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['clients'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['clients'])
            &&
            is_array($fields['browsers']['clients'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['clients'] as $k => $item ){
                    if( $sortedItem->client_id == $item['id'] ){     $newItems[] = $item;     }
                }
            }
            $fields['browsers']['clients'] = $newItems;
        }

        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "about_award_home_page"
        $sortedItems = DB::table('about_award_home_page')
        ->where('home_page_id', $fields['id'])
        ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['aboutAwards'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['aboutAwards'])
            &&
            is_array($fields['browsers']['aboutAwards'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['aboutAwards'] as $k => $item ){
                    if( $sortedItem->about_award_id == $item['id'] ){
                        $newItems[] = $item;
                    }
                }
            }
            $fields['browsers']['aboutAwards'] = $newItems;
        }

        ////////
//        $sortedItems = DB::table('home_page_project')
//            ->where('home_page_id', $fields['id'])
//            ->orderBy('position')->get();
//        // Сформируем новый массив $fields['browsers']['projects'], уже отсортированный в нужном порядке
//        if(
//            isset($fields['browsers']['projects'])
//            &&
//            is_array($fields['browsers']['projects'])
//            &&
//            count($sortedItems) > 0
//        ){
//            $newItems = [];
//            foreach ( $sortedItems as $sk => $sortedItem ){
//                foreach ( $fields['browsers']['projects'] as $k => $item ){
//                    if( $sortedItem->project_id == $item['id'] ){     $newItems[] = $item;     }
//                }
//            }
//            $fields['browsers']['projects'] = $newItems;
//        }

        //dd($fields['browsers']);

        return $fields;
    }



}
