<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\ServicesPage;
use Illuminate\Support\Facades\DB;


class ServicesPageRepository extends ModuleRepository
{

    use HandleBlocks, HandleTranslations, HandleMedias, HandleFiles;

    public function __construct(ServicesPage $model)
    {

        $this->model = $model;




    }




    public function afterSave( $object, $fields ){
        $this->updateBrowser( $object, $fields, 'services' );
        parent::afterSave( $object, $fields );
    }

    public function getFormFields($object){
        $fields = parent::getFormFields($object);
        $fields['browsers']['services'] = $this->getFormFieldsForBrowser($object, 'services');
        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "services_page_service"
        $sortedItems = DB::table('service_services_page')
        ->where('services_page_id', $fields['id'])
        ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['services'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['services'])
            &&
            is_array($fields['browsers']['services'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['services'] as $k => $item ){
                    if( $sortedItem->service_id == $item['id'] ){
                        $newItems[] = $item;
                    }
                }
            }
            $fields['browsers']['services'] = $newItems;
        }
        return $fields;
    }




}
