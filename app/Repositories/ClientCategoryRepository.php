<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\ClientCategory;


class ClientCategoryRepository extends ModuleRepository
{
    use HandleTranslations, HandleMedias, HandleSlugs, HandleFiles;

    public function __construct(ClientCategory $model)
    {
        $this->model = $model;
    }


    public function beforeSave($object, $fields){
        // Подставляем уникальный слаг при сохранении
        if(
            isset($fields['title']['ru'])
            &&
            strlen($fields['title']['ru']) > 0
        ){
            if(
                !isset($fields['slug_field'])
                ||
                strlen($fields['slug_field']) == 0
            ){
                $slug = ClientCategory::getUniqueSlug(
                    $fields['title']['ru'], null,
                    isset($object->id)?$object->id:null
                );
            } else {
                $slug = ClientCategory::getUniqueSlug(
                    null, $fields['slug_field'],
                    isset($object->id)?$object->id:null
                );
            }
            if( isset($slug) ){
                $fields['slug_field'] = $slug;
                $object->slug_field = $slug;
            }
        }
        parent::beforeSave($object, $fields);
    }


}
