<?php

namespace App\Repositories;

use App\Classes\Helper;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRepeaters;
use A17\Twill\Repositories\ModuleRepository;
use A17\Twill\Repositories\Behaviors\HandleBlocks;
use App\Models\AboutPage;
use Illuminate\Support\Facades\DB;

class AboutPageRepository extends ModuleRepository
{
    use HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleBlocks, HandleRepeaters;



    public function __construct(AboutPage $model){
        $this->model = $model;
    }



    public function prepareFieldsBeforeSave($object, $fields) {
        if ( isset($fields['repeaters']['services']) ){
            $fields['services_items_json'] = $fields['repeaters']['services'];
        }
        return parent::prepareFieldsBeforeSave( $object, $fields );
    }



    public function afterSave($object, $fields){
        $this->updateBrowser($object, $fields, 'aboutDirections');
        $this->updateBrowser($object, $fields, 'aboutAwards');
        $this->updateBrowser($object, $fields, 'aboutClients');
        $this->updateBrowser($object, $fields, 'facts');
        $this->updateBrowser($object, $fields, 'jobs');
        parent::afterSave($object, $fields);
    }



    public function getFormFields($object){

        $fields = parent::getFormFields($object);

        if ( isset($fields['services_items_json']) && !empty($fields['services_items_json']) ){
            $fields = Helper::getJsonRepeater($fields, 'services', 'services_items_json');
        }

        $fields['browsers']['jobs'] = $this->getFormFieldsForBrowser($object, 'jobs');
        $fields['browsers']['aboutDirections'] = $this->getFormFieldsForBrowser($object, 'aboutDirections');
        $fields['browsers']['aboutAwards'] = $this->getFormFieldsForBrowser($object, 'aboutAwards');
        $fields['browsers']['aboutClients'] = $this->getFormFieldsForBrowser($object, 'aboutClients');
        $fields['browsers']['facts'] = $this->getFormFieldsForBrowser($object, 'facts');

        ////////
        $sortedItems = DB::table('about_page_job')
        ->where('about_page_id', $fields['id'])
        ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['jobs'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['jobs'])
            &&
            is_array($fields['browsers']['jobs'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['jobs'] as $k => $item ){
                    if( $sortedItem->job_id == $item['id'] ){     $newItems[] = $item;     }
                }
            }
            $fields['browsers']['jobs'] = $newItems;
        }

        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "about_direction_about_page"
        $sortedItems = DB::table('about_direction_about_page')
        ->where('about_page_id', $fields['id'])
        ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['aboutDirections'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['aboutDirections'])
            &&
            is_array($fields['browsers']['aboutDirections'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['aboutDirections'] as $k => $item ){
                    if( $sortedItem->about_direction_id == $item['id'] ){
                        $newItems[] = $item;
                    }
                }
            }
            $fields['browsers']['aboutDirections'] = $newItems;
        }

        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "about_award_about_page"
        $sortedItems = DB::table('about_award_about_page')
            ->where('about_page_id', $fields['id'])
            ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['aboutAwards'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['aboutAwards'])
            &&
            is_array($fields['browsers']['aboutAwards'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['aboutAwards'] as $k => $item ){
                    if( $sortedItem->about_award_id == $item['id'] ){
                        $newItems[] = $item;
                    }
                }
            }
            $fields['browsers']['aboutAwards'] = $newItems;
        }

        // Расставим элементы по сортировке (по умолчанию порядок по position не устанавливается)
        // Получим из базы отсортированный порядок из таблицы "client_about_page"
        $sortedItems = DB::table('about_page_client')
            ->where('about_page_id', $fields['id'])
            ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['aboutClients'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['aboutClients'])
            &&
            is_array($fields['browsers']['aboutClients'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['aboutClients'] as $k => $item ){
                    if( $sortedItem->client_id == $item['id'] ){
                        $newItems[] = $item;
                    }
                }
            }
            $fields['browsers']['aboutClients'] = $newItems;
        }


        ////////
        $sortedItems = DB::table('about_page_fact')
        ->where('about_page_id', $fields['id'])
        ->orderBy('position')->get();
        // Сформируем новый массив $fields['browsers']['facts'], уже отсортированный в нужном порядке
        if(
            isset($fields['browsers']['facts'])
            &&
            is_array($fields['browsers']['facts'])
            &&
            count($sortedItems) > 0
        ){
            $newItems = [];
            foreach ( $sortedItems as $sk => $sortedItem ){
                foreach ( $fields['browsers']['facts'] as $k => $item ){
                    if( $sortedItem->fact_id == $item['id'] ){     $newItems[] = $item;     }
                }
            }
            $fields['browsers']['facts'] = $newItems;
        }


        return $fields;
    }



}
