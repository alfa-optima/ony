<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Job;

class JobRepository extends ModuleRepository
{
    use HandleTranslations, HandleMedias, HandleFiles;

    public function __construct(Job $model)
    {
        $this->model = $model;
    }


}
