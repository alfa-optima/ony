<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Social;
use Illuminate\Support\Facades\DB;

class SocialRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleMedias, HandleFiles;

    public function __construct(Social $model)
    {
        $this->model = $model;
    }







}
