<?php

namespace App\Repositories;

use App\Classes\Helper;
use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Service;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\DB;


class ServiceRepository extends ModuleRepository {

    use HandleBlocks, HandleTranslations, HandleMedias, HandleFiles;


    public function __construct(Service $model){
        $this->model = $model;
    }



    public function prepareFieldsBeforeSave($object, $fields) {
        if ( isset($fields['repeaters']['service_items']) ){
            $fields['items_json'] = $fields['repeaters']['service_items'];
        }
        return parent::prepareFieldsBeforeSave( $object, $fields );
    }



    public function beforeSave($object, $fields){
        // Подставляем уникальный слаг при сохранении
        if(
            isset($fields['title']['ru'])
            &&
            strlen($fields['title']['ru']) > 0
        ){
            if(
                !isset($fields['slug_field'])
                ||
                strlen($fields['slug_field']) == 0
            ){
                $slug = Service::getUniqueSlug(
                    $fields['title']['ru'], null,
                    isset($object->id)?$object->id:null
                );
            } else {
                $slug = Service::getUniqueSlug(
                    null, $fields['slug_field'],
                    isset($object->id)?$object->id:null
                );
            }
            if( isset($slug) ){
                $fields['slug_field'] = $slug;
                $object->slug_field = $slug;
            }
        }
        parent::beforeSave($object, $fields);
    }



    public function getFormFields($object){
        $fields = parent::getFormFields($object);
        if ( isset($fields['items_json']) && !empty($fields['items_json']) ){
            $fields = Helper::getJsonRepeater($fields, 'service_items', 'items_json');
        }
        return $fields;
    }



}
