<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\ModuleRepository;
use A17\Twill\Repositories\Behaviors\HandleBlocks;
use App\Models\AboutDirection;


class AboutDirectionRepository extends ModuleRepository
{
    use HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleBlocks;

    public function __construct(AboutDirection $model)
    {
        $this->model = $model;
    }


    public function beforeSave($object, $fields){
        // Подставляем уникальный слаг при сохранении
        if(
            isset($fields['title']['ru'])
            &&
            strlen($fields['title']['ru']) > 0
        ){
            if(
                !isset($fields['slug_field'])
                ||
                strlen($fields['slug_field']) == 0
            ){
                $slug = AboutDirection::getUniqueSlug(
                    $fields['title']['ru'], null,
                    isset($object->id)?$object->id:null
                );
            } else {
                $slug = AboutDirection::getUniqueSlug(
                    null, $fields['slug_field'],
                    isset($object->id)?$object->id:null
                );
            }
            if( isset($slug) ){
                $fields['slug_field'] = $slug;
                $object->slug_field = $slug;
            }
        }
        parent::beforeSave($object, $fields);
    }


}
