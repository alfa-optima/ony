

@formField('wysiwyg', [
    'name' => 'text',
    'label' => 'Текст',
    'placeholder' => 'Текст',
    'translated' => true,
    'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6]],
        'bold',
        'italic',
        'underline',
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        'link',
    ],
    'editSource' => true,
])

@formField('input', [
    'name' => 'link_title',
    'label' => 'Название ссылки',
    'maxlength' => 250,
    'translated' => true,
])

@formField('input', [
    'name' => 'link',
    'label' => 'Ссылка',
    'maxlength' => 250
])
