

@formField('input', [
    'name' => 'title',
    'label' => 'Заголовок текста',
    'maxlength' => 255,
    'translated' => true,
])


@formField('wysiwyg', [
    'name' => 'text',
    'label' => 'Текст',
    'placeholder' => 'Text',
    'translated' => true,
    'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6]],
        'bold',
        'italic',
        'underline',
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        'link',
    ],
    'editSource' => true,
])


