
@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
    'required' => true,
])


@formField('wysiwyg', [
    'name' => 'text',
    'label' => 'Text',
    'placeholder' => 'Text',
    'translated' => true,
    'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6]],
        'bold',
        'italic',
        'underline',
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        'link',
    ],
    'editSource' => true,
])
