

@formField('checkbox', [
    'name' => 'active',
    'label' => 'Активность',
    'value' => 1
])


@formField('select', [
    'name' => 'gallery_style',
    'label' => 'Стиль (тёмный/светлый)',
    'options' => [
        [
            'value' => 'black',
            'label' => 'Тёмный'
        ],
        [
            'value' => 'white',
            'label' => 'Светлый'
        ],
    ]
])


@formField('medias', [
    'name' => 'project_block_images',
    'label' => 'Images',
    'max' => 100,
    'fieldNote' => 'Images'
])

@formField('checkbox', [
    'name' => 'use_slider',
    'label' => 'Это слайдер',
    'value' => 1
])


@formField('wysiwyg', [
    'name' => 'text',
    'label' => 'Text',
    'placeholder' => 'Text',
    'translated' => true,
    'toolbarOptions' => [
        ['header' => [2, 3, 4, 5, 6]],
        'bold',
        'italic',
        'underline',
        ['list' => 'ordered'],
        ['list' => 'bullet'],
        'link',
    ],
    'editSource' => true,
])


@formField('files', [
    'name' => 'project_block_video_mp4',
    'label' => 'Видео MP4',
    'required' => true,
    'note' => 'Рекомендуемый размер видео 1920х1080 c битрейтом 10Mbps',
])


@formField('input', [
    'name' => 'project_block_video_mp4_link',
    'label' => 'Видео (ссылка на Vimeo)',
    'maxlength' => 255
])


@formField('checkbox', [
    'name' => 'use_vimeo_player',
    'label' => 'Использовать плеер Vimeo',
    'value' => 1
])

