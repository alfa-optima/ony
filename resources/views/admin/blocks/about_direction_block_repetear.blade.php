
@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
    'required' => true,
])

@formField('input', [
    'name' => 'block_id',
    'label' => 'Block ID',
    'required' => true,
])
