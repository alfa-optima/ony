@extends('twill::layouts.form')

@section('contentFields')

    @formField('input', [
        'name' => 'slug_field',
        'label' => 'Слаг',
        'required' => true,
        'maxlength' => 255
    ])

    @formField('browser', [
        'moduleName' => 'projects',
        'name' => 'projects',
        'label' => 'Проекты',
        'note' => 'Прикрепите необходимые проекты',
        'max' => 1000,
        'required' => true
    ])

@stop
