@extends('twill::layouts.form')

@section('contentFields')


    @formField('medias', [
        'name' => 'header_image',
        'label' => 'Header (картинка)',
        'required' => true,
    ])


    @formField('input', [
        'name' => 'header_vimeo_video_link',
        'label' => 'Header (ссылка на видео Vimeo)',
        'maxlength' => 100
    ])


    <hr>


    @formField('medias', [
        'name' => 'showreel_image',
        'label' => 'Showreel (картинка)',
        'required' => true,
    ])


    @formField('input', [
        'name' => 'showreel_vimeo_video_link',
        'label' => 'Showreel (ссылка на видео Vimeo)',
        'maxlength' => 100
    ])

    <hr>

    @formField('wysiwyg', [
        'name' => 'lead_text',
        'label' => 'Lead text',
        'placeholder' => 'Lead text',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])

{{--    @formField('wysiwyg', [--}}
{{--        'name' => 'contacts',--}}
{{--        'label' => 'Contacts',--}}
{{--        'placeholder' => 'Contacts',--}}
{{--        'translated' => true,--}}
{{--        'toolbarOptions' => [--}}
{{--            ['header' => [2, 3, 4, 5, 6]],--}}
{{--            'bold',--}}
{{--            'italic',--}}
{{--            'underline',--}}
{{--            ['list' => 'ordered'],--}}
{{--            ['list' => 'bullet'],--}}
{{--            'link',--}}
{{--        ],--}}
{{--        'editSource' => true,--}}
{{--    ])--}}

    <hr>

    @formField('input', [
        'name' => 'about_block_title',
        'label' => 'Заголовок блока About',
        'translated' => true,
        'maxlength' => 100
    ])

    @formField('wysiwyg', [
        'name' => 'about_block_text',
        'label' => 'Описание блока About',
        'placeholder' => 'Описание блока About',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])

    <hr>

    <h3>Блок направлений</h3>

    @formField('input', [
        'name' => 'departments_block_title',
        'label' => 'Заголовок блока направлений',
        'translated' => true,
        'maxlength' => 100
    ])

    @formField('browser', [
        'moduleName' => 'aboutDirections',
        'name' => 'aboutDirections',
        'label' => 'Направления',
        'note' => 'Прикрепите необходимые направления',
        'max' => 1000,
        'required' => true
    ])

    <hr>

    @formField('input', [
        'name' => 'client_block_title',
        'label' => 'Заголовок блока с клиентами',
        'translated' => true,
        'maxlength' => 100
    ])

{{--    @formField('wysiwyg', [--}}
{{--        'name' => 'client_block_text',--}}
{{--        'label' => 'Описание к блока с клиентами',--}}
{{--        'placeholder' => 'Описание к блока с клиентами',--}}
{{--        'translated' => true,--}}
{{--        'toolbarOptions' => [--}}
{{--            ['header' => [2, 3, 4, 5, 6]],--}}
{{--            'bold',--}}
{{--            'italic',--}}
{{--            'underline',--}}
{{--            ['list' => 'ordered'],--}}
{{--            ['list' => 'bullet'],--}}
{{--            'link',--}}
{{--        ],--}}
{{--        'editSource' => true,--}}
{{--    ])--}}

    @formField('browser', [
        'moduleName' => 'clients',
        'name' => 'aboutClients',
        'label' => 'Клиенты',
        'note' => 'Прикрепите необходимых клиентов',
        'max' => 1000,
        'required' => true
    ])


    <hr>

    <h3>Блок наград</h3>

    @formField('input', [
        'name' => 'awards_block_title',
        'label' => 'Заголовок блока наград',
        'translated' => true,
        'maxlength' => 100
    ])


    @formField('wysiwyg', [
        'name' => 'awards_block_text',
        'label' => 'Описание к блокам наград',
        'placeholder' => 'Описание к блокам наград',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    @formField('browser', [
        'moduleName' => 'aboutAwards',
        'name' => 'aboutAwards',
        'label' => 'Награды',
        'note' => 'Прикрепите необходимые награды',
        'max' => 1000,
        'required' => true
    ])


    <hr>


    @formField('input', [
        'name' => 'services_title',
        'label' => 'Услуги (заголовок)',
        'translated' => true,
        'maxlength' => 100
    ])

    @formField('wysiwyg', [
        'name' => 'services_text',
        'label' => 'Услуги (описание)',
        'placeholder' => 'Services text',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    <hr>


    <h2 style="text-align: center;">Блок фактов</h2>

    @formField('input', [
        'name' => 'facts_block_title',
        'label' => 'Заголовок для блока фактов',
        'translated' => true,
        'maxlength' => 200
    ])

    @formField('browser', [
        'moduleName' => 'facts',
        'name' => 'facts',
        'label' => 'Факты',
        'note' => 'Прикрепите необходимые факты',
        'max' => 1000,
        'required' => true
    ])


    <hr>


    <h2 style="text-align: center;">Блок вакансий</h2>

    @formField('input', [
        'name' => 'jobs_block_title',
        'label' => 'Заголовок для блока вакансий',
        'translated' => true,
        'maxlength' => 200
    ])

    @formField('browser', [
        'moduleName' => 'jobs',
        'name' => 'jobs',
        'label' => 'Вакансии',
        'note' => 'Прикрепите необходимые вакансии',
        'max' => 1000,
        'required' => true
    ])


    <h3>Перечень услуг</h3>
    @formField('repeater', ['type' => 'services'])



@stop



