@extends('twill::layouts.form')

@section('contentFields')

{{--    @formField('checkbox', [--}}
{{--        'name' => 'show_admin_only',--}}
{{--        'label' => 'Показывать только админу',--}}
{{--        'value' => 1--}}
{{--    ])--}}

    @formField('checkbox', [
        'name' => 'add_to_upload_list',
        'label' => 'Добавить к загрузке на Vimeo',
        'value' => 1
    ])

    @formField('input', [
        'name' => 'slug_field',
        'label' => 'Слаг',
        'maxlength' => 255,
        'required' => true,
    ])

    @formField('date_picker', [
        'name' => 'date',
        'label' => 'Дата создания'
    ])

    @formField('select', [
        'name' => 'section_id',
        'label' => 'Категория сайта',
        'placeholder' => 'Можно указать категорию сайта',
        'options' => $siteSections
    ])

    @formField('multi_select', [
        'name' => 'project_categories',
        'label' => 'Типы работ',
        'options' => $projectCategoriesMulti
    ])

    @formField('select', [
        'name' => 'project_direction_id',
        'label' => 'Направление работы',
        'placeholder' => 'Направление работы',
        'options' => $projectDirections
    ])

    @formField('select', [
        'name' => 'client_id',
        'label' => 'Клиент',
        'placeholder' => 'Клиент',
        'options' => $clients
    ])

    @formField('browser', [
        'moduleName' => 'aboutAwards',
        'name' => 'about_awards',
        'label' => 'Награды',
        'max' => 1000,
    ])

    @formField('medias', [
        'name' => 'poster',
        'label' => 'Постер',
        'required' => true,
    ])

    @formField('medias', [
        'name' => 'poster_mobile',
        'label' => 'Постер (мобильная версия)',
        'required' => true,
    ])

    @formField('medias', [
        'name' => 'block_image',
        'label' => 'Изображение для блока представления',
        'required' => true,
    ])

    @formField('medias', [
        'name' => 'list_image',
        'label' => 'Картинка для списка',
        'required' => true,
    ])

    <?php if(
        isset( $video_mp4_info )
        &&
        count($video_mp4_info) > 0
    ){ ?>
        <hr style="margin-top: 20px;">
    <? } ?>

    @formField('files', [
        'name' => 'video_mp4',
        'label' => 'Видео в шапке Mp4',
        'note' => 'Рекомендуемый размер видео 1920х1080 c битрейтом 10Mbps',
    ])

    @formField('input', [
        'name' => 'video_mp4_link',
        'label' => 'Видео в шапке Mp4 (ссылка на Vimeo)',
        'maxlength' => 255
    ])

    <?php if(
        isset( $video_mp4_info )
        &&
        count($video_mp4_info) > 0
    ){
        $status = false;
        if(
            isset( $video_mp4_info['status'] )
            &&
            (
                $video_mp4_info['status'] != 'complete'
                ||
                !isset( $video_mp4_info['files'] )
            )
        ){   $status = 'в обработке';   } ?>
        <p style="color:blue;"><b>Видео загружено на Vimeo:</b> <a href="<?=$video_mp4_info['link']?>" target="_blank"><?=$video_mp4_info['link']?></a><?php
            if( $status ){ ?> <b>(Статус: "<?php echo $status; ?>")</b><?php }
        ?></p>
        <hr style="margin-top: 20px;">
    <? } ?>

    @formField('input', [
        'name' => 'site_link',
        'label' => 'Site link',
        'maxlength' => 200
    ])

{{--    @formField('input', [--}}
{{--        'name' => 'header',--}}
{{--        'label' => 'Header',--}}
{{--        'maxlength' => 100,--}}
{{--        'required' => true,--}}
{{--        'translated' => true,--}}
{{--    ])--}}

    @formField('wysiwyg', [
        'name' => 'header',
        'label' => 'Header',
        'placeholder' => 'Header',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])

    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'maxlength' => 10000,
        'type' => 'textarea',
        'translated' => true
    ])

{{--    @formField('wysiwyg', [--}}
{{--        'name' => 'description',--}}
{{--        'label' => 'Description',--}}
{{--        'placeholder' => 'Description',--}}
{{--        'translated' => true,--}}
{{--        'toolbarOptions' => [--}}
{{--            ['header' => [2, 3, 4, 5, 6]],--}}
{{--            'bold',--}}
{{--            'italic',--}}
{{--            'underline',--}}
{{--            ['list' => 'ordered'],--}}
{{--            ['list' => 'bullet'],--}}
{{--            'link',--}}
{{--        ],--}}
{{--        'editSource' => true,--}}
{{--    ])--}}

    @formField('wysiwyg', [
        'name' => 'long_description',
        'label' => 'Long description',
        'placeholder' => 'Long description',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    <h2 style="text-align: center;">Данные для авторизации</h2>

    @formField('input', [
        'name' => 'login',
        'label' => 'Login',
        'maxlength' => 200
    ])

    @formField('input', [
        'name' => 'password',
        'label' => 'Password',
        'maxlength' => 200
    ])


    <h2 style="text-align: center;">Блоки</h2>


    @formField('block_editor', [
        'blocks' => [
            'project_block',
        ],
    ])





@stop
