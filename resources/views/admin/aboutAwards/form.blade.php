@extends('twill::layouts.form')

@section('contentFields')

    @formField('input', [
        'name' => 'title_without_year',
        'label' => 'Заголовок (без года)',
        'maxlength' => 250,
        'translated' => true,
    ])

    @formField('input', [
        'name' => 'year',
        'label' => 'Год',
        'maxlength' => 250
    ])

    @formField('input', [
        'name' => 'count',
        'label' => 'Количество',
        'maxlength' => 250,
    ])

    @formField('medias', [
        'name' => 'image',
        'label' => 'Логотип',
        'note' => 'Рекомендуемый размер 360x360px',
    ])

    @formField('medias', [
        'name' => 'image_dark',
        'label' => 'Логотип (для тёмного фона)',
        'note' => 'Рекомендуемый размер 360x360px',
    ])

@stop
