@extends('twill::layouts.form')

@section('contentFields')

    <h2>SEO</h2>

    @formField('input', [
        'label' => 'title',
        'name' => 'seo_title',
        'translated' => true,
    ])

    @formField('input', [
        'label' => 'description',
        'name' => 'seo_description',
        'translated' => true,
    ])

    @formField('medias', [
        'name' => 'seo_ogImage',
        'label' => 'ogImage',
    ])

    @formField('input', [
        'label' => 'ogDescription',
        'name' => 'seo_ogDescription',
        'translated' => true,
    ])


    <h2>FOOTER</h2>

    <h2>Адрес</h2>

    @formField('input', [
        'label' => 'Заголовок адреса',
        'name' => 'footer_address_title',
        'translated' => true,
    ])

    @formField('wysiwyg', [
        'name' => 'footer_address_content',
        'label' => 'Адрес',
        'placeholder' => 'Адрес',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])

    <h2>New business contacts</h2>

    @formField('input', [
        'label' => 'Заголовок',
        'name' => 'footer_newBusinessContacts_title',
        'translated' => true,
    ])

    @formField('wysiwyg', [
        'name' => 'footer_newBusinessContacts_content',
        'label' => 'Контент',
        'placeholder' => 'Контент',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])

    <h2>Other contacts</h2>

    @formField('input', [
        'label' => 'Заголовок',
        'name' => 'footer_otherContacts_title',
        'translated' => true,
    ])

    @formField('wysiwyg', [
        'name' => 'footer_otherContacts_content',
        'label' => 'Контент',
        'placeholder' => 'Контент',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    @formField('browser', [
        'moduleName' => 'socials',
        'name' => 'socials',
        'label' => 'Соц. сети',
        'note' => 'Прикрепите необходимые соц. сети',
        'max' => 1000,
        'required' => true
    ])


    <h2>API</h2>

    @formField('input', [
        'label' => 'Секретный ключ для API',
        'name' => 'api_secret_key'
    ])


@stop
