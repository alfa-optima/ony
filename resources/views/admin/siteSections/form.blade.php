@extends('twill::layouts.form')

@section('contentFields')


    @formField('input', [
        'name' => 'slug',
        'label' => 'Символьный код для URL',
        'required' => true,
        'maxlength' => 250
    ])


    @formField('checkbox', [
        'name' => 'show_in_menu',
        'label' => 'Отображать в меню',
        'value' => 1
    ])


    @formField('select', [
        'name' => 'module',
        'label' => 'Модуль',
        'placeholder' => 'Можно указать модуль',
        'options' => $modules
    ])


    @formField('input', [
        'name' => 'uri',
        'label' => 'Символьный код для URL',
        'translated' => false,
        'maxlength' => 100
    ])


    @formField('input', [
        'name' => 'meta_kw',
        'label' => 'Meta keywords',
        'translated' => false,
        'maxlength' => 100
    ])


    @formField('input', [
        'name' => 'meta_desc',
        'label' => 'Meta description',
        'translated' => false,
        'maxlength' => 100
    ])


@stop
