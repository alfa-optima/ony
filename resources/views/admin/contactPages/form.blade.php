@extends('twill::layouts.form')

@section('contentFields')


    @formField('medias', [
        'name' => 'contacts_image',
        'label' => 'Картинка',
    ])


    @formField('block_editor', [
        'blocks' => [
            'contacts_text_block',
        ],
    ])


@stop
