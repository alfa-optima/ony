@extends('twill::layouts.form')

@section('contentFields')


    @formField('wysiwyg', [
        'name' => 'lead_text',
        'label' => 'Lead text',
        'placeholder' => 'Lead text',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


{{--    @formField('wysiwyg', [--}}
{{--        'name' => 'contacts',--}}
{{--        'label' => 'Contacts',--}}
{{--        'placeholder' => 'Contacts',--}}
{{--        'translated' => true,--}}
{{--        'toolbarOptions' => [--}}
{{--            ['header' => [2, 3, 4, 5, 6]],--}}
{{--            'bold',--}}
{{--            'italic',--}}
{{--            'underline',--}}
{{--            ['list' => 'ordered'],--}}
{{--            ['list' => 'bullet'],--}}
{{--            'link',--}}
{{--        ],--}}
{{--        'editSource' => true,--}}
{{--    ])--}}


    <hr>


    <h2 style="text-align: center;">Блок клиентов</h2>

    @formField('input', [
        'name' => 'clients_block_title',
        'label' => 'Заголовок для блока клиентов',
        'translated' => true,
        'maxlength' => 200
    ])

    @formField('browser', [
        'moduleName' => 'clients',
        'name' => 'clients',
        'label' => 'Клиенты',
        'note' => 'Прикрепите необходимых клиентов',
        'max' => 1000,
        'required' => true
    ])


    <hr>


    @formField('input', [
        'name' => 'awards_block_title',
        'label' => 'Заголовок блока наград',
        'translated' => true,
        'maxlength' => 100
    ])


    @formField('wysiwyg', [
        'name' => 'awards_block_text',
        'label' => 'Описание к блокам наград',
        'placeholder' => 'Описание к блокам наград',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    @formField('browser', [
        'moduleName' => 'aboutAwards',
        'name' => 'aboutAwards',
        'label' => 'Награды',
        'note' => 'Прикрепите необходимые награды',
        'max' => 1000,
        'required' => true
    ])


    <hr>


    <h2 style="text-align: center;">Блок About</h2>

    @formField('input', [
        'name' => 'about_block_title',
        'label' => 'Заголовок для блока About',
        'translated' => true,
        'maxlength' => 200
    ])

    @formField('wysiwyg', [
        'name' => 'about_block_text',
        'label' => 'Описание для блока About',
        'placeholder' => 'Описание для блока About',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])

    @formField('medias', [
        'name' => 'about_image',
        'label' => 'Постер для видео',
        'required' => true,
    ])

    @formField('files', [
        'name' => 'about_video',
        'label' => 'Видео',
        'translated' => false,
    ])



    @formField('block_editor', [
        'blocks' => [
            'home_text_block',
            'home_project_block',
        ],
    ])



@stop
