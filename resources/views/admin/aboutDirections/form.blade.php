@extends('twill::layouts.form')

@section('contentFields')


    @formField('input', [
        'name' => 'slug_field',
        'label' => 'Слаг',
        'required' => true,
        'maxlength' => 255
    ])

    @formField('block_editor', [
        'blocks' => [
            'about_direction_block',
        ],
    ])


@stop
