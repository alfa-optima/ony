@extends('twill::layouts.form')

@section('contentFields')

    @formField('input', [
        'name' => 'email',
        'label' => 'Email',
        'maxlength' => 200
    ])

@stop
