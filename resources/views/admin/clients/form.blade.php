@extends('twill::layouts.form')

@section('contentFields')


{{--    @formField('select', [--}}
{{--        'name' => 'client_category_id',--}}
{{--        'label' => 'Категория клиентов',--}}
{{--        'placeholder' => 'Можно указать категорию клиента',--}}
{{--        'options' => $clientSections--}}
{{--    ])--}}


    @formField('multi_select', [
        'name' => 'client_categories',
        'label' => 'Категории клиентов',
        'options' => $сSectionsMulti
    ])


    @formField('input', [
        'name' => 'link',
        'label' => 'Link',
        'maxlength' => 200
    ])


    @formField('medias', [
        'name' => 'logo',
        'label' => 'Логотип',
        'note' => 'Рекомендуемый размер 360x360px',
    ])

    @formField('medias', [
        'name' => 'logo_dark',
        'label' => 'Логотип (для тёмного фона)',
        'note' => 'Рекомендуемый размер 360x360px',
    ])

    @formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Описание',
        'placeholder' => 'Описание',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


@stop
