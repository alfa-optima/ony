@extends('twill::layouts.form')

@section('contentFields')

    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'translated' => true,
        'maxlength' => 100
    ])


    @formField('input', [
        'name' => 'slug_field',
        'label' => 'Slug',
        'maxlength' => 255
    ])


{{--    @formField('wysiwyg', [--}}
{{--        'name' => 'text',--}}
{{--        'label' => 'Text',--}}
{{--        'placeholder' => 'Text',--}}
{{--        'translated' => true,--}}
{{--        'toolbarOptions' => [--}}
{{--            ['header' => [2, 3, 4, 5, 6]],--}}
{{--            'bold',--}}
{{--            'italic',--}}
{{--            'underline',--}}
{{--            ['list' => 'ordered'],--}}
{{--            ['list' => 'bullet'],--}}
{{--            'link',--}}
{{--        ],--}}
{{--        'editSource' => true,--}}
{{--    ])--}}


@endsection


@section('fieldsets')

    <a17-fieldset title="Service items" id="service_items" :open="true">
        @formField('repeater', ['type' => 'service_items'])
    </a17-fieldset>

@endsection
