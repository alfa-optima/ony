@extends('twill::layouts.form')

@section('contentFields')


    @formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Описание факта',
        'placeholder' => 'Описание факта',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    @formField('medias', [
        'name' => 'image',
        'label' => 'Картинка',
        'required' => true,
    ])


@stop
