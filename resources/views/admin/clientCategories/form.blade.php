@extends('twill::layouts.form')

@section('contentFields')


    @formField('input', [
        'name' => 'slug_field',
        'label' => 'Слаг',
        'required' => true,
        'maxlength' => 255
    ])


    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'translated' => true,
        'maxlength' => 100
    ])

@stop
