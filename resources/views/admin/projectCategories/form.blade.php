@extends('twill::layouts.form')

@section('contentFields')

    @formField('input', [
        'name' => 'slug_field',
        'label' => 'Слаг',
        'required' => true,
        'maxlength' => 255
    ])

{{--    @formField('select', [--}}
{{--        'name' => 'section_id',--}}
{{--        'label' => 'Категория сайта',--}}
{{--        'placeholder' => 'Можно указать категорию сайта',--}}
{{--        'options' => $siteSections--}}
{{--    ])--}}

@stop
