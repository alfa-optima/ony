@extends('twill::layouts.form')

@section('contentFields')

    @formField('input', [
        'name' => 'page_title',
        'label' => 'Page title',
        'translated' => true,
        'maxlength' => 255
    ])

    @formField('medias', [
        'name' => 'header_image',
        'label' => 'Header image',
        'required' => true,
    ])


    @formField('input', [
        'name' => 'header_vimeo_video_link',
        'label' => 'Header vimeo video link',
        'maxlength' => 255
    ])


    @formField('wysiwyg', [
        'name' => 'lead_text',
        'label' => 'Lead text',
        'placeholder' => 'Lead text',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    @formField('input', [
        'name' => 'body_title',
        'label' => 'Body title',
        'translated' => true,
        'maxlength' => 255
    ])


    @formField('wysiwyg', [
        'name' => 'body_text',
        'label' => 'Body text',
        'placeholder' => 'Body text',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


    @formField('input', [
        'name' => 'services_title',
        'label' => 'Services title',
        'translated' => true,
        'maxlength' => 100
    ])


{{--    @formField('input', [--}}
{{--        'name' => 'items_title',--}}
{{--        'label' => 'Items title',--}}
{{--        'translated' => true,--}}
{{--        'maxlength' => 100--}}
{{--    ])--}}


{{--    @formField('wysiwyg', [--}}
{{--        'name' => 'items_description',--}}
{{--        'label' => 'Items description',--}}
{{--        'placeholder' => 'Items description',--}}
{{--        'translated' => true,--}}
{{--        'toolbarOptions' => [--}}
{{--            ['header' => [2, 3, 4, 5, 6]],--}}
{{--            'bold',--}}
{{--            'italic',--}}
{{--            'underline',--}}
{{--            ['list' => 'ordered'],--}}
{{--            ['list' => 'bullet'],--}}
{{--            'link',--}}
{{--        ],--}}
{{--        'editSource' => true,--}}
{{--    ])--}}


    @formField('browser', [
        'moduleName' => 'services',
        'name' => 'services',
        'label' => 'Services',
        'max' => 1000,
    ])


    @formField('wysiwyg', [
        'name' => 'contacts',
        'label' => 'Contacts text',
        'placeholder' => 'Contacts text',
        'translated' => true,
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6]],
            'bold',
            'italic',
            'underline',
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            'link',
        ],
        'editSource' => true,
    ])


@stop
