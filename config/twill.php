<?php

return [

    'locales' => [ 'ru', 'en' ],

    'namespace' => 'App',

    'enabled' => [
        'users-management' => true,
        'media-library' => true,
        'file-library' => true,
        'dashboard' => true,
        'search' => true,
        'block-editor' => true,
        'buckets' => true,
        'users-image' => false,
        'users-description' => false,
        'site-link' => false,
        'settings' => true,
        'activitylog' => true,
    ],

    'media_library' => [
        'disk' => 'media_library',
        'endpoint_type' => env('MEDIA_LIBRARY_ENDPOINT_TYPE', 's3'),
        'cascade_delete' => env('MEDIA_LIBRARY_CASCADE_DELETE', false),
        'local_path' => env('MEDIA_LIBRARY_LOCAL_PATH'),
        'image_service' => env('MEDIA_LIBRARY_IMAGE_SERVICE', 'A17\Twill\Services\MediaLibrary\Imgix'),
        'acl' => env('MEDIA_LIBRARY_ACL', 'private'),
        'filesize_limit' => env('MEDIA_LIBRARY_FILESIZE_LIMIT', 50),
        'allowed_extensions' => ['svg', 'jpg', 'gif', 'png', 'jpeg'],
        'init_alt_text_from_filename' => true,
        'translated_form_fields' => false,
    ],

    'file_library' => [
        'disk' => 'files_library',
        'endpoint_type' => env('FILE_LIBRARY_ENDPOINT_TYPE', 's3'),
        'cascade_delete' => env('FILE_LIBRARY_CASCADE_DELETE', false),
        'local_path' => env('FILE_LIBRARY_LOCAL_PATH'),
        'file_service' => env('FILE_LIBRARY_FILE_SERVICE', 'A17\Twill\Services\FileLibrary\Disk'),
        'acl' => env('FILE_LIBRARY_ACL', 'public-read'),
        'filesize_limit' => env('FILE_LIBRARY_FILESIZE_LIMIT', 50),
        'allowed_extensions' => ['gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'mp4'],
        'prefix_uuid_with_local_path' => false
    ],

    'dashboard' => [
        'modules' => [

            'App\Models\Project' => [
                'name' => 'projects',
                'label' => 'Проекты',
                'label_singular' => 'проект',
                'routePrefix' => 'proj',
                'count' => true,
                'create' => true,
                'activity' => true,
                'draft' => true,
                'search' => true,
            ],
            'App\Models\ProjectList' => [
                'name' => 'projectLists',
                'label' => 'Списки проектов',
                'label_singular' => 'список проектов',
                'routePrefix' => 'proj',
                'count' => true,
                'create' => true,
                'activity' => true,
                'draft' => true,
                'search' => true,
            ],

            'App\Models\Client' => [
                'name' => 'clients',
                'label' => 'Клиенты',
                'label_singular' => 'клиент',
                'routePrefix' => 'proj',
                'count' => true,
                'create' => true,
                'activity' => true,
                'draft' => true,
                'search' => true,
            ],

        ],
    ],

    'block_editor' => [

        'blocks' => [

            'project_block' => [
                'title' => 'Block',
                'icon' => 'text',
                'component' => 'a17-block-project_block',
            ],

            'about_direction_block' => [
                'title' => 'Подпункт направления',
                'icon' => 'text',
                'component' => 'a17-block-about_direction_block',
            ],

            'home_text_block' => [
                'title' => 'Текст',
                'icon' => 'text',
                'component' => 'a17-block-home_text_block',
            ],

            'home_project_block' => [
                'title' => 'Проект',
                'icon' => 'text',
                'component' => 'a17-block-home_project_block',
            ],

            'project' => [
                'title' => 'Проект',
                'icon' => 'text',
                'component' => 'a17-block-projects',
            ],

            'contacts_text_block' => [
                'title' => 'Текстовый блок',
                'icon' => 'text',
                'component' => 'a17-block-contacts_text_block',
            ],

        ],

        'browser_route_prefixes' => [
            'projects' => 'proj',
        ],

        'repeaters' => [

            'services' => [
                'title' => 'Услуга',
                'trigger' => 'Добавить услугу',
                'component' => 'a17-block-about_services_item'
            ],

            'service_items' => [
                'title' => 'Item',
                'trigger' => 'Add item',
                'component' => 'a17-block-service_item'
            ],

        ],

		'crops' => [

            'project_block_images' => [
                'default' => [
                    [
                        'name' => 'default',
//                        'ratio' => 1280 / 720,
//                        'minValues' => [
//                            'width' => 850,
//                            'height' => 480,
//                        ],
                    ],
                ],
            ],

            'about_awards_block_image' => [
                'default' => [
                    [
                        'name' => 'default',
                        'ratio' => 1920 / 1080,
                        'minValues' => [
                            'width' => 1920,
                            'height' => 1080,
                        ],
                    ],
                ],
            ],

        ],

		'files' => [ 'project_block_video_mp4' ]

	],



    'settings' => [
        'crops' => [
            'seo_ogImage' => [
                'default' => [
                    [
                        'name' => 'landscape',
                        //'ratio' => 16 / 9,
                    ]
                ]
            ],
        ]
    ]

];
