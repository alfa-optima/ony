<?php

return [

    // Главная страница
    'home' => [
        'title'=> 'Главная',
        'route' => 'admin.home.homePages.index',
        'primary_navigation' => [

            // Главная страница
            'homePages' => [
                'title' => 'Главная',
                'module' => true
            ],

        ]
    ],

    // Категории сайта
    'siteSections' => [
        'title' => 'Категории',
        'module' => true
    ],

    // Проекты
    'proj' => [
        'title'=> 'Проекты',
        'route' => 'admin.proj.projects.index',
        'primary_navigation' => [

            // Проекты
            'projects' => [
                'title' => 'Проекты',
                'module' => true
            ],

            // Списки
            'projectLists' => [
                'title' => 'Списки',
                'module' => true
            ],

            // Типы работ
            'projectCategories' => [
                'title' => 'Типы работ',
                'module' => true
            ],

            // Направления работ
            'projectDirections' => [
                'title' => 'Направления работ',
                'module' => true
            ],

            // Клиенты
            'clients' => [
                'title' => 'Клиенты',
                'module' => true
            ],

            // Категории проектов
            'clientCategories' => [
                'title' => 'Категории клиентов',
                'module' => true
            ],

        ]
    ],

    // Страница услуг
    'serv' => [
        'title'=> 'Услуги',
        'route' => 'admin.serv.servicesPages.index',
        'primary_navigation' => [

            // Страница услуг
            'servicesPages' => [
                'title' => 'Страница "Услуги"',
                'module' => true
            ],

            // услуги
            'services' => [
                'title' => 'Услуги',
                'module' => true
            ],

        ]
    ],



    // О нас
    'about' => [
        'title'=> 'О нас',
        'route' => 'admin.about.aboutPages.index',
        'primary_navigation' => [

            // About
            'aboutPages' => [
                'title' => 'Страница "О нас"',
                'module' => true
            ],

            // Направления для About
            'aboutDirections' => [
                'title' => 'Направления',
                'module' => true
            ],

            // Направления для About
            'aboutAwards' => [
                'title' => 'Награды',
                'module' => true
            ],

            // Факты
            'facts' => [
                'title' => 'Факты',
                'module' => true
            ],

            // Вакансии
            'jobs' => [
                'title' => 'Вакансии',
                'module' => true
            ],

        ]
    ],



    // Контакты
    'contactPages' => [
        'title' => 'Контакты',
        'module' => true
    ],



    // Настройки
    'main_settings' => [
        'title'=> 'Настройки',
        'route' => 'admin.main_settings.shells.index',
        'primary_navigation' => [

            // Shell
            'shells' => [
                'title' => 'Shell',
                'module' => true
            ],

            // Соц. сети
            'socials' => [
                'title' => 'Соц. сети',
                'module' => true
            ],
        ]
    ],



];
